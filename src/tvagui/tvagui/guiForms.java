package tvagui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import tech.tablesaw.api.*;
import tech.tablesaw.columns.Column;
import tech.tablesaw.io.csv.CsvReadOptions;
import tech.tablesaw.io.csv.CsvWriteOptions;
import tech.tablesaw.selection.Selection;


public class guiForms {
	String dateDef = " / /    ";
	String numDef = "0.0000";
	public guiForms() {
		
	}
		// TODO Auto-generated constructor stub
		
		
	public void addArg(HashMap<String, ArrayList<String>> dict, ArrayList<String> arg, String name  ) {
		boolean alreadyExists  = dict.containsKey(name);		
		if(alreadyExists) {
			dict.replace(name, arg);
		}
		System.out.println(arg);
		System.out.println(name);
		if(arg.size() != 0 ) {
			dict.put(name, arg);
		}
		
		
	}

	public void sanitizeDates(ArrayList<String> dates) {
	    for (int counter = 0; counter < dates.size() - 1; counter++) {
	    	if(dates.get(counter).equals(dateDef)) {
	    		dates.set(counter, "");
	    	}
	    	else {
	    		String str = dates.get(counter);
	    		int pos = str.lastIndexOf("/");
	    		String year = str.substring(pos + 1, str.length());
	    		String newYear = year + "/";
	    		dates.set(counter, newYear + str.substring(0, pos));
	    		
	    	}
	    }

	}
	
	public void sanitizeDateasDouble(ArrayList<String> dates) {
	    for (int counter = 0; counter < dates.size() - 1; counter++) {
	    	if(dates.get(counter).equals(dateDef)) {
	    		dates.set(counter, "");
	    	}
	    	else {
	    		String str = dates.get(counter);
	    		List<String> list = new ArrayList<String>(Arrays.asList(str.split("/")));
	    		String month = "";
	    		String day = "";

	    		if(list.get(1).length() == 1) {
	    			month = "0" + list.get(1);
	    		}
	    		else {month = list.get(1);}
	    		if(list.get(2).length() == 1) {
	    			day = "0" + list.get(2);
	    		}
	    		else {day = list.get(2);}
	    		
	    		String newString = list.get(0) + month + day;
	    		dates.set(counter, newString);
	    		
	    	}
	    }
		
	}

	public void sanitizeNumbers(ArrayList<String> numbers) {
		 for (int counter = 0; counter < numbers.size() - 1; counter++) {
		    	if(numbers.get(counter).equals(numDef)) {
		    		numbers.set(counter, "");
		    	}
	}
}

	public void sanitizeLists(ArrayList<String> listString) {
		 for (int counter = 0; counter < listString.size() - 1; counter++) {
			 if(listString.size() != 1) {
				 String str = "'" +  listString.get(counter) + "'";
				 listString.set(counter, str);
			 }
		 }
		 
		 
	}
	public void sanitizeSelectedLists(ArrayList<String> listString) {
		 for (int counter = 0; counter < listString.size(); counter++) {
			 if(listString.size() != 1) {
				String str = "'" +  listString.get(counter) + "'";
				listString.set(counter, str);
			 }
		 }
	}
	
	public void breakUpCusipList(ArrayList<String> cusip6List, ArrayList<String> cusip9List, ArrayList<String> cusipAllList) {
		for(String i: cusipAllList) {
			i = i.trim();
			if(i.length() == 6) {
				cusip6List.add(i);
			}
			if(i.length() == 9) {
				cusip9List.add(i);
			}
		}
	}
	
	public Table XlsxCusipReader(String filename) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook(filename);

        XSSFSheet sheet = workbook.getSheetAt(0);
        StringBuilder data = new StringBuilder();
        try {
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    CellType type = cell.getCellType();
                    if (type == CellType.BOOLEAN) {
                        data.append(cell.getBooleanCellValue());
                    } else if (type == CellType.NUMERIC) {
                    	if(cell.getColumnIndex() == 0) {
                    		data.append(cell.getLocalDateTimeCellValue().toLocalDate());
                    	}
                    	else {
                        data.append(cell.getNumericCellValue());
                    	}
                    } else if (type == CellType.STRING) {
                        data.append(cell.getStringCellValue());
              
                    } else if (type == CellType.BLANK) {
                    } else {
                        data.append(cell + "");
                    }
                    data.append(",");
                }
                data.append('\n');
            }
            Files.write(Paths.get("cusips.csv"),
                data.toString().getBytes("UTF-8"));
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    	CsvReadOptions.Builder builder = 
    			CsvReadOptions.builder("cusips.csv")
    				.separator(',');									// table is tab-delimited
    	CsvReadOptions options = builder.build();
    	
        Table table = Table.read().usingOptions(options);
		return(table);
	}



	public Table XlsCusipReader(String filename) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(filename));
        workbook.setMissingCellPolicy(Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        HSSFSheet sheet = workbook.getSheetAt(0);

        StringBuilder data = new StringBuilder();
        try {
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    CellType type = cell.getCellType();
                    if (type == CellType.BOOLEAN) {
                        data.append(cell.getBooleanCellValue());
                    } else if (type == CellType.NUMERIC) {
                    	if(cell.getColumnIndex() == 0) {
                    		data.append(cell.getLocalDateTimeCellValue().toLocalDate());
                    	}
                    	else {
                        data.append(cell.getNumericCellValue());
                    	}
                    } else if (type == CellType.STRING) {
                        data.append(cell.getStringCellValue());
              
                    } else if (type == CellType.BLANK) {
                    } else {
                        data.append(cell + "");
                    }
                    data.append(",");
                }
                data.append('\n');
            }
            Files.write(Paths.get("cusips.csv"),
                data.toString().getBytes("UTF-8"));
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    	CsvReadOptions.Builder builder = 
    			CsvReadOptions.builder("cusips.csv")
    				.separator(',');									// table is tab-delimited
    	CsvReadOptions options = builder.build();
    	
        Table table = Table.read().usingOptions(options);
        return(table);
	}


}

class bondInfo extends guiForms {
	
	HashMap<String,ArrayList<String>> bondInfoDictionary = new HashMap<String, ArrayList<String>>();
	HashMap<String,ArrayList<String>> issueInfoDictionary = new HashMap<String, ArrayList<String>>();
	HashMap<String,ArrayList<String>> varRateDictionary = new HashMap<String, ArrayList<String>>();
	HashMap<String,ArrayList<String>> callInfoDictionary = new HashMap<String, ArrayList<String>>();
	HashMap<String,ArrayList<String>> partRdemptnDictionary = new HashMap<String, ArrayList<String>>();
	HashMap<String,ArrayList<String>> redemptnDictionary = new HashMap<String, ArrayList<String>>();

	HashMap<String,ArrayList<String>> sinkFundDictionary = new HashMap<String, ArrayList<String>>();
	HashMap<String,ArrayList<String>> addlCreditDictionary = new HashMap<String, ArrayList<String>>();

	
	HashMap<String,ArrayList<String>> moodyRatingDictionary = new HashMap<String, ArrayList<String>>();
	HashMap<String,ArrayList<String>> spRatingDictionary = new HashMap<String, ArrayList<String>>();

	


	





	public HashMap<String, ArrayList<String>> getBondInfoDictionary() {
		HashMap<String, ArrayList<String>> bondInfoDictionaryCopy = new HashMap<String, ArrayList<String>>();
		bondInfoDictionaryCopy.putAll(bondInfoDictionary);
		return bondInfoDictionaryCopy;
	}
	public void setBondInfoDictionary(HashMap<String, ArrayList<String>> bondInfoDictionary) {
		this.bondInfoDictionary = bondInfoDictionary;
	}
	public HashMap<String, ArrayList<String>> getIssueInfoDictionary() {
		HashMap<String, ArrayList<String>> issueInfoDictionaryCopy = new HashMap<String, ArrayList<String>>();
		issueInfoDictionaryCopy.putAll(issueInfoDictionary);
		return issueInfoDictionaryCopy;
	}
	public void setIssueInfoDictionary(HashMap<String, ArrayList<String>> issueInfoDictionary) {
		this.issueInfoDictionary = issueInfoDictionary;
	}
	public HashMap<String, ArrayList<String>> getVarRateDictionary() {
		HashMap<String, ArrayList<String>> varRateDictionayCopy = new HashMap<String, ArrayList<String>>();
		varRateDictionayCopy.putAll(varRateDictionary);
		return varRateDictionayCopy;
	}
	public void setVarRateDictionary(HashMap<String, ArrayList<String>> varRateDictionay) {
		this.varRateDictionary = varRateDictionay;
	}
	public HashMap<String, ArrayList<String>> getCallInfoDictionary() {
		HashMap<String, ArrayList<String>> callInfoDictionaryCopy = new HashMap<String, ArrayList<String>>();
		callInfoDictionaryCopy.putAll(callInfoDictionary);
		return callInfoDictionary;
	}
	public void setCallInfoDictionary(HashMap<String, ArrayList<String>> callInfoDictionay) {
		this.callInfoDictionary = callInfoDictionay;
	}
	public HashMap<String, ArrayList<String>> getPartRdemptnDictionary() {
		HashMap<String, ArrayList<String>> partRdemptnDictionaryCopy = new HashMap<String, ArrayList<String>>();
		partRdemptnDictionaryCopy.putAll(partRdemptnDictionary);
		return partRdemptnDictionary;
	}
	public void setPartRdemptnDictionary(HashMap<String, ArrayList<String>> partRdemptnDictionary) {
		this.partRdemptnDictionary = partRdemptnDictionary;
	}
	public HashMap<String, ArrayList<String>> getRedemptnDictionary() {
		HashMap<String, ArrayList<String>> redemptnDictionaryCopy = new HashMap<String, ArrayList<String>>();
		redemptnDictionaryCopy.putAll(redemptnDictionary);

		return redemptnDictionary;
	}
	public void setRedemptnDictionary(HashMap<String, ArrayList<String>> redemptnDictionary) {
		this.redemptnDictionary = redemptnDictionary;
	}
	public HashMap<String, ArrayList<String>> getSinkFundDictionary() {
		HashMap<String, ArrayList<String>> sinkFundDictionaryCopy = new HashMap<String, ArrayList<String>>();
		sinkFundDictionaryCopy.putAll(sinkFundDictionary);
		return sinkFundDictionary;
	}
	public void setSinkFundDictionary(HashMap<String, ArrayList<String>> sinkFundDictionary) {
		this.sinkFundDictionary = sinkFundDictionary;
	}





	public HashMap<String, ArrayList<String>> getAddlCreditDictionary() {
		return addlCreditDictionary;
	}
	public void setAddlCreditDictionary(HashMap<String, ArrayList<String>> addlCreditDictionary) {
		this.addlCreditDictionary = addlCreditDictionary;
	}





	ArrayList<String> matDate = new ArrayList<String>();
    ArrayList<String> datedDate = new ArrayList<String>();
    ArrayList<String> coupon = new ArrayList<String>();
    ArrayList<String> yield = new ArrayList<String>();
    ArrayList<String> parAmount = new ArrayList<String>();
    ArrayList<String> price = new ArrayList<String>();
    ArrayList<String> taxExempt =  new ArrayList<String>();
    ArrayList<String> couponCode =  new ArrayList<String>();
    ArrayList<String> debtType =  new ArrayList<String>();
    ArrayList<String> state =  new ArrayList<String>();
    ArrayList<String> useProceeds =  new ArrayList<String>();
    ArrayList<String> insurance =  new ArrayList<String>();
    ArrayList<String> SPRating =  new ArrayList<String>();
    ArrayList<String> MoodyRating =  new ArrayList<String>();
    ArrayList<String> addlCredit =  new ArrayList<String>();

    ArrayList<String> capitalPurpose =  new ArrayList<String>();

	ArrayList<String> bankQualified =  new ArrayList<String>();
    ArrayList<String> currInterestRate = new ArrayList<String>();
    ArrayList<String> nextCallDate = new ArrayList<String>();
    ArrayList<String> nextCallPrice = new ArrayList<String>();

    ArrayList<String> UpdatedDate = new ArrayList<String>();

    ArrayList<String> callDate = new ArrayList<String>();
    ArrayList<String> callPrice = new ArrayList<String>();
    ArrayList<String> partialCallDate = new ArrayList<String>();
    ArrayList<String> partialCallRate = new ArrayList<String>();
    ArrayList<String> sinkDate = new ArrayList<String>();
    ArrayList<String> sinkPrice = new ArrayList<String>();
    ArrayList<String> redemptionDate = new ArrayList<String>();
    ArrayList<String> redemptionPrice = new ArrayList<String>();

    
    boolean activeMaturities; 
    tradingConfigs bondInfoTrading = new tradingConfigs();
    
    
     
    
	public ArrayList<String> getMatDate() {
		return matDate;
	}
	public void setMatDate(ArrayList<String> matDate) {
		this.matDate = matDate;
		sanitizeDates(this.matDate);
		addArg(bondInfoDictionary,this.matDate,"maturity_date_d");
	}

	public ArrayList<String> getDatedDate() {
		return datedDate;
		
		
	}
	public void setDatedDate(ArrayList<String> datedDate) {
		this.datedDate = datedDate;
		sanitizeDates(this.datedDate);
		addArg(bondInfoDictionary,this.datedDate,"dated_date_d");

	}

	public ArrayList<String> getCoupon() {
		return coupon;
	}
	public void setCoupon(ArrayList<String> coupon) {
		this.coupon = coupon;
		sanitizeNumbers(this.coupon);
		addArg(bondInfoDictionary,this.coupon,"coupon_f");

	}

	public ArrayList<String> getYield() {
		return yield;
	}
	public void setYield(ArrayList<String> yield) {
		this.yield = yield;
		sanitizeNumbers(this.yield);
		addArg(bondInfoDictionary,this.yield,"offering_yield_f");

	}

	public ArrayList<String> getParAmount() {
		return parAmount;

	}
	public void setParAmount(ArrayList<String> parAmount) {
		this.parAmount = parAmount;
		sanitizeNumbers(this.parAmount);
		addArg(bondInfoDictionary,this.parAmount,"total_maturity_offering_amt_f");

	}

	public ArrayList<String> getPrice() {
		return price;
	}
	public void setPrice(ArrayList<String> price) {
		this.price = price;
		sanitizeNumbers(this.price);
		addArg(bondInfoDictionary,this.price,"offering_price_f");

		
	}

	public ArrayList<String> getTaxExempt() {
		return taxExempt;
	}
	public void setTaxExempt(ArrayList<Boolean> taxExempt) {
		ArrayList<String> taxExemptCodes = new ArrayList<String>();
		if(taxExempt.get(0)) {
			taxExemptCodes.add("TAX");
		}
		if(taxExempt.get(1)) {
			taxExemptCodes.add("EXMP");
		}
		if(taxExempt.get(2)) {
			taxExemptCodes.add("AMT");
		}
		if(taxExemptCodes.size() != 3) {
			this.taxExempt = taxExemptCodes;
			addArg(bondInfoDictionary,this.taxExempt,"tax_code_c");

		}
		
	}

	public ArrayList<String> getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(ArrayList<String> couponCode) {
		
		this.couponCode = couponCode;
		sanitizeSelectedLists(this.couponCode);
		addArg(bondInfoDictionary,this.couponCode,"coupon_code_c");

		
	}

	public ArrayList<String> getDebtType() {
		return debtType;
	}
	public void setDebtType(ArrayList<String> debtType) {
		this.debtType = debtType;
		sanitizeSelectedLists(this.debtType);
		addArg(bondInfoDictionary,this.debtType,"debt_type_c");
	}



	public ArrayList<String> getState() {
		return state;
	}
	public void setState(ArrayList<String> state) {
		this.state = state;
		sanitizeSelectedLists(this.state);

		addArg(issueInfoDictionary,this.state,"STATE_C");

	}


	public ArrayList<String> getUseProceeds() {
		return useProceeds;
	}
	public void setUseProceeds(ArrayList<String> useProceeds) {
		this.useProceeds = useProceeds;
		sanitizeSelectedLists(this.useProceeds);

		addArg(bondInfoDictionary,this.useProceeds,"use_of_proceeds_c");

	}

	public ArrayList<String> getInsurance() {
		return insurance;
	}
	public void setInsurance(ArrayList<String> insurance) {
		this.insurance = insurance;
		sanitizeSelectedLists(this.insurance);

		addArg(bondInfoDictionary,this.insurance,"BOND_INSURANCE_CODE_C");

	}

	

    public ArrayList<String> getAddlCredit() {
		return addlCredit;
	}
	public void setAddlCredit(ArrayList<String> addlCredit) {
		this.addlCredit = addlCredit;
		sanitizeSelectedLists(this.addlCredit);
		addArg(addlCreditDictionary,this.addlCredit,"addl_credit_type_code_c");
	}
	
	
	
	
	public ArrayList<String> getCapitalPurpose() {
		return capitalPurpose;
	}
	public void setCapitalPurpose(ArrayList<String> capitalPurpose) {
		this.capitalPurpose = capitalPurpose;
		sanitizeSelectedLists(this.capitalPurpose);
		addArg(bondInfoDictionary,this.capitalPurpose,"capital_purpose_c");

	}
	
	
	
	
	public ArrayList<String> getSPRating() {
		return SPRating;
	}
	public void setSPRating(ArrayList<String> sPRating) {
		this.SPRating = SPRating;
	}


	public ArrayList<String> getMoodyRating() {
		return MoodyRating;
	}
	public void setMoodyRating(ArrayList<String> moodyRating) {
		this.MoodyRating = MoodyRating;		
	}

	
	public ArrayList<String> getBankQualified() {
		return bankQualified;
	}
	public void setBankQualified(ArrayList<Boolean> bankQualified) {
		ArrayList<String> bankQualifiedCodes = new ArrayList<String>();
		if(bankQualified.get(0)) {
			bankQualifiedCodes.add("Y");
		}
		if(bankQualified.get(1)) {
			bankQualifiedCodes.add("N");
		}
		if(bankQualifiedCodes.size() != 2) {
			this.bankQualified = bankQualifiedCodes;
			sanitizeSelectedLists(this.bankQualified);
			addArg(bondInfoDictionary,this.bankQualified,"bank_qualified_i");

		}
		
		
		
	}


	public ArrayList<String> getCurrInterestRate() {
		return currInterestRate;
	}
	public void setCurrInterestRate(ArrayList<String> currInterestRate) {
		this.currInterestRate = currInterestRate;
		addArg(varRateDictionary,this.currInterestRate,"current_interest_rate_f");
	}

	public ArrayList<String> getUpdatedDate() {
		return UpdatedDate;
	}
	public void setUpdatedDate(ArrayList<String> UpdatedDate) {
		this.UpdatedDate = UpdatedDate;
		sanitizeDates(this.UpdatedDate);
		addArg(bondInfoDictionary,this.UpdatedDate,"update_date_d");		
	}

	
	public ArrayList<String> getCallDate() {
		return callDate;
	}
	public void setCallDate(ArrayList<String> callDate) {
		this.callDate = callDate;
		sanitizeDates(this.callDate);
		sanitizeDateasDouble(this.callDate);
		addArg(callInfoDictionary,this.callDate,"call_date_d");

		
	}

	public ArrayList<String> getNextCallPrice() {
		return nextCallPrice;
	}
	public void setNextCallPrice(ArrayList<String> nextCallPrice) {
		this.nextCallPrice = nextCallPrice;
		sanitizeNumbers(this.nextCallPrice);
		addArg(bondInfoDictionary,this.nextCallPrice,"next_call_price_f");	
	}


	public ArrayList<String> getNextCallDate() {
		return nextCallDate;
	}
	public void setNextCallDate(ArrayList<String> nextCallDate) {
		this.nextCallDate = nextCallDate;
		sanitizeDates(this.nextCallDate);
		addArg(bondInfoDictionary,this.nextCallDate,"next_call_date_d");

		
	}

	public ArrayList<String> getCallPrice() {
		return callPrice;
	}
	public void setCallPrice(ArrayList<String> callPrice) {
		this.callPrice = callPrice;
		sanitizeNumbers(this.callPrice);
		addArg(callInfoDictionary,this.callPrice,"call_price_f");	
	}


	
	public ArrayList<String> getPartialCallDate() {
		return partialCallDate;
	}
	public void setPartialCallDate(ArrayList<String> partialCallDate) {
		this.partialCallDate = partialCallDate;
		sanitizeDates(this.partialCallDate);
		addArg(partRdemptnDictionary,this.partialCallDate,"partial_call_date_d");

		
	}


	public ArrayList<String> getPartialCallRate() {
		return partialCallRate;
	}
	public void setPartialCallRate(ArrayList<String> partialCallRate) {
		this.partialCallRate = partialCallRate;
		sanitizeNumbers(this.partialCallRate);
		addArg(partRdemptnDictionary,this.partialCallRate,"partial_call_rate_f");	

	}


	public ArrayList<String> getSinkDate() {
		return sinkDate;
	}
	public void setSinkDate(ArrayList<String> sinkDate) {
		this.sinkDate = sinkDate;
		sanitizeDates(this.sinkDate);
		addArg(sinkFundDictionary,this.sinkDate,"sink_date_d");		
	}




	public ArrayList<String> getSinkPrice() {
		return sinkPrice;
	}
	public void setSinkPrice(ArrayList<String> sinkPrice) {
		this.sinkPrice = sinkPrice;
		sanitizeNumbers(this.sinkPrice);
		addArg(sinkFundDictionary,this.sinkPrice,"sink_price_f");	
	}




	public ArrayList<String> getRedemptionDate() {
		return redemptionDate;
	}
	public void setRedemptionDate(ArrayList<String> redemptionDate) {
		this.redemptionDate = redemptionDate;
		sanitizeDates(this.redemptionDate);
		addArg(redemptnDictionary,this.redemptionDate,"redemption_date_d");		

	}




	public ArrayList<String> getRedemptionPrice() {
		return redemptionPrice;
	}
	public void setRedemptionPrice(ArrayList<String> redemptionPrice) {
		this.redemptionPrice = redemptionPrice;
		sanitizeNumbers(this.redemptionPrice);
		addArg(redemptnDictionary,this.redemptionPrice,"redemption_price_f");		

	}




	public boolean isActiveMaturities() {
		return activeMaturities;
	}
	public void setActiveMaturities(boolean activeMaturities) {
		this.activeMaturities = activeMaturities;
		ArrayList<String> activeMaturitiesOn = new ArrayList<String>();
		activeMaturitiesOn.add("Y");
		addArg(bondInfoDictionary,activeMaturitiesOn,"active_maturity_flag_i");
	}




	public tradingConfigs getBondInfoTrading() {
		return bondInfoTrading;
	}





	public bondInfo() {
		super();
	}
	
	
	
	
}


 class cheapTrade extends bondInfo {
	  ArrayList<String> MMDSpread = new ArrayList<String>();

	 
	 
		public ArrayList<String> getMMDSpread() {
		return MMDSpread;
	}



	public void setMMDSpread(ArrayList<String> mMDSpread) {
		MMDSpread = mMDSpread;
	}



		public void cheapTrade() {
			bondInfoTrading.setTradingChecks(true);
			bondInfoTrading.cusipFilter = true;
		}
		
		
 }
class quickBond extends bondInfo {
	
	
	String cusipFile;
	String cusipText;
	boolean activeMaturities;
	ArrayList<String> cusipArrayTextList = new ArrayList<String>();
	ArrayList<String> cusip9List = new ArrayList<String>();
	ArrayList<String> cusip6List = new ArrayList<String>();
	
	ArrayList<String> agmFilterList = new ArrayList<String>();
	String agmFilter; 
	
	
	public String getCusipFile() {
		return cusipFile;
	}
	
	public void setCusipFile(String cusipFile) throws IOException {
		this.cusipFile = cusipFile;
		Table c = null;
		if(cusipFile.endsWith("xls")) {
			
			
			c = XlsCusipReader(this.cusipFile);

		}
		else if(cusipFile.endsWith("xlsx") | cusipFile.endsWith("xlsm")) {
		
			c = XlsxCusipReader(this.cusipFile);
		}
		else if  (cusipFile.endsWith("csv")) {
			
	    	CsvReadOptions.Builder builder = 
	    			CsvReadOptions.builder(this.cusipFile)
	    				.separator(',').header(true);									// table is tab-delimited

	    	CsvReadOptions options = builder.build();
			c = Table.read().usingOptions(options);

		}
		
		
		
		List cusipList = c.stringColumn(0).asList();
		cusipArrayTextList = new ArrayList<String>(cusipList);
		cusipList = null;
		breakUpCusipList(cusip6List,cusip9List,cusipArrayTextList );

		sanitizeSelectedLists(cusip6List); 
		sanitizeSelectedLists(cusip9List); 
		
		System.out.println(cusip6List);
		System.out.println(cusip9List);
		
		addArg(bondInfoDictionary,cusip9List,"cusip_c");
		addArg(bondInfoDictionary,cusip6List,"cusip_6");

		
	}
	
	public String getAGMFilter() {
		return this.agmFilter;
	}
	
	public void setAGMFilter(String cusipFile) throws IOException {
		this.agmFilter = cusipFile;
		Table c = null;
		if(cusipFile.endsWith("agmFilter")) {
			
			
			c = XlsCusipReader(this.cusipFile);

		}
		else if(cusipFile.endsWith("xlsx") | cusipFile.endsWith("xlsm")) {
		
			c = XlsxCusipReader(this.agmFilter);
		}
		else if  (cusipFile.endsWith("csv")) {
	    	CsvReadOptions.Builder builder = 
	    			CsvReadOptions.builder(this.agmFilter)
	    				.separator(',').header(true);									// table is tab-delimited

	    	CsvReadOptions options = builder.build();
			c = Table.read().usingOptions(options);

		}
		
		
		
		List cusipList = c.stringColumn(0).asList();
		agmFilterList = new ArrayList<String>(cusipList);
		sanitizeSelectedLists(agmFilterList); 
		
		System.out.println(agmFilterList);		
		addArg(bondInfoDictionary,agmFilterList,"agm_filter");	
	}
	
	
	public String getCusipText() {
		return cusipText;
	}
	public void setCusipText(String cusipText) {
		System.out.println(cusipText);
		List<String> list = Arrays.asList(cusipText.split(","));
		
		cusipArrayTextList = new ArrayList<String>(list);
		breakUpCusipList(cusip6List,cusip9List,cusipArrayTextList );		
		
		sanitizeSelectedLists(cusip6List); 
		sanitizeSelectedLists(cusip9List); 

		
		addArg(bondInfoDictionary,cusip9List,"cusip_c");
		addArg(bondInfoDictionary,cusip6List,"cusip_6");
		

		this.cusipText = cusipText;
	
	}
	public boolean isActiveMaturities() {
		return activeMaturities;
	}
	public void setActiveMaturities(boolean activeMaturities) {
		this.activeMaturities = activeMaturities;
		ArrayList<String> activeMaturitiesOn = new ArrayList<String>();
		activeMaturitiesOn.add("Y");
		addArg(bondInfoDictionary,activeMaturitiesOn,"active_maturity_flag_i");
	}
	
	
	
	
	
}


class tradingCusipVisualiser extends quickBond {

	public void tradingCusipVisualiser() {
		bondInfoTrading.setTradingChecks(true);
	}
	
}



class tradingConfigs extends guiForms {
	
	boolean tradingChecks;
	boolean cusipFilter = true;
	
	
	HashMap<String,ArrayList<String>> tradeInfoDictionary = new HashMap<String, ArrayList<String>>();



	public HashMap<String, ArrayList<String>> getTradeInfoDictionary() {
		HashMap<String, ArrayList<String>> tradeInfoDictionaryCopy = new HashMap<String, ArrayList<String>>();
		tradeInfoDictionaryCopy.putAll(tradeInfoDictionary);
		return tradeInfoDictionaryCopy;
	}

	public void setTradeInfoDictionary(HashMap<String, ArrayList<String>> tradeInfoDictionary) {
		this.tradeInfoDictionary = tradeInfoDictionary;
	}


	ArrayList<String> tradeDate = new ArrayList<String>();
	ArrayList<String> maturityDate = new ArrayList<String>();
	ArrayList<String> coupon = new ArrayList<String>();
    ArrayList<String> parAmount = new ArrayList<String>();
    ArrayList<String> parDollarPrice = new ArrayList<String>();
    ArrayList<String> parTradedYield = new ArrayList<String>();
    ArrayList<String> tradeType = new ArrayList<String>();
    
    
    
    boolean calculateTradingYield = false;
    String MMDTable = ""; 

	public tradingConfigs() {
		CheckDefaultMMD();
	}
	
    public void CheckDefaultMMD() {
		String str = "MSRB_Analyis_Historical_MMD_Interpolated.xlsm";
		//this.MMDTable =  str;	

		File f = new File(System.getProperty("java.class.path"));
		File userdir = f.getAbsoluteFile().getParentFile();
		String path = userdir.toString();

		File dir = new File(path +"/" + str);
		boolean exists = dir.exists();
		System.out.println("Default MMD File?" + exists);
		if(exists) {
			this.MMDTable = path + "/" + str;	
		}
		
		
    }
    
    public boolean isTradingChecks() {
		return tradingChecks;
	}
    
    
    
	public void setTradingChecks(boolean tradingChecks) {
		this.tradingChecks = tradingChecks;
	}
	
    public boolean isCusipFilter() {
		return cusipFilter;
	}
	public void setCusipFilter(boolean cusipFilter) {
		this.cusipFilter = cusipFilter;
	}


	public ArrayList<String> getTradeDate() {
		return tradeDate;
	}	
	
	
	public void setTradeDate(ArrayList<String> tradeDate) {
		this.tradeDate = tradeDate;
		sanitizeDates(this.tradeDate);
		addArg(tradeInfoDictionary,this.tradeDate,"trade_date");
	}


	
	
	
	public ArrayList<String> getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(ArrayList<String> maturityDate) {
		this.maturityDate = maturityDate;
		sanitizeDates(this.maturityDate);
		addArg(tradeInfoDictionary,this.maturityDate,"maturity_date");

	}

	public ArrayList<String> getCoupon() {
		return coupon;
	}

	public void setCoupon(ArrayList<String> coupon) {
		this.coupon = coupon;
		sanitizeNumbers(this.coupon);
		addArg(tradeInfoDictionary,this.coupon,"coupon");
	}

	public ArrayList<String> getParAmount() {
		return parAmount;
	}
	public void setParAmount(ArrayList<String> parAmount) {
		this.parAmount = parAmount;
		sanitizeNumbers(this.parAmount);
		addArg(tradeInfoDictionary,this.parAmount,"par_traded");
	}


	public ArrayList<String> getParDollarPrice() {
		return parDollarPrice;
	}
	public void setParDollarPrice(ArrayList<String> parDollarPrice) {
		this.parDollarPrice = parDollarPrice;
		sanitizeNumbers(this.parDollarPrice);
		addArg(tradeInfoDictionary,this.parDollarPrice,"dollar_price");

	}


	public ArrayList<String> getParTradedYield() {
		return parTradedYield;
	}
	public void setParTradedYield(ArrayList<String> parTradedYield) {
		this.parTradedYield = parTradedYield;
		sanitizeNumbers(this.parTradedYield);
		addArg(tradeInfoDictionary,this.parTradedYield,"yield");

	}


	public ArrayList<String> getTradeType() {
		return tradeType;
	}
	public void setTradeType(ArrayList<Boolean> tradeType) {
		ArrayList<String> tradeTypeCodes = new ArrayList<String>();
		if(tradeType.get(0)) {
			tradeTypeCodes.add("D");
		}
		if(tradeType.get(1)) {
			tradeTypeCodes.add("S");
		}
		if(tradeType.get(2)) {
			tradeTypeCodes.add("P");
		}
		if(tradeTypeCodes.size() != 3) {
			this.tradeType = tradeTypeCodes;
			sanitizeSelectedLists(this.tradeType);
			addArg(tradeInfoDictionary,this.tradeType,"trade_type");

		}		
		
		
	}


	public boolean isCalculateTradingYield() {
		return calculateTradingYield;
	}
	public void setCalculateTradingYield(boolean calculateTradingYield) {
		this.calculateTradingYield = calculateTradingYield;
	}


	public String getMMDTable() {
		return MMDTable;
	}
	public void setMMDTable(String MMDTable) {
		this.MMDTable = MMDTable;
	}


	public tradingConfigs(boolean t) {
		tradingChecks = t;
    }
    
	
}


