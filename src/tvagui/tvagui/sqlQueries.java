package tvagui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class sqlQueries {
	String mergentName = System.getProperty("merg2dbname");
	String user = System.getProperty("mergdbuser");
	static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	//First condition is a boolean indicating if this condition is the first one in the query, we want to add a "(" before instead of an "AND". If not continue with the AND 
	boolean firstCondition = true;
	String emptyString = "";
	String dateDef = " / /    ";
	int spinDef = 0;
	
	//Maximum of 1,000,000 per query
	int queryLimit = 1000000;
	
	public sqlQueries() {
		// TODO Auto-generated constructor stub
	}
	
	
	//The following are sql string constructors. THey take a condition and an arraylist and form the necessary sql string.
	
	//Add logic expression adds a logical expression (>,<,=) from a category and a value.
	public String addLogicExp(String queryString, String name, ArrayList<String> category) {
	    if((category.isEmpty()) | (category.get(0).equals(emptyString)) |(category.get(0).equals("''"))) {
	    	return(queryString);
	    }
	    String value = category.get(0);
	    String logic = category.get(1);
	    if(firstCondition) {
	        queryString += " AND " + name + " " + logic + " '" + value + "' ";
	        }
	    else {
	        queryString += " WHERE ";
	        queryString += "(" +  name + ' ' + logic + " '" + value + "' ";
	        firstCondition = true;
	        }
	    
	    //If the name is update_date, then we should make sure that the mat > update. (not used)
		String updateCheck = "update_date_d";
		if(name.equals(updateCheck)) {
			queryString += " and maturity_date_d > update_date_d ";
		}
		
	    return (queryString);

}
	//Takes a list and filters the table if a specific value is in the tuple. 
	//For example bond_insurance_code_c in ('BAM','SBAM'). Single tuples use an "=" (bond_insurance_code_c = 'BAM')
	public String addTuple(String queryString, String name, ArrayList<String> category) {
		String operator = " AND ";
		if(category.isEmpty()) {
			return queryString;
		}
		
		StringBuilder str = new StringBuilder(category.toString());
		str.setCharAt(0,'(');
		str.setCharAt(str.length() - 1,')');
	    if(category.size() == 1) {
	        if(firstCondition) {
	           queryString += operator + name + " = '" + category.get(0) + "' " ;
	        
	        }
	        else {
	            queryString += " WHERE " ;
	            queryString +="(" + name + " = '" +  category.get(0) + "' ";
	            firstCondition = true;
	        }
	    
	    }
	    
	    else {
	    if(firstCondition) {
	        queryString += operator + name + " IN " + str.toString() + ' ';
	    }
	    else {
	        queryString += " WHERE ";
	        System.out.println(str.toString());
	        queryString += "(" + name + " IN " + str.toString() + ' ';
	        firstCondition = true;

	    }
	}
	    
		return(queryString);
	}
	
	//This is a partialMatch string. We use the OR LIKE methodology. 
	public String partialMatchStrings(String queryString, String name, ArrayList<String> category ) {
		if(firstCondition == false) {
	           queryString += " WHERE ((";
	          firstCondition = true;
		}
		else {
	          queryString += " and (";
		}
         for(int i = 0; i < category.size(); i++) {
      	   if(i == 1) {
	        	  queryString += " " + name + " LIKE " + "'" +   category.get(i) + "%' "; 
      	   }
      	   else {
	 	           queryString += "  or " + name + " LIKE " + category.get(i) + "%' ";  ;
      	   }
         }
         
        queryString += ") "; 	
		return(queryString);
		
	}
	//Follow addTuple but you can modify the operator. You can use "OR" instead of "AND" (default)
	public String addTuple(String queryString, String name, ArrayList<String> category, String operator) {
		if(category.isEmpty()) {
			return queryString;
		}
		
		StringBuilder str = new StringBuilder(category.toString());
		str.setCharAt(0,'(');
		str.setCharAt(str.length() - 1,')');
	    if(category.size() == 1) {
	        if(firstCondition) {
	           queryString += operator + name + " = '" + category.get(0) + "' " ;
	        
	        }
	        else {
	            queryString += " WHERE " ;
	            queryString +="(" + name + " = '" +  category.get(0) + "' ";
	            firstCondition = true;
	        }
	    
	    }
	    
	    else {
	    if(firstCondition) {
	        queryString += operator + name + " IN " + str.toString() + ' ';
	    }
	    else {
	        queryString += " WHERE ";
	        System.out.println(str.toString());
	        queryString += "(" + name + " IN " + str.toString() + ' ';
	        firstCondition = true;

	    }
	}
	    
		return(queryString);
	}

	//Tuple but with "Not In". Used for AGM_Filter
	public String addNOTTuple(String queryString, String name, ArrayList<String> category, String operator) {
		if(category.isEmpty()) {
			return queryString;
		}
		
		StringBuilder str = new StringBuilder(category.toString());
		str.setCharAt(0,'(');
		str.setCharAt(str.length() - 1,')');
	    if(category.size() == 1) {
	        if(firstCondition) {
	           queryString += operator + name + "! = '" + category.get(0) + "' " ;
	        
	        }
	        else {
	            queryString += " WHERE " ;
	            queryString +="(" + name + " != '" +  category.get(0) + "' ";
	            firstCondition = true;
	        }
	    
	    }
	    
	    else {
	    if(firstCondition) {
	        queryString += operator + name + " NOT IN " + str.toString() + ' ';
	    }
	    else {
	        queryString += " WHERE ";
	        System.out.println(str.toString());
	        queryString += "(" + name + " NOT IN " + str.toString() + ' ';
	        firstCondition = true;

	    }
	}
	    
		return(queryString);
	}

	
	//Add string for range condition. Usually dates or numbers. Takes a name and a range which is an ArrayList<String> of length 2. At least one is not blank.
	public String addRangeTuple(String queryString, String name, ArrayList<String> category) {
		
		if(category.get(0).equals(dateDef) |category.get(0).equals(spinDef) |category.get(0).equals(emptyString)) {
		    String str = category.get(1);
			category.set(0,str);
			category.set(1, "<=");
	       
			return(addLogicExp(queryString, name, category));
		}
		if(category.get(1).equals(dateDef) |category.get(1).equals(spinDef) | category.get(1).equals(emptyString) ) {
				category.set(1, ">=");
		       return(addLogicExp(queryString, name, category));
			}
		if(firstCondition) {
	        queryString += " AND " + name + " BETWEEN '" + category.get(0) + "' AND '" + category.get(1) + "' ";

		}
		else {
	        queryString += " WHERE "; 
	        queryString += "(" + name + " BETWEEN '"+ category.get(0) + "' AND '" + category.get(1) + "' ";
	        firstCondition = true;

		}
		
	    //If the name is update_date, then we should make sure that the mat > update. (used)
		String updateCheck = "update_date_d";
		if(name.equals(updateCheck)) {
			queryString += " and maturity_date_d > update_date_d ";
		}
	    return(queryString);

	}
	
	//Main function used to construct SQL Queries
	//Condition is a boolean value that indicates if a previous condition on the query has been made, this changes the condstruction of the query from 
	//adding a (WHERE..... to AND....
	public String stringAdd(String queryString, String name, ArrayList<String> category) {
		System.out.println("Category Before" + category.toString());
			
		if(category.isEmpty()) {
			return(queryString);
		}
		String type = category.get(category.size() -1);
		String rangeIndicator = "R";
		
		ArrayList<String> categoryCopy = new ArrayList<String>(category);
		boolean range = false;
		if(type.equals(rangeIndicator)) {
			range = true;
			categoryCopy.remove(categoryCopy.size()-1);
		}
		
		if(range) {
			String result = addRangeTuple(queryString, name, categoryCopy);
			System.out.println("Category After" + category.toString());
	       return(result);
		}
		String result = addTuple(queryString, name, categoryCopy);
		System.out.println("Category After" + category.toString());
        return(result);
	}
	
	
	
	public String addLimit(String queryString, int queryLimit) {
	    queryString += " Limit " + queryLimit;
	    return(queryString);
	    
	}
	//Use Default
	public String addLimit(String queryString) {
	    queryString += " Limit " + queryLimit;
	    return(queryString);
	    
	}
	
	//Drop TempTable
	public String dropTempTableString(String TempTableName) {
		return(String.format("drop temporary table if exists %s;", TempTableName));

	}
	

	

}


class msrbQueries extends sqlQueries {
	public msrbQueries() {
		// TODO Auto-generated constructor stub
	}

		//Given the dictionary, iterate and construct the conditions to add to queryString
	public String getMSRBData(HashMap<String, ArrayList<String>> dict,boolean haveLimit) {
	    System.out.println("GETTING MSRB DATA");
	    
	    
		String queryString = "SELECT cusip, trade_type, sec_desc, dated_date, coupon, maturity_date, trade_date, trade_time, settle_date, par_traded, dollar_price, yield FROM msrb_db.rtrs";	    
	    firstCondition = false;
	   
	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	    }
	    return(queryString);
	
	}
	
	///TCV Data (Not currently used in algos
	public String getMSRBDataForTCV(ArrayList<ArrayList<String>> tradingArgs, ArrayList<String> tradingArgsKeys, boolean haveLimit) {
	    String queryString = "SELECT cusip, trade_type, sec_desc, dated_date, coupon, maturity_date, trade_date, trade_time, settle_date, par_traded, dollar_price, yield FROM msrb_db.rtrs";
	    firstCondition = false;
	    for (int counter = 0; counter < tradingArgs.size(); counter++) {
	    	
	    	///For cusip6 use partialMatch
	    	if(tradingArgsKeys.get(counter).equals("cusip_6")) {
	    		queryString =  partialMatchStrings(queryString,"cusip  ",tradingArgs.get(counter));
	    	}
	    	else if (tradingArgsKeys.get(counter).equals("cusip")) {
	    		queryString = addTuple(queryString,tradingArgsKeys.get(counter),tradingArgs.get(counter), " OR ");
	    	}
	    	
	    	else {
		    	queryString = stringAdd(queryString,tradingArgsKeys.get(counter),tradingArgs.get(counter));
	    	}
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	    }
	    return(queryString);
	
		
	}

	///Grabs list of all insured cusip9s from MSRB (Not used)
	public String getAllInsuredCusips(int queryLimit) {
	    String queryString = "Select cusip9 from msrb_db.all_insured_cusips";
	    queryString += " Limit " + queryLimit;
	    return(queryString);
		
	}
	
	//Grabs list of all BAM insured cusip9 from MSRB (Not used)
	public String getBamInsuredCusips(int queryLimit) {
	    String queryString = "Select cusip9 from msrb_db.bam_insured_cusips";
	    queryString += " Limit " + queryLimit;
	    return(queryString);

	}
		    
	
	//Generate temp table by selecting cusips
	public String genCusipTempTableFromSql(String TempTableName, String queryString) {
		
		String tempCreate  = "create temporary table if not exists " +  TempTableName +  "as select cusip ";
		int posFROM = queryString.indexOf("FROM");
        String afterFROM = queryString.substring(posFROM, queryString.length() - 1);
        return(tempCreate + afterFROM);   

	}
	
	//Used to filer the issue_id_i, maturity_id_i, cusip, cusip_6 from cusip filter
	public String getIssueMaturityFromCusip(String queryString, String TempTableName ) {
		String tempTableString = "create temporary table if not exists " +  TempTableName +  " as select DISTINCT issue_id_i,  maturity_id_i, cusip_c, cusip_6 FROM " + mergentName + ".bondinfo inner join msrb_db.rtrs on " + mergentName + ".bondinfo.cusip_c = msrb_db.rtrs.cusip ";
		int posWHERE = queryString.indexOf("WHERE");
		
        String afterWHERE= queryString.substring(posWHERE);
        return(tempTableString + afterWHERE);		
        
	}
	
	//Here we create a second table from the temp of just the cusips
	//Then we merge our results with the msrb query. 
	//We then use this filter
	public ArrayList<String> genCusipTableFromBondInfo(String queryString, String tempTableName) {
	
		String tempTableName2 = tempTableName + "2";
		String getMergentCusips = "SELECT  cusip_c  FROM  " + tempTableName;
		String createTempTable2  = "create temporary table if not exists " +  tempTableName2 +  " as  "  + getMergentCusips;

		int posWHERE = queryString.indexOf("WHERE");
		String joinString = " inner join " + tempTableName2 + " on msrb_db.rtrs.cusip = " + tempTableName2 + ".cusip_c ";
		String beforeWHERE = queryString;
		String afterWhere = "";
		if(posWHERE != -1) {
    		beforeWHERE= queryString.substring(0, posWHERE);
    		afterWhere = queryString.substring(posWHERE);
        }
		String qString = beforeWHERE + joinString + afterWhere;
		qString += " ORDER BY trade_date desc "; 
		qString = addLimit(qString,500000);
		String dropTempTable = "drop temporary table if exists " + tempTableName2;

        ArrayList<String> queries = new ArrayList<String>();
        queries.add(createTempTable2);
        queries.add(qString);
        queries.add(dropTempTable);
        return(queries);
    	
	}
	
	public ArrayList<String> genCusipFilter(String queryString, String tempTableName ) {
		String tempTableName2 = tempTableName + "2";
		String tempCreate  = "create temporary table if not exists " +  tempTableName2 +  " as select distinct "  +  tempTableName + "." + "issue_id_i, " + tempTableName + ".maturity_id_i, " + tempTableName + ".cusip_c, " + tempTableName + ".cusip_6 from " + tempTableName;
		int posWHERE = queryString.indexOf("WHERE");
		String joinString = " inner join msrb_db.rtrs" + " on " + "msrb_db.rtrs.cusip = " + tempTableName + ".cusip_c "; 
		String afterWHERE= queryString.substring(posWHERE);
		String tempCreateFinal = tempCreate +  joinString + " " + afterWHERE;
	
		
		String dropTempTable = "drop temporary table if exists " + tempTableName;
		String createTempTable = "create temporary table if not exists " + tempTableName + " as select distinct issue_id_i, maturity_id_i, cusip_c, cusip_6 FROM " + tempTableName2;
		String dropTempTable2 = "drop temporary table if exists " + tempTableName2;
		ArrayList<String> queries  = new ArrayList<String>();
		queries.add(tempCreateFinal);
		queries.add(dropTempTable);
		queries.add(createTempTable);
		queries.add(dropTempTable2);
		
		return(queries);
		
	}
		
}

class mergentQueries extends sqlQueries {

	
	public mergentQueries() {
		// TODO Auto-generated constructor stub
	}
	
	//These construct queries for each table in mergent
	public String getVarRateData(HashMap<String, ArrayList<String>> dict,  boolean haveLimit) {
	    String queryString = "SELECT " + mergentName + ".varrate.* FROM "+ mergentName + ".varrate";
	    firstCondition = false;
	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
	    return(queryString);
	}

	public String getPartRedemptnData( HashMap<String, ArrayList<String>> dict,  boolean haveLimit) {
	    String queryString = "SELECT " + mergentName +  ".partredm.* FROM "+ mergentName +  ".partredm";
	    firstCondition = false;

	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
	    return(queryString);
	}

	public String getRedemptnData(HashMap<String, ArrayList<String>> dict,  boolean haveLimit) {
	    String queryString = "SELECT " + mergentName + ".redemptn.* FROM "+ mergentName + ".redemptn";
	    firstCondition = false;
	    
	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
	    return(queryString);
	}
	
	public String getSinkFundData( HashMap<String, ArrayList<String>> dict,  boolean haveLimit) {
	    String queryString = "SELECT " + mergentName + ".sinkfund.* FROM "+ mergentName + ".sinkfund";
	    firstCondition = false;
	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
	    return(queryString);
	}

	
	public String getAddlCreditQuery( HashMap<String, ArrayList<String>> dict,  boolean haveLimit) {
	    String queryString = "SELECT " + mergentName + ".addlcred.* FROM "+ mergentName + ".addlcred";
	    firstCondition = false;
	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
	    return(queryString);
	}
	
	public String getCallScheduleQuery( HashMap<String, ArrayList<String>> dict,  boolean haveLimit) {
	    String queryString = "SELECT " +  mergentName + ".callschd.* FROM "+ mergentName + ".callschd";
	    firstCondition = false;
	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
	    return(queryString);
	}

	public String getIssueInfoQuery( HashMap<String, ArrayList<String>> dict,  boolean haveLimit) {
	    String queryString = "SELECT " + mergentName + ".issueinfo.* FROM "+ mergentName + ".issueinfo";
	    firstCondition = false;
	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
	    return(queryString);
	}

	public String getBondInfoQuery( HashMap<String, ArrayList<String>> dict,  boolean haveLimit) {
	    String queryString = "SELECT mergent_db2.bondinfo.issue_id_i,mergent_db2.bondinfo.maturity_id_i,mergent_db2.bondinfo.cusip_c,mergent_db2.bondinfo.cusip_6, prior_cusip_c, coupon_f,update_date_d, maturity_date_d,	settlement_date_d,	maturity_amount_f, bond_insurance_code_c,	series_code_c,	active_maturity_flag_i,coupon_code_c,	debt_type_c,	offering_price_f,	offering_yield_f,	total_maturity_offering_amt_f,	tot_mat_amt_outstanding_f,	tot_mat_amt_outstanding_date_d,	additional_credit_i,addl_credit_schedule_num_i,	series_c,	default_flag_i,	dfrd_int_cnvrsn_date_d,	put_flag_i,	optional_call_flag_i,	call_schedule_number_i,	redemption_flag_i,	prtl_redemption_flag_i,	reoffered_i,	reoffered_yield_f,	reoffered_date_d,	material_event_flag_i,	capital_purpose_c,	tax_code_c,	state_tax_i,	bank_qualified_i,	orig_cusip_status_i,	orig_cusip_type_i,		cusip_change_reason_c,	cusip_change_date_d,	project_name_c,	use_of_proceeds_c,	security_code_i,sink_fund_type_i,super_sinker_flag_i,registration_type_i,average_life_date_d,dated_date_d,delivery_date_d,	interest_calc_code_i,first_coupon_date_d,interest_frequency_i,	interest_accrual_date_d,depository_type_i,	denomination_amount_f, mtg_insurance_code_c, note_c,isin_c,	cav_maturity_amt_f,	next_par_call_date_d,next_par_call_price_f,	next_call_date_d,	next_call_price_f,	sf_accel_pct_f, redeem_method_c,	sinking_fund_allocation_i,	source_of_repayment_i,	next_int_pay_date_d,	last_int_pay_date_d,rule_144a_i,seniority_c  FROM "+ mergentName + ".bondinfo";
	    firstCondition = false;
	    for (String key : dict.keySet()) {
	    	queryString = stringAdd(queryString,key,dict.get(key));
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
	    return(queryString);
	}

	//Extended BondInfo takes arguments from BondInfo and IssueInfo
	public String getExtendedBondInfoQuery( HashMap<String, ArrayList<String>> bondInfoDict, HashMap<String, ArrayList<String>> issueInfoDict,  boolean haveLimit) {
	    String queryString = "SELECT mergent_db2.bondinfo.issue_id_i,mergent_db2.bondinfo.maturity_id_i,mergent_db2.bondinfo.cusip_c,mergent_db2.bondinfo.cusip_6, prior_cusip_c, coupon_f,update_date_d, maturity_date_d,	settlement_date_d,	maturity_amount_f, bond_insurance_code_c,	series_code_c,	active_maturity_flag_i,coupon_code_c,	debt_type_c,	offering_price_f,	offering_yield_f,	total_maturity_offering_amt_f,	tot_mat_amt_outstanding_f,	tot_mat_amt_outstanding_date_d,	additional_credit_i,addl_credit_schedule_num_i,	series_c,	default_flag_i,	dfrd_int_cnvrsn_date_d,	put_flag_i,	optional_call_flag_i,	call_schedule_number_i,	redemption_flag_i,	prtl_redemption_flag_i,	reoffered_i,	reoffered_yield_f,	reoffered_date_d,	material_event_flag_i,	capital_purpose_c,	tax_code_c,	state_tax_i,	bank_qualified_i,	orig_cusip_status_i,	orig_cusip_type_i,		cusip_change_reason_c,	cusip_change_date_d,	project_name_c,	use_of_proceeds_c,	security_code_i,sink_fund_type_i,super_sinker_flag_i,registration_type_i,average_life_date_d,dated_date_d,delivery_date_d,	interest_calc_code_i,first_coupon_date_d,interest_frequency_i,	interest_accrual_date_d,depository_type_i,	denomination_amount_f, mtg_insurance_code_c, note_c,isin_c,	cav_maturity_amt_f,	next_par_call_date_d,next_par_call_price_f,	next_call_date_d,	next_call_price_f,	sf_accel_pct_f, redeem_method_c,	sinking_fund_allocation_i,	source_of_repayment_i,	next_int_pay_date_d,	last_int_pay_date_d,rule_144a_i,seniority_c  FROM "+ mergentName + ".bondinfo";
	    queryString += " inner join " + mergentName + ".issueinfo on " + mergentName + ".bondinfo.issue_id_i  = " + mergentName + ".issueinfo.issue_id_i ";
	    firstCondition = false;
	    for (String key : bondInfoDict.keySet()) {
	    	queryString = stringAdd(queryString,key,bondInfoDict.get(key));
	    	
	    }
	    for (String key : issueInfoDict.keySet()) {
	    	queryString = stringAdd(queryString,key,issueInfoDict.get(key));
	    	
	    }
	    
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	    
	    }
	    
	    return(queryString);
	}

//Quick Bond Works like regular BondInfo, but has conditions for certain keys
	public String getQuickBondInfoQuery(HashMap<String, ArrayList<String>> bondInfoDict, boolean haveLimit) {
	    String queryString = "SELECT mergent_db2.bondinfo.issue_id_i,mergent_db2.bondinfo.maturity_id_i,mergent_db2.bondinfo.cusip_c,mergent_db2.bondinfo.cusip_6, 	prior_cusip_c, coupon_f,update_date_d, maturity_date_d,	settlement_date_d,	maturity_amount_f, bond_insurance_code_c,	series_code_c,	active_maturity_flag_i,coupon_code_c,	debt_type_c,	offering_price_f,	offering_yield_f,	total_maturity_offering_amt_f,	tot_mat_amt_outstanding_f,	tot_mat_amt_outstanding_date_d,	additional_credit_i,addl_credit_schedule_num_i,	series_c,	default_flag_i,	dfrd_int_cnvrsn_date_d,	put_flag_i,	optional_call_flag_i,	call_schedule_number_i,	redemption_flag_i,	prtl_redemption_flag_i,	reoffered_i,	reoffered_yield_f,	reoffered_date_d,	material_event_flag_i,	capital_purpose_c,	tax_code_c,	state_tax_i,	bank_qualified_i,	orig_cusip_status_i,	orig_cusip_type_i,	cusip_change_reason_c,	cusip_change_date_d,	project_name_c,	use_of_proceeds_c,	security_code_i,sink_fund_type_i,super_sinker_flag_i,registration_type_i,average_life_date_d,dated_date_d,delivery_date_d,	interest_calc_code_i,first_coupon_date_d,interest_frequency_i,	interest_accrual_date_d,depository_type_i,	denomination_amount_f, mtg_insurance_code_c, note_c,isin_c,	cav_maturity_amt_f,	next_par_call_date_d,next_par_call_price_f,	next_call_date_d,	next_call_price_f,	sf_accel_pct_f, redeem_method_c,	sinking_fund_allocation_i,	source_of_repayment_i,	next_int_pay_date_d,	last_int_pay_date_d,rule_144a_i,seniority_c  FROM "+ mergentName + ".bondinfo";
	    firstCondition = false;
	    
	    for (String key : bondInfoDict.keySet()) {

	    	if(key.equals("cusip_6" )) {
	    		queryString = addTuple(queryString,key,bondInfoDict.get(key), " OR ");
	    	}
	    	else if (key.equals("cusip_c")) {
	    		queryString = addTuple(queryString,key,bondInfoDict.get(key), " OR ");
	    	}
	    	
	    	else if (key.equals("agm_filter")) {
	    		queryString = addNOTTuple(queryString,"cusip_c",bondInfoDict.get(key), " AND ");
	    	}
	    	else {
	    		queryString = stringAdd(queryString,key,bondInfoDict.get(key));

	    	}
	    	
	    }
	    if(firstCondition) {
	    	queryString += ")";
	    }
	    
	    if(haveLimit) {
	        queryString = addLimit(queryString,queryLimit);
	
	    
	    }
		return(queryString);
		
	}
	
	public String getCodeDict(String code) {
	    return("SELECT code_c,code_desc_c FROM " + mergentName + ".codes WHERE code_col_name_c = '" + code + "' ORDER BY code_desc_c ASC");
	}
	    
	public String getStates() {
	    return(getCodeDict("STATE_C"));
	}
	public String getUseProceeds() {
	    return(getCodeDict("use_of_proceeds_c"));
	}
	public String getBondInsurance() {
	    return(getCodeDict("BOND_INSURANCE_CODE_C"));
	}
	public String getCouponCode() {
	    return(getCodeDict("COUPON_CODE_C"));
	}
	public String getDebtType() {
	    return(getCodeDict("DEBT_TYPE_C"));
	}
	
	public String getAddlCredit() {
	    return(getCodeDict("ADDL_CREDIT_TYPE_CODE_C"));
	}
	
	public String getSPLongRating() {
	    return(getCodeDict("SP_LONG_RATING_C"));
	}
	public String getSPShortRating() {
	    return(getCodeDict("SP_SHORT_RATING_C"));
	}
	public String getMoodyLongRating() {
	    return(getCodeDict("MOODY_LONG_RATING_C"));
	}
	public String getMoodyShortRating() {
	    return(getCodeDict("MOODY_Short_RATING_C"));
	}

	
	public String getCapitalPurpose() {
	    return(getCodeDict("CAPITAL_PURPOSE_C"));
	}
	
	public String genMergentTempTableName() {
		return(mergentName + "."  + user + "_temptable"  +  (int)(Math.random()*ALPHABET.length()));

	}
	
	
	public String genIssueTempTableFromSql(String TempTableName, String queryString) {
		
		
		String tempCreate  = "create temporary table if not exists " +  TempTableName +  " as select issue_id_i ";
		int posFROM = queryString.indexOf("FROM");
        String afterFROM = queryString.substring(posFROM);
        return(tempCreate + afterFROM);   

	}
	
	
	public ArrayList<String> genIssueMaturityTempTableFromSql(String TempTableName, String queryString, String mergentTableName ) {
		String tempCreate  = "";
		String b = "bondinfo";
		
		if(mergentTableName.equals(b)) {
				tempCreate  = "create temporary table if not exists " +  TempTableName +  " as select DISTINCT " + mergentName + "." + mergentTableName + "." + "issue_id_i, " + mergentName + "." + mergentTableName + ".maturity_id_i, " + mergentTableName + ".cusip_c,  " +  mergentTableName + ".cusip_6 ";
				int posFROM = queryString.indexOf("FROM");
		        String afterFROM = queryString.substring(posFROM);
		        ArrayList<String> array  = new ArrayList<String>();
		        array.add(tempCreate + afterFROM);
		        return(array);   
		}
		else {
				String TempTableName2 = TempTableName + "2";
			 	tempCreate  = "create temporary table if not exists " +  TempTableName2 +  " as select DISTINCT " + mergentName + "." + mergentTableName + "." + "issue_id_i, " + mergentName + "." + mergentTableName + ".maturity_id_i ";
				int posFROM = queryString.indexOf("FROM");
		        String afterFROM = queryString.substring(posFROM);
		        String firstQuery = tempCreate + afterFROM;
		        String tempCreate2 = "create temporary table if not exists " + TempTableName + " as select DISTINCT " + mergentName  + ".bondinfo.issue_id_i, " + mergentName + ".bondinfo.maturity_id_i, " + mergentName + ".bondinfo.cusip_c, " + mergentName + ".bondinfo.cusip_6  FROM " + mergentName + ".bondinfo ";
		        tempCreate2 += " inner join " + TempTableName2 + " on " + mergentName + "." + "bondinfo.issue_id_i = " + TempTableName2 + ".issue_id_i and " + mergentName + "." + "bondinfo.maturity_id_i = " + TempTableName2 + ".maturity_id_i ";
		        String dropTempCreate2 = "drop temporary table if exists " + TempTableName2;
		        ArrayList<String> array  = new ArrayList<String>();
		        array.add(firstQuery);
		        array.add(tempCreate2);
		        array.add(dropTempCreate2);
		        return(array);

		}

	}
	
	
	public String genIssueMaturityFilterTable(String queryString, String tempTableName, String mergentTableName) {
		int posSELECT = queryString.indexOf("SELECT");
		String b = "bondinfo";
		if(!mergentTableName.equals(b)) {
			String mergeCusip = "SELECT " +  tempTableName + ".cusip_c, " +  tempTableName + ".cusip_6, ";
	        String afterSELECT= queryString.substring(posSELECT + 6);
	        queryString = mergeCusip + afterSELECT;
		}
		
		int posWHERE = queryString.indexOf("WHERE");
		String joinString = " inner join " + tempTableName + " on " + mergentName + "." + mergentTableName + ".issue_id_i = " + tempTableName + ".issue_id_i and " + mergentName + "." + mergentTableName + ".maturity_id_i = " + tempTableName + ".maturity_id_i ";
		if(posWHERE == -1) {
			return(queryString + joinString);
		}
        String beforeWHERE= queryString.substring(0, posWHERE);
        return(beforeWHERE + joinString);
	
		
	}

	
	public String  genIssueFilterTable(String queryString, String tempTableName, String mergentTableName) {
		int posSELECT = queryString.indexOf("SELECT");
		String mergeCusip = "SELECT " +   tempTableName + ".cusip_6, ";
        String afterSELECT= queryString.substring(posSELECT + 6);
        queryString = mergeCusip + afterSELECT;
		
		int posWHERE = queryString.indexOf("WHERE");
		String joinString = " inner join " + tempTableName + " on " + mergentName + "." + mergentTableName + ".issue_id_i = " + tempTableName + ".issue_id_i ";
		if(posWHERE == -1) {
			String tempCreate = queryString + joinString;
			return(tempCreate);
		}
		String beforeWHERE= queryString.substring(0, posWHERE);
        return(beforeWHERE + joinString);
	
	}


	//Filter temp table by creating another one and replacing the existing one with it.
	public ArrayList<String> filterTempTable(String queryString, String tempTableName, String tableName) {
		String tempTableName2 = tempTableName + "2";
		String tempCreate  = "create temporary table if not exists " +  tempTableName2 +  " as select distinct "  +  tempTableName + "." + "issue_id_i, " + tempTableName + ".maturity_id_i, " + tempTableName + ".cusip_c, " + tempTableName + ".cusip_6 from " + mergentName + "." + tableName;
		int posWHERE = queryString.indexOf("WHERE");
		String joinString = " inner join " + tempTableName + " on " + tempTableName + ".issue_id_i = " + mergentName + "." + tableName + ".issue_id_i and " + tempTableName + ".maturity_id_i = " + mergentName + "." + tableName + ".maturity_id_i ";
		String afterWHERE= queryString.substring(posWHERE);
		
		String tempCreateFinal = tempCreate +  joinString + " " + afterWHERE;
	
		
		String dropTempTable = "drop temporary table if exists " + tempTableName;
		String createTempTable = "create temporary table if not exists " + tempTableName + " as select distinct issue_id_i, maturity_id_i, cusip_c, cusip_6 FROM " + tempTableName2;
		String dropTempTable2 = "drop temporary table if exists " + tempTableName2;
		ArrayList<String> queries  = new ArrayList<String>();
		queries.add(tempCreateFinal);
		queries.add(dropTempTable);
		queries.add(createTempTable);
		queries.add(dropTempTable2);
		return(queries);

	}
	
	public String genIssueTempTableName() {
		return(mergentName + "."  + user + "_Issuetemptable"  +  (int)(Math.random()*ALPHABET.length()));

	}
	

	public String createEmptyIssueTempTable(String TempTableName) {
		return(String.format("create temporary table if not exists %s (issue_id decimal(10,0)));", TempTableName));

	}
	
	public String createEmptyCusipTempTable(String TempTableName) {
		return(	String.format("create temporary table if not exists %s (cusip9 char(9));", TempTableName));

	}

	public String createEmptyIssueMaturityTempTable(String TempTableName) {
		return(String.format("create temporary table if not exists %s (issue_id decimal(10,0),maturity_id decimal(10,0));" , TempTableName));

	}
	
	public ArrayList<String>  genTempTableMergentFromCusipList(ArrayList<String> cusip) {
		StringBuilder str = new StringBuilder();
		for(String cusip9 : cusip) {
			str.append("('"+ cusip9 + "'),");
		}
		String cusipString = str.substring(0,str.length() - 1);
		String TempTableName = genMergentTempTableName();
		String queryString  = String.format("Insert INTO " + TempTableName + " VALUES  %s;", cusipString);
		String DropTable = dropTempTableString(TempTableName);
		String CreateTable = createEmptyCusipTempTable(TempTableName);
		
		ArrayList<String> tempTableQueries = new ArrayList<String>();
		tempTableQueries.add(TempTableName);
		tempTableQueries.add(DropTable);
		tempTableQueries.add(CreateTable);
		tempTableQueries.add(queryString);

		return(tempTableQueries);
				
				
				
	}

	public ArrayList<String>  genIssueTempTableFromIssueIDs(ArrayList<String> issueID) {
		StringBuilder str = new StringBuilder();
		for(String issue : issueID) {
			str.append("('"+ issue + "'),");
		}
		String issueString = str.substring(0,str.length() - 1);
		String TempTableName = genIssueTempTableName();
		String queryString  = String.format("Insert INTO " + TempTableName + " VALUES  %s;", issueString);
		String DropTable = dropTempTableString(TempTableName);
		String CreateTable = createEmptyIssueTempTable(TempTableName);
		
		ArrayList<String> tempTableQueries = new ArrayList<String>();
		tempTableQueries.add(TempTableName);
		tempTableQueries.add(DropTable);
		tempTableQueries.add(CreateTable);
		tempTableQueries.add(queryString);

		return(tempTableQueries);
				
				
				
	}

	

	
}