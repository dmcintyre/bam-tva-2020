package tvagui;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.swing.JFileChooser;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.nebula.widgets.datechooser.DateChooserCombo;
import org.eclipse.nebula.widgets.opal.switchbutton.SwitchButton;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.SWTResourceManager;
import org.json.simple.parser.ParseException;

import com.bamcore.util.BAMBOBException;
import com.bamcore.util.BD2;


import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.columns.Column;
import tech.tablesaw.io.csv.CsvReadOptions;
import tech.tablesaw.io.csv.CsvWriteOptions;
import tech.tablesaw.selection.Selection;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.layout.RowData;

public class guiTVA extends Composite  {

	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	private Text txtQuickBondCusipSearch;
	private Text txtTCVCusipSearch;
	
	
	
	String dateDef = " / /    ";
	int spinDef = 0;
	String listDef = "[]";
	String cusipDef = "";
	
	DateChooserCombo minMatDate;
	DateChooserCombo maxMatDate;
	DateChooserCombo maxDatedDate;
	DateChooserCombo minDatedDate;
	Spinner minCoupon; 
	Spinner maxCoupon; 
	Spinner minYield; 
	Spinner maxYield; 
	Spinner minParAmount;
	Spinner maxParAmount;
	Spinner minPrice;
	Spinner maxPrice;


	Button btnTaxExempt;
	Button btnNotTaxExempt;
	Button btnAMT;
	Button btnNoBankQual;
	Button btnYesBankQual;
	
	List couponCodeList;
	List debtTypeList;
	List stateList;
	List useProceedsList;
	List insuranceList;
	List addlCreditList;
	List capitalPurposeList;
	List bondInfoMoodyRatingList;
	List bondInfoSPRatingList;
	
	Button btnFilterCusips;
	
	ArrayList<ArrayList<String>> couponCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> couponNames  = new ArrayList<String>();

	
	ArrayList<ArrayList<String>> debtTypeCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> debtTypeNames  = new ArrayList<String>();

	ArrayList<ArrayList<String>> stateCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> stateNames  = new ArrayList<String>();
	
	
	ArrayList<ArrayList<String>> useProceedsCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> useProceedsNames  = new ArrayList<String>();

	ArrayList<ArrayList<String>> bondInsuranceCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> bondInsuranceNames  = new ArrayList<String>();
	
	ArrayList<ArrayList<String>> addlCreditCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> addlCreditNames  = new ArrayList<String>();
	
	
	ArrayList<ArrayList<String>> capitalPurposeCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> capitalPurposeNames  = new ArrayList<String>();
	
	

	Spinner minCurrIntRate;
	Spinner maxCurrIntRate;
	
	Spinner minNextCallPrice;
	Spinner maxNextCallPrice;
	DateChooserCombo minNextCallDate;
	DateChooserCombo maxNextCallDate;

	
	DateChooserCombo minCallDate;
	DateChooserCombo maxCallDate;
	Spinner minCallPrice;
	Spinner maxCallPrice;
	
	DateChooserCombo minPartialCallDate;
	DateChooserCombo maxPartialCallDate;
	
	DateChooserCombo maxUpdatedDate;
	DateChooserCombo minUpdatedDate;

	DateChooserCombo minSinkDate;
	DateChooserCombo maxSinkDate;
	
	Spinner maxPartialCallRate;
	Spinner minPartialCallRate;
	Spinner minSinkPrice;
	Spinner maxSinkPrice;
	DateChooserCombo minRedemptionDate;
	DateChooserCombo maxRedemptionDate;
	Spinner minRedemptionPrice;
	Spinner maxRedemptionPrice;

	
	SwitchButton btnActiveMaturities;
	
	Spinner minParAmountTraded;
	Spinner maxParAmountTraded;
	DateChooserCombo minTradeDate;
	DateChooserCombo maxTradeDate;
	Spinner minTradedYield;
	Spinner maxTradedYield;

	Spinner minParDollarPrice;
	Spinner maxParDollarPrice;

	Button btnCalculateTradingYield;
	Button btnTradingChecks;
	Button btnUploadMmd;
	Button btnTradeTypeD;
	Button btnTradeTypeS;
	Button btnTradeTypeP;
	
	

	
	
	SwitchButton btnQuickBondActiveMaturities;
	Button btnQuickBondFilterCusips;

	
	Spinner minQuickBondParAmountTraded;
	Spinner maxQuickBondParAmountTraded;
	
	DateChooserCombo minQuickBondTradeDate;
	DateChooserCombo maxQuickBondTradeDate;
	DateChooserCombo minQuickBondMaturityDate;
	DateChooserCombo maxQuickBondMaturityDate;

	Spinner minQuickBondCoupon;
	Spinner maxQuickBondCoupon;
	
	Spinner minQuickBondTradedYield;
	Spinner maxQuickBondTradedYield;

	Spinner minQuickBondParDollarPrice;
	Spinner maxQuickBondParDollarPrice;

	Button btnQuickBondCalculateTradingYield;
	Button btnQuickBondTradingChecks;
	Button btnQuickBondUploadMmd;


	
	Button btnQuickBondCusipList;
	Button btnQuickBondTradeTypeD;
	Button btnQuickBondTradeTypeS;
	Button btnQuickBondTradeTypeP;

	
	
	Spinner minTCVParAmountTraded;
	Spinner maxTCVParAmountTraded;
	DateChooserCombo minTCVTradeDate;
	DateChooserCombo maxTCVTradeDate;
	
	DateChooserCombo minTCVMaturityDate;
	DateChooserCombo maxTCVMaturityDate;
	
	
	Spinner minTCVCoupon;
	Spinner maxTCVCoupon;
	
	Spinner minTCVTradedYield;
	Spinner maxTCVTradedYield;

	Spinner minTCVParDollarPrice;
	Spinner maxTCVParDollarPrice;
	Button btnTCVUploadMmd;
	Button btnTCVCusipList;
	Button btnTCVTradeTypeD;
	Button btnTCVTradeTypeS;
	Button btnTCVTradeTypeP;
	
	
	DateChooserCombo minCheapTradeMatDate;
	DateChooserCombo maxCheapTradeMatDate;
	DateChooserCombo maxCheapTradeDatedDate;
	DateChooserCombo minCheapTradeDatedDate;
	Spinner minCheapTradeCoupon; 
	Spinner maxCheapTradeCoupon; 
	Spinner minCheapTradeYield; 
	Spinner maxCheapTradeYield; 
	Spinner minCheapTradeParAmount;
	Spinner maxCheapTradeParAmount;
	Spinner minCheapTradePrice;
	Spinner maxCheapTradePrice;

	Spinner minCheapTradeTradeCoupon;
	Spinner maxCheapTradeTradeCoupon;
	
	Button btnCheapTradeTaxExempt;
	Button btnCheapTradeNotTaxExempt;
	Button btnCheapTradeAMT;
	Button btnCheapTradeNoBankQual;
	Button btnCheapTradeYesBankQual;

	
	
	List CheapTradeCouponCodeList;
	List CheapTradeDebtTypeList;
	List CheapTradeStateList;
	List CheapTradeUseProceedsList;
	List CheapTradeInsuranceList;
	List spRatingList;
	List moodyRatingList;
	List CheapTradeAddlCreditList;
	List CheapTradeCapitalPurposeList;
	
	Spinner minCheapTradeNextCallPrice;
	Spinner maxCheapTradeNextCallPrice;
	DateChooserCombo minCheapTradeNextCallDate;
	DateChooserCombo maxCheapTradeNextCallDate;
	
	
	DateChooserCombo maxCheapTradeUpdatedDate;
	DateChooserCombo minCheapTradeUpdatedDate;

	
	SwitchButton btnCheapTradeActiveMaturities;

	
	ArrayList<ArrayList<String>> spRatingCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> spRatingNames  = new ArrayList<String>();

	
	ArrayList<ArrayList<String>> moodyRatingCodes = new ArrayList<ArrayList<String>>();
	ArrayList<String> moodyRatingNames  = new ArrayList<String>();
	
	
	
	Spinner minCheapTradeParAmountTraded;
	Spinner maxCheapTradeParAmountTraded;
	DateChooserCombo minCheapTradeTradeDate;
	DateChooserCombo maxCheapTradeTradeDate;
	Spinner minCheapTradeTradedYield;
	Spinner maxCheapTradeTradedYield;

	Spinner minCheapTradeParDollarPrice;
	Spinner maxCheapTradeParDollarPrice;

	Button btnCheapTradeCalculateTradingYield;
	Button btnCheapTradeUploadMmd;
	Button btnCheapTradeTradeTypeD;
	Button btnCheapTradeTradeTypeS;
	Button btnCheapTradeTradeTypeP;
	
	Spinner maxMMDSpread;
	Spinner minMMDSpread;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 * @throws SQLException 
	 */
	public guiTVA(Composite parent, int style) throws SQLException {
		super(parent, SWT.BORDER);

		
		tableMethods t = new tableMethods();
		t.getStateCodes(stateCodes,stateNames);
		t.getBondInsuranceCodes(bondInsuranceCodes, bondInsuranceNames);
		t.getCouponCodes(couponCodes, couponNames);
		t.getDebtTypeCodes(debtTypeCodes, debtTypeNames);
		t.getUseProceedsCodes(useProceedsCodes, useProceedsNames);
		t.getSPRatingCodes(spRatingCodes, spRatingNames);
		t.getMoodyRatingCodes(moodyRatingCodes, moodyRatingNames);
		t.getAdditionalCreditCodes(addlCreditCodes, addlCreditNames);
		t.getCapitalPurposeCodes(capitalPurposeCodes,capitalPurposeNames);
		
		String[] stateNameString = stateNames.toArray(new String[stateNames.size()]);
		String[] bondInsuranceNameString = bondInsuranceNames.toArray(new String[bondInsuranceNames.size()]);
		String[] couponCodeString = couponNames.toArray(new String[couponNames.size()]);
		String[] debtTypeString = debtTypeNames.toArray(new String[debtTypeNames.size()]);
		String[] useProceedsString = useProceedsNames.toArray(new String[useProceedsNames.size()]);
		String[] spRatingString = spRatingNames.toArray(new String[spRatingNames.size()]);
		String[] moodyRatingString = moodyRatingNames.toArray(new String[moodyRatingNames.size()]);
		String[] addlCreditString = addlCreditNames.toArray(new String[addlCreditNames.size()]);
		String[] capitalPurposeString = capitalPurposeNames.toArray(new String[capitalPurposeNames.size()]);

		
		System.out.println(stateNames.toString());
		setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		setLayout(null);
		
		TabFolder tabFolder = new TabFolder(this, SWT.BORDER);
		tabFolder.setBounds(3, 3, 1574, 990);
		tabFolder.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		tabFolder.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_RED));
		
		TabItem BondInfoTab = new TabItem(tabFolder, SWT.NONE);
		BondInfoTab.setText("Bond Info");
		
		Composite BondInfoComposite = new Composite(tabFolder, SWT.BORDER);
		BondInfoComposite.setLocation(-15, -160);
		BondInfoComposite.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		BondInfoComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		BondInfoTab.setControl(BondInfoComposite);
		formToolkit.paintBordersFor(BondInfoComposite);
		
		Composite MaturityDate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		MaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		
		MaturityDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		MaturityDate.setBounds(50, 23, 211, 114);
		formToolkit.paintBordersFor(MaturityDate);
		
		Label lblMaturityDate_1 = new Label(MaturityDate, SWT.BORDER);
		lblMaturityDate_1.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaturityDate_1.setAlignment(SWT.CENTER);
		lblMaturityDate_1.setBounds(0, 0, 207, 25);
		formToolkit.adapt(lblMaturityDate_1, true, true);
		lblMaturityDate_1.setText("Maturity Date");
		
		Label lblMinMatDate = formToolkit.createLabel(MaturityDate, "Min", SWT.BORDER);
		lblMinMatDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinMatDate.setBounds(0, 36, 38, 25);
		
		Label lblMaxMatDate = formToolkit.createLabel(MaturityDate, "Max", SWT.BORDER);
		lblMaxMatDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxMatDate.setBounds(2, 78, 38, 22);
		
		minMatDate = new DateChooserCombo(MaturityDate, SWT.NONE);
		minMatDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minMatDate.setBounds(72, 37, 112, 22);
		formToolkit.adapt(minMatDate);
		formToolkit.paintBordersFor(minMatDate);
		
		maxMatDate = new DateChooserCombo(MaturityDate, SWT.NONE);
		maxMatDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxMatDate.setBounds(72, 78, 112, 22);
		formToolkit.adapt(maxMatDate);
		formToolkit.paintBordersFor(maxMatDate);
		
		Composite DatedDate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		DatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		DatedDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		DatedDate.setBounds(278, 23, 196, 114);
		formToolkit.paintBordersFor(DatedDate);
		
		Label lblDated_Date = new Label(DatedDate, SWT.NONE);
		lblDated_Date.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblDated_Date.setAlignment(SWT.CENTER);
		lblDated_Date.setLocation(0, 0);
		lblDated_Date.setSize(192, 25);
		lblDated_Date.setText("Dated Date");
		formToolkit.adapt(lblDated_Date, true, true);
		
		Label lblMinDatedDate = new Label(DatedDate, SWT.NONE);
		lblMinDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinDatedDate.setBounds(2, 31, 35, 25);
		formToolkit.adapt(lblMinDatedDate, true, true);
		lblMinDatedDate.setText("Min");
		
		Label lblMaxDatedDate = new Label(DatedDate, SWT.NONE);
		lblMaxDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxDatedDate.setText("Max");
		lblMaxDatedDate.setBounds(2, 75, 35, 24);
		formToolkit.adapt(lblMaxDatedDate, true, true);
		
		maxDatedDate = new DateChooserCombo(DatedDate, SWT.NONE);
		maxDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxDatedDate.setBounds(57, 75, 125, 24);
		formToolkit.adapt(maxDatedDate);
		formToolkit.paintBordersFor(maxDatedDate);
		
		minDatedDate = new DateChooserCombo(DatedDate, SWT.NONE);
		minDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minDatedDate.setBounds(57, 30, 125, 24);
		formToolkit.adapt(minDatedDate);
		formToolkit.paintBordersFor(minDatedDate);
		
		Composite Coupon = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		Coupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		Coupon.setBackground(SWTResourceManager.getColor(32, 44, 57));
		Coupon.setBounds(486, 23, 167, 114);
		formToolkit.paintBordersFor(Coupon);
		
		maxCoupon = new Spinner(Coupon, SWT.BORDER);
		maxCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxCoupon.setMaximum(2000000000);
		maxCoupon.setDigits(4);
		maxCoupon.setBounds(54, 83, 99, 22);
		formToolkit.adapt(maxCoupon);
		formToolkit.paintBordersFor(maxCoupon);
		
		Label lblMaxCoupon = new Label(Coupon, SWT.NONE);
		lblMaxCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxCoupon.setBounds(1, 82, 33, 23);
		lblMaxCoupon.setText("Max");
		formToolkit.adapt(lblMaxCoupon, true, true);
		
		Label lblMinCoupon = new Label(Coupon, SWT.NONE);
		lblMinCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinCoupon.setBounds(1, 46, 33, 22);
		lblMinCoupon.setText("Min");
		formToolkit.adapt(lblMinCoupon, true, true);
		
		minCoupon = new Spinner(Coupon, SWT.BORDER | SWT.WRAP);
		minCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minCoupon.setDigits(4);
		minCoupon.setMaximum(2000000000);
		minCoupon.setSelection(0);
		minCoupon.setBounds(54, 46, 99, 22);
		formToolkit.adapt(minCoupon);
		formToolkit.paintBordersFor(minCoupon);
		
		Label lblCoupon = new Label(Coupon, SWT.NONE);
		lblCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblCoupon.setAlignment(SWT.CENTER);
		lblCoupon.setBounds(0, 0, 163, 25);
		lblCoupon.setText("Coupon");
		formToolkit.adapt(lblCoupon, true, true);
		
		
		Composite Yield = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		Yield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		Yield.setBackground(SWTResourceManager.getColor(32, 44, 57));
		Yield.setBounds(659, 23, 167, 114);
		formToolkit.paintBordersFor(Yield);
		
		maxYield = new Spinner(Yield, SWT.BORDER);
		maxYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxYield.setMaximum(2000000000);
		maxYield.setDigits(4);
		maxYield.setBounds(58, 78, 95, 22);
		formToolkit.adapt(maxYield);
		formToolkit.paintBordersFor(maxYield);
		
		Label lblMinYield = new Label(Yield, SWT.NONE);
		lblMinYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinYield.setText("Min");
		lblMinYield.setBounds(0, 41, 33, 22);
		formToolkit.adapt(lblMinYield, true, true);
		
		minYield = new Spinner(Yield, SWT.BORDER | SWT.WRAP);
		minYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minYield.setMaximum(2000000000);
		minYield.setDigits(4);
		minYield.setBounds(58, 42, 95, 22);
		formToolkit.adapt(minYield);
		formToolkit.paintBordersFor(minYield);
		
		Label lblYield = new Label(Yield, SWT.NONE);
		lblYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblYield.setAlignment(SWT.CENTER);
		lblYield.setText("Yield");
		lblYield.setBounds(0, 0, 163, 25);
		formToolkit.adapt(lblYield, true, true);
		
		Label lblMaxYield = new Label(Yield, SWT.NONE);
		lblMaxYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxYield.setText("Max");
		lblMaxYield.setBounds(0, 75, 36, 25);
		formToolkit.adapt(lblMaxYield, true, true);
		
		Composite ParAmount = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		ParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		ParAmount.setBackground(SWTResourceManager.getColor(32, 44, 57));
		ParAmount.setBounds(832, 23, 196, 114);
		formToolkit.paintBordersFor(ParAmount);
		
		Composite Price = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		Price.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		Price.setBounds(1034, 23, 159, 114);
		Price.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(Price);
		
		maxPrice = new Spinner(Price, SWT.BORDER);
		maxPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxPrice.setMaximum(2000000000);
		maxPrice.setDigits(4);
		maxPrice.setBounds(44, 77, 101, 22);
		formToolkit.adapt(maxPrice);
		formToolkit.paintBordersFor(maxPrice);
		
		Label lblMaxPrice = new Label(Price, SWT.NONE);
		lblMaxPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxPrice.setText("Max");
		lblMaxPrice.setBounds(1, 74, 37, 22);
		formToolkit.adapt(lblMaxPrice, true, true);
		
		Label lblMinPrice = new Label(Price, SWT.NONE);
		lblMinPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinPrice.setText("Min");
		lblMinPrice.setBounds(1, 39, 37, 22);
		formToolkit.adapt(lblMinPrice, true, true);
		
		minPrice = new Spinner(Price, SWT.BORDER | SWT.WRAP);
		minPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minPrice.setMaximum(2000000000);
		minPrice.setDigits(4);
		minPrice.setBounds(44, 41, 101, 22);
		formToolkit.adapt(minPrice);
		formToolkit.paintBordersFor(minPrice);
		
		Label lblPrice = new Label(Price, SWT.NONE);
		lblPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblPrice.setAlignment(SWT.CENTER);
		lblPrice.setText("Price");
		lblPrice.setBounds(0, 0, 155, 25);
		formToolkit.adapt(lblPrice, true, true);
		
		maxParAmount = new Spinner(ParAmount, SWT.BORDER);
		maxParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxParAmount.setMaximum(2000000000);
		maxParAmount.setBounds(59, 77, 123, 22);
		formToolkit.adapt(maxParAmount);
		formToolkit.paintBordersFor(maxParAmount);
		
		minParAmount = new Spinner(ParAmount, SWT.BORDER);
		minParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minParAmount.setMaximum(2000000000);
		minParAmount.setBounds(59, 41, 123, 22);
		formToolkit.adapt(minParAmount);
		formToolkit.paintBordersFor(minParAmount);
		
		Label lblParAmount = new Label(ParAmount, SWT.NONE);
		lblParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblParAmount.setAlignment(SWT.CENTER);
		lblParAmount.setText("Par Amount");
		lblParAmount.setBounds(0, 0, 192, 25);
		formToolkit.adapt(lblParAmount, true, true);
		
		Label lblMinParAmount = new Label(ParAmount, SWT.NONE);
		lblMinParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinParAmount.setText("Min");
		lblMinParAmount.setBounds(0, 45, 39, 18);
		formToolkit.adapt(lblMinParAmount, true, true);
		
		Label lblMaxParAmount = new Label(ParAmount, SWT.NONE);
		lblMaxParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxParAmount.setText("Max");
		lblMaxParAmount.setBounds(0, 82, 39, 17);
		formToolkit.adapt(lblMaxParAmount, true, true);
		
		Composite bankQualified = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		bankQualified.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		bankQualified.setBackground(SWTResourceManager.getColor(32, 44, 57));
		bankQualified.setBounds(1353, 23, 120, 114);
		formToolkit.paintBordersFor(bankQualified);
		
		Label lblBankQualified = new Label(bankQualified, SWT.NONE);
		lblBankQualified.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 10, SWT.NORMAL));
		lblBankQualified.setAlignment(SWT.CENTER);
		lblBankQualified.setText("Bank Qualified");
		lblBankQualified.setBounds(0, 0, 116, 25);
		formToolkit.adapt(lblBankQualified, true, true);
		
		btnYesBankQual = new Button(bankQualified, SWT.CHECK);
		btnYesBankQual.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnYesBankQual.setAlignment(SWT.CENTER);
		btnYesBankQual.setBounds(10, 40, 82, 18);
		formToolkit.adapt(btnYesBankQual, true, true);
		btnYesBankQual.setText("Yes");
		
		btnNoBankQual = new Button(bankQualified, SWT.CHECK);
		btnNoBankQual.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnNoBankQual.setAlignment(SWT.CENTER);
		btnNoBankQual.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnNoBankQual.setText("No");
		btnNoBankQual.setBounds(10, 70, 82, 18);
		formToolkit.adapt(btnNoBankQual, true, true);
		
		Composite taxExempt = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		taxExempt.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		taxExempt.setBackground(SWTResourceManager.getColor(32, 44, 57));
		taxExempt.setBounds(1199, 23, 148, 114);
		formToolkit.paintBordersFor(taxExempt);
		
		Label lblTaxExempt = new Label(taxExempt, SWT.NONE);
		lblTaxExempt.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTaxExempt.setAlignment(SWT.CENTER);
		lblTaxExempt.setText("Tax Exempt");
		lblTaxExempt.setBounds(0, 0, 146, 25);
		formToolkit.adapt(lblTaxExempt, true, true);
		
		btnTaxExempt = new Button(taxExempt, SWT.CHECK);
		btnTaxExempt.setAlignment(SWT.CENTER);
		btnTaxExempt.setText("Tax Exempt");
		btnTaxExempt.setBounds(6, 36, 130, 18);
		formToolkit.adapt(btnTaxExempt, true, true);
		
		btnNotTaxExempt = new Button(taxExempt, SWT.CHECK);
		btnNotTaxExempt.setAlignment(SWT.CENTER);
		btnNotTaxExempt.setText("Not Tax Exempt");
		btnNotTaxExempt.setBounds(6, 60, 130, 18);
		formToolkit.adapt(btnNotTaxExempt, true, true);
		
		btnAMT = new Button(taxExempt, SWT.CHECK);
		btnAMT.setAlignment(SWT.CENTER);
		btnAMT.setText("AMT");
		btnAMT.setBounds(6, 84, 130, 18);
		formToolkit.adapt(btnAMT, true, true);
		
		Composite CouponCode = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		CouponCode.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		CouponCode.setBackground(SWTResourceManager.getColor(32, 44, 57));
		CouponCode.setBounds(13, 143, 185, 186);
		formToolkit.paintBordersFor(CouponCode);
		
		Label lblCouponCode = new Label(CouponCode, SWT.BORDER);
		lblCouponCode.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblCouponCode.setAlignment(SWT.CENTER);
		lblCouponCode.setText("Coupon Code");
		lblCouponCode.setBounds(0, 0, 181, 26);
		formToolkit.adapt(lblCouponCode, true, true);
		
		ListViewer couponCodeViewer = new ListViewer(CouponCode, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);

		couponCodeList = couponCodeViewer.getList();
		
		couponCodeList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		
		couponCodeList.setItems(couponCodeString);
		couponCodeList.setBounds(10, 43, 162, 129);
		
		Composite DebtType = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		DebtType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		DebtType.setBackground(SWTResourceManager.getColor(32, 44, 57));
		DebtType.setBounds(204, 143, 273, 186);
		formToolkit.paintBordersFor(DebtType);
		
		Label lblDebtType = new Label(DebtType, SWT.BORDER);
		lblDebtType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblDebtType.setAlignment(SWT.CENTER);
		lblDebtType.setText("Debt Type");
		lblDebtType.setBounds(0, 0, 269, 26);
		formToolkit.adapt(lblDebtType, true, true);
		
		ListViewer debtTypeViewer = new ListViewer(DebtType, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		debtTypeList = debtTypeViewer.getList();
		debtTypeList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		debtTypeList.setItems(debtTypeString);
		debtTypeList.setBounds(10, 45, 249, 127);
		
		Composite stateSelection = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		stateSelection.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		stateSelection.setBackground(SWTResourceManager.getColor(32, 44, 57));
		stateSelection.setBounds(488, 143, 211, 186);
		formToolkit.paintBordersFor(stateSelection);
		
		Label lblState = new Label(stateSelection, SWT.BORDER);
		lblState.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblState.setAlignment(SWT.CENTER);
		lblState.setText("State");
		lblState.setBounds(0, 0, 207, 26);
		formToolkit.adapt(lblState, true, true);
		
		ListViewer stateViewer = new ListViewer(stateSelection, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		stateList = stateViewer.getList();
		stateList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));		
		stateList.setItems(stateNameString);
		stateList.setBounds(10, 45, 187, 127);
		
		Composite UseProceeds = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		UseProceeds.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		UseProceeds.setBackground(SWTResourceManager.getColor(32, 44, 57));
		UseProceeds.setBounds(705, 143, 259, 186);
		formToolkit.paintBordersFor(UseProceeds);
		
		ListViewer useProceedsViewer = new ListViewer(UseProceeds, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		useProceedsList = useProceedsViewer.getList();
		useProceedsList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		useProceedsList.setItems(useProceedsString);
		useProceedsList.setBounds(10, 44, 235, 128);
		
		Label lblUseProceeds = new Label(UseProceeds, SWT.BORDER);
		lblUseProceeds.setText("Use of Proceeds");
		lblUseProceeds.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblUseProceeds.setAlignment(SWT.CENTER);
		lblUseProceeds.setBounds(0, 0, 255, 26);
		formToolkit.adapt(lblUseProceeds, true, true);
		
		Composite Insurance = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		Insurance.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		Insurance.setBackground(SWTResourceManager.getColor(32, 44, 57));
		Insurance.setBounds(970, 143, 259, 186);
		formToolkit.paintBordersFor(Insurance);
		
		Label lblInsurance = new Label(Insurance, SWT.BORDER);
		lblInsurance.setAlignment(SWT.CENTER);
		lblInsurance.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblInsurance.setText("Insurance");
		lblInsurance.setBounds(0, 0, 255, 26);
		formToolkit.adapt(lblInsurance, true, true);
		
		ListViewer insuranceViewer = new ListViewer(Insurance, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI | SWT.H_SCROLL);
		insuranceList = insuranceViewer.getList();
		insuranceList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));		
		insuranceList.setItems(bondInsuranceNameString);
		insuranceList.setBounds(8, 45, 237, 127);
		
		Composite capitalPurposeSelection = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		capitalPurposeSelection.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		capitalPurposeSelection.setBackground(SWTResourceManager.getColor(32, 44, 57));
		capitalPurposeSelection.setBounds(1240, 143, 251, 186);
		formToolkit.paintBordersFor(capitalPurposeSelection);
		
		Label lblCapitalPurpose = new Label(capitalPurposeSelection, SWT.BORDER);
		lblCapitalPurpose.setText("Capital Purpose");
		lblCapitalPurpose.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblCapitalPurpose.setAlignment(SWT.CENTER);
		lblCapitalPurpose.setBounds(0, 1, 247, 26);
		formToolkit.adapt(lblCapitalPurpose, true, true);
		
		ListViewer capitalPurposeViewer = new ListViewer(capitalPurposeSelection, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		capitalPurposeList = capitalPurposeViewer.getList();
		capitalPurposeList.setItems(capitalPurposeString);
		capitalPurposeList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		capitalPurposeList.setBounds(10, 43, 227, 129);
		
		Composite bondInfoMoodyRatings = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		bondInfoMoodyRatings.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		bondInfoMoodyRatings.setBackground(SWTResourceManager.getColor(32, 44, 57));
		bondInfoMoodyRatings.setBounds(496, 602, 96, 163);
		formToolkit.paintBordersFor(bondInfoMoodyRatings);
		
		Label lblBondInfoMoodyRatings = new Label(bondInfoMoodyRatings, SWT.BORDER);
		lblBondInfoMoodyRatings.setAlignment(SWT.CENTER);
		lblBondInfoMoodyRatings.setText("Moodys Ratings");
		lblBondInfoMoodyRatings.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblBondInfoMoodyRatings.setBounds(0, 0, 94, 24);
		formToolkit.adapt(lblBondInfoMoodyRatings, true, true);
		
		ListViewer bondInfoMoodyRatingViewer = new ListViewer(bondInfoMoodyRatings, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		bondInfoMoodyRatingList = bondInfoMoodyRatingViewer.getList();
		bondInfoMoodyRatingList.setItems(moodyRatingString);
		bondInfoMoodyRatingList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		bondInfoMoodyRatingList.setBounds(10, 30, 74, 119);
		
		Composite BondInfoSPRating = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		BondInfoSPRating.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		BondInfoSPRating.setBackground(SWTResourceManager.getColor(32, 44, 57));
		BondInfoSPRating.setBounds(380, 603, 110, 163);
		formToolkit.paintBordersFor(BondInfoSPRating);
		
		Label lblBondInfoSPRating = new Label(BondInfoSPRating, SWT.BORDER | SWT.CENTER);
		lblBondInfoSPRating.setText("SP Rating");
		lblBondInfoSPRating.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblBondInfoSPRating.setBounds(0, 0, 108, 24);
		formToolkit.adapt(lblBondInfoSPRating, true, true);
		
		ListViewer bondInfoSPRatingViewer = new ListViewer(BondInfoSPRating, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		bondInfoSPRatingList = bondInfoSPRatingViewer.getList();
		bondInfoSPRatingList.setItems(spRatingString);
		bondInfoSPRatingList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		bondInfoSPRatingList.setBounds(10, 30, 84, 121);
		
		
		Composite CurrIntRate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		CurrIntRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		CurrIntRate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		CurrIntRate.setBounds(370, 482, 196, 114);
		formToolkit.paintBordersFor(CurrIntRate);
		
		maxCurrIntRate = new Spinner(CurrIntRate, SWT.BORDER);
		maxCurrIntRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxCurrIntRate.setMaximum(2000000000);
		maxCurrIntRate.setDigits(4);
		maxCurrIntRate.setBounds(65, 82, 117, 22);
		formToolkit.adapt(maxCurrIntRate);
		formToolkit.paintBordersFor(maxCurrIntRate);
		
		Label lblMinCurrIntRate = new Label(CurrIntRate, SWT.NONE);
		lblMinCurrIntRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinCurrIntRate.setText("Min");
		lblMinCurrIntRate.setBounds(0, 46, 40, 22);
		formToolkit.adapt(lblMinCurrIntRate, true, true);
		
		minCurrIntRate = new Spinner(CurrIntRate, SWT.BORDER | SWT.WRAP);
		minCurrIntRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minCurrIntRate.setMaximum(2000000000);
		minCurrIntRate.setDigits(4);
		minCurrIntRate.setBounds(65, 43, 117, 22);
		formToolkit.adapt(minCurrIntRate);
		formToolkit.paintBordersFor(minCurrIntRate);
		
		Label lblCurrentInterestRate = new Label(CurrIntRate, SWT.NONE);
		lblCurrentInterestRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblCurrentInterestRate.setAlignment(SWT.CENTER);
		lblCurrentInterestRate.setText("Current Interest Rate");
		lblCurrentInterestRate.setBounds(0, 0, 192, 26);
		formToolkit.adapt(lblCurrentInterestRate, true, true);
		
		Label lblMaxCurrIntRate = new Label(CurrIntRate, SWT.NONE);
		lblMaxCurrIntRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxCurrIntRate.setText("Max");
		lblMaxCurrIntRate.setBounds(0, 82, 40, 19);
		formToolkit.adapt(lblMaxCurrIntRate, true, true);
		
		Composite NextCallDate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		NextCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		NextCallDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		NextCallDate.setBounds(13, 350, 185, 126);
		formToolkit.paintBordersFor(NextCallDate);
		
		Label lblNextCallDate = new Label(NextCallDate, SWT.BORDER | SWT.CENTER);
		lblNextCallDate.setText("Next Call Date");
		lblNextCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblNextCallDate.setBounds(0, 0, 181, 26);
		formToolkit.adapt(lblNextCallDate, true, true);
		
		Label lblNextMinCallDate = formToolkit.createLabel(NextCallDate, "Min", SWT.BORDER);
		lblNextMinCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblNextMinCallDate.setBounds(0, 44, 33, 26);
		
		Label lblNextMaxCallDate = formToolkit.createLabel(NextCallDate, "Max", SWT.BORDER);
		lblNextMaxCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblNextMaxCallDate.setBounds(0, 85, 38, 27);
		
		minNextCallDate = new DateChooserCombo(NextCallDate, SWT.NONE);
		minNextCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minNextCallDate.setBounds(56, 50, 115, 21);
		formToolkit.adapt(minNextCallDate);
		formToolkit.paintBordersFor(minNextCallDate);
		
		maxNextCallDate = new DateChooserCombo(NextCallDate, SWT.NONE);
		maxNextCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxNextCallDate.setBounds(55, 90, 116, 22);
		formToolkit.adapt(maxNextCallDate);
		formToolkit.paintBordersFor(maxNextCallDate);
		
		Composite NextCallPrice = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		NextCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		NextCallPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		NextCallPrice.setBounds(204, 350, 159, 126);
		formToolkit.paintBordersFor(NextCallPrice);
		
		maxNextCallPrice = new Spinner(NextCallPrice, SWT.BORDER);
		maxNextCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxNextCallPrice.setMaximum(2000000000);
		maxNextCallPrice.setDigits(4);
		maxNextCallPrice.setBounds(51, 90, 93, 22);
		formToolkit.adapt(maxNextCallPrice);
		formToolkit.paintBordersFor(maxNextCallPrice);
		
		Label lblNextMaxCallPrice = new Label(NextCallPrice, SWT.NONE);
		lblNextMaxCallPrice.setText("Max");
		lblNextMaxCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblNextMaxCallPrice.setBounds(1, 86, 35, 26);
		formToolkit.adapt(lblNextMaxCallPrice, true, true);
		
		Label lblNextMinCallPrice = new Label(NextCallPrice, SWT.NONE);
		lblNextMinCallPrice.setText("Min");
		lblNextMinCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblNextMinCallPrice.setBounds(1, 46, 35, 22);
		formToolkit.adapt(lblNextMinCallPrice, true, true);
		
		minNextCallPrice = new Spinner(NextCallPrice, SWT.BORDER | SWT.WRAP);
		minNextCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minNextCallPrice.setMaximum(2000000000);
		minNextCallPrice.setDigits(4);
		minNextCallPrice.setBounds(51, 49, 93, 22);
		formToolkit.adapt(minNextCallPrice);
		formToolkit.paintBordersFor(minNextCallPrice);
		
		Label lblNextCallPrice = new Label(NextCallPrice, SWT.CENTER);
		lblNextCallPrice.setText("Next Call Price");
		lblNextCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblNextCallPrice.setBounds(0, 0, 155, 26);
		formToolkit.adapt(lblNextCallPrice, true, true);
		
		Composite UpdatedDate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		UpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		UpdatedDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		UpdatedDate.setBounds(13, 602, 148, 135);
		formToolkit.paintBordersFor(UpdatedDate);
		
		Label lblUpdateDate = new Label(UpdatedDate, SWT.BORDER);
		lblUpdateDate.setText("Updated Date");
		lblUpdateDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblUpdateDate.setAlignment(SWT.CENTER);
		lblUpdateDate.setBounds(0, 0, 146, 23);
		formToolkit.adapt(lblUpdateDate, true, true);
		
		Label lblMinUpdatedDate = formToolkit.createLabel(UpdatedDate, "Min", SWT.BORDER);
		lblMinUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinUpdatedDate.setBounds(0, 45, 38, 23);
		
		Label lblMaxUpdatedDate = formToolkit.createLabel(UpdatedDate, "Max", SWT.BORDER);
		lblMaxUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxUpdatedDate.setBounds(0, 87, 38, 23);
		
		maxUpdatedDate = new DateChooserCombo(UpdatedDate, SWT.NONE);
		maxUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxUpdatedDate.setBounds(44, 90, 102, 23);
		formToolkit.adapt(maxUpdatedDate);
		formToolkit.paintBordersFor(maxUpdatedDate);
		
		minUpdatedDate = new DateChooserCombo(UpdatedDate, SWT.NONE);
		minUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minUpdatedDate.setBounds(44, 45, 102, 23);
		formToolkit.adapt(minUpdatedDate);
		formToolkit.paintBordersFor(minUpdatedDate);
		
		
		Composite CallDate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		CallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		CallDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		CallDate.setBounds(369, 350, 161, 126);
		formToolkit.paintBordersFor(CallDate);
		
		Label lblCallDate = new Label(CallDate, SWT.BORDER | SWT.CENTER);
		lblCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblCallDate.setText("Call Date");
		lblCallDate.setBounds(0, 0, 157, 26);
		formToolkit.adapt(lblCallDate, true, true);
		
		Label lblMinCallDate = formToolkit.createLabel(CallDate, "Min", SWT.BORDER);
		lblMinCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinCallDate.setBounds(0, 50, 35, 26);
		
		Label lblMaxCallDate = formToolkit.createLabel(CallDate, "Max", SWT.BORDER);
		lblMaxCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxCallDate.setBounds(0, 91, 35, 21);
		
		minCallDate = new DateChooserCombo(CallDate, SWT.NONE);
		minCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minCallDate.setBounds(50, 50, 97, 21);
		formToolkit.adapt(minCallDate);
		formToolkit.paintBordersFor(minCallDate);
		
		maxCallDate = new DateChooserCombo(CallDate, SWT.NONE);
		maxCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxCallDate.setBounds(49, 90, 98, 22);
		formToolkit.adapt(maxCallDate);
		formToolkit.paintBordersFor(maxCallDate);
		
		Composite CallPrice = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		CallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		CallPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		CallPrice.setBounds(538, 350, 161, 126);
		formToolkit.paintBordersFor(CallPrice);
		
		maxCallPrice = new Spinner(CallPrice, SWT.BORDER);
		maxCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxCallPrice.setMaximum(2000000000);
		maxCallPrice.setDigits(4);
		maxCallPrice.setBounds(54, 87, 93, 22);
		formToolkit.adapt(maxCallPrice);
		formToolkit.paintBordersFor(maxCallPrice);
		
		Label lblMaxCallPrice = new Label(CallPrice, SWT.NONE);
		lblMaxCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxCallPrice.setText("Max");
		lblMaxCallPrice.setBounds(1, 86, 36, 26);
		formToolkit.adapt(lblMaxCallPrice, true, true);
		
		Label lblMinCallPrice = new Label(CallPrice, SWT.NONE);
		lblMinCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinCallPrice.setText("Min");
		lblMinCallPrice.setBounds(1, 46, 34, 22);
		formToolkit.adapt(lblMinCallPrice, true, true);
		
		minCallPrice = new Spinner(CallPrice, SWT.BORDER | SWT.WRAP);
		minCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minCallPrice.setMaximum(2000000000);
		minCallPrice.setDigits(4);
		minCallPrice.setBounds(54, 46, 93, 22);
		formToolkit.adapt(minCallPrice);
		formToolkit.paintBordersFor(minCallPrice);
		
		Label lblCallPrice = new Label(CallPrice, SWT.CENTER);
		lblCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblCallPrice.setText("Call Price");
		lblCallPrice.setBounds(0, 0, 157, 26);
		formToolkit.adapt(lblCallPrice, true, true);
		
		Composite PartialCallDate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		PartialCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		PartialCallDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		PartialCallDate.setBounds(708, 350, 172, 126);
		formToolkit.paintBordersFor(PartialCallDate);
		
		Label lblPartialCallDate = new Label(PartialCallDate, SWT.BORDER);
		lblPartialCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblPartialCallDate.setAlignment(SWT.CENTER);
		lblPartialCallDate.setText("Partial Call Date");
		lblPartialCallDate.setBounds(0, 0, 168, 26);
		formToolkit.adapt(lblPartialCallDate, true, true);
		
		
		Label lblMinPartialCallDate = formToolkit.createLabel(PartialCallDate, "Min", SWT.BORDER);
		lblMinPartialCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinPartialCallDate.setBounds(0, 44, 37, 26);
		
		Label lblMaxPartialCallDate = formToolkit.createLabel(PartialCallDate, "Max", SWT.BORDER);
		lblMaxPartialCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxPartialCallDate.setBounds(0, 89, 37, 23);
		
		minPartialCallDate = new DateChooserCombo(PartialCallDate, SWT.NONE);
		minPartialCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minPartialCallDate.setBounds(53, 49, 105, 23);
		formToolkit.adapt(minPartialCallDate);
		formToolkit.paintBordersFor(minPartialCallDate);
		
		maxPartialCallDate = new DateChooserCombo(PartialCallDate, SWT.NONE);
		maxPartialCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxPartialCallDate.setBounds(53, 89, 105, 23);
		formToolkit.adapt(maxPartialCallDate);
		formToolkit.paintBordersFor(maxPartialCallDate);

		
		Composite SinkDate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		SinkDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		SinkDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		SinkDate.setBounds(1064, 350, 178, 126);
		formToolkit.paintBordersFor(SinkDate);
		
		Label lblSinkDate = new Label(SinkDate, SWT.BORDER);
		lblSinkDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblSinkDate.setAlignment(SWT.CENTER);
		lblSinkDate.setText("Sink Date");
		lblSinkDate.setBounds(0, 0, 174, 26);
		formToolkit.adapt(lblSinkDate, true, true);
		
		
		
		Label lblMinSinkDate = formToolkit.createLabel(SinkDate, "Min", SWT.BORDER);
		lblMinSinkDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinSinkDate.setBounds(2, 44, 32, 21);
		
		Label lblMaxSinkDate = formToolkit.createLabel(SinkDate, "Max", SWT.BORDER);
		lblMaxSinkDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxSinkDate.setBounds(2, 86, 38, 20);
		
		minSinkDate = new DateChooserCombo(SinkDate, SWT.NONE);
		minSinkDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minSinkDate.setBounds(56, 44, 110, 21);
		formToolkit.adapt(minSinkDate);
		formToolkit.paintBordersFor(minSinkDate);
		
		maxSinkDate = new DateChooserCombo(SinkDate, SWT.NONE);
		maxSinkDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxSinkDate.setBounds(56, 85, 110, 21);
		formToolkit.adapt(maxSinkDate);
		formToolkit.paintBordersFor(maxSinkDate);

		
		Composite PartialCallRate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		PartialCallRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		PartialCallRate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		PartialCallRate.setBounds(886, 350, 172, 126);
		formToolkit.paintBordersFor(PartialCallRate);
		
		maxPartialCallRate = new Spinner(PartialCallRate, SWT.BORDER);
		maxPartialCallRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxPartialCallRate.setMaximum(2000000000);
		maxPartialCallRate.setDigits(4);
		maxPartialCallRate.setBounds(64, 88, 94, 22);
		formToolkit.adapt(maxPartialCallRate);
		formToolkit.paintBordersFor(maxPartialCallRate);
		
		Label lblMaxPartialCallRate = new Label(PartialCallRate, SWT.NONE);
		lblMaxPartialCallRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxPartialCallRate.setText("Max");
		lblMaxPartialCallRate.setBounds(2, 87, 43, 23);
		formToolkit.adapt(lblMaxPartialCallRate, true, true);
		
		Label lblMinPartialCallRate = new Label(PartialCallRate, SWT.NONE);
		lblMinPartialCallRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinPartialCallRate.setText("Min");
		lblMinPartialCallRate.setBounds(2, 47, 43, 22);
		formToolkit.adapt(lblMinPartialCallRate, true, true);
		
		minPartialCallRate = new Spinner(PartialCallRate, SWT.BORDER | SWT.WRAP);
		minPartialCallRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minPartialCallRate.setMaximum(2000000000);
		minPartialCallRate.setDigits(4);
		minPartialCallRate.setBounds(64, 47, 94, 22);
		formToolkit.adapt(minPartialCallRate);
		formToolkit.paintBordersFor(minPartialCallRate);
		
		Label lblPartialCallRate = new Label(PartialCallRate, SWT.BORDER);
		lblPartialCallRate.setText("Partial Call Rate");
		lblPartialCallRate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblPartialCallRate.setAlignment(SWT.CENTER);
		lblPartialCallRate.setBounds(-10, 0, 178, 26);
		formToolkit.adapt(lblPartialCallRate, true, true);
		

		
		Composite SinkPrice = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		SinkPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		SinkPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		SinkPrice.setBounds(1248, 350, 159, 126);
		formToolkit.paintBordersFor(SinkPrice);
		
		maxSinkPrice = new Spinner(SinkPrice, SWT.BORDER);
		maxSinkPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxSinkPrice.setMaximum(2000000000);
		maxSinkPrice.setDigits(4);
		maxSinkPrice.setBounds(48, 90, 99, 22);
		formToolkit.adapt(maxSinkPrice);
		formToolkit.paintBordersFor(maxSinkPrice);
		
		Label lblMaxSinkPrice = new Label(SinkPrice, SWT.NONE);
		lblMaxSinkPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxSinkPrice.setText("Max");
		lblMaxSinkPrice.setBounds(-1, 86, 36, 26);
		formToolkit.adapt(lblMaxSinkPrice, true, true);
		
		Label lblMinSinkPrice = new Label(SinkPrice, SWT.NONE);
		lblMinSinkPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinSinkPrice.setText("Min");
		lblMinSinkPrice.setBounds(-1, 46, 32, 22);
		formToolkit.adapt(lblMinSinkPrice, true, true);
		
		minSinkPrice = new Spinner(SinkPrice, SWT.BORDER | SWT.WRAP);
		minSinkPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minSinkPrice.setMaximum(2000000000);
		minSinkPrice.setDigits(4);
		minSinkPrice.setBounds(48, 50, 99, 22);
		formToolkit.adapt(minSinkPrice);
		formToolkit.paintBordersFor(minSinkPrice);
		
		Label lblSinkPrice = new Label(SinkPrice, SWT.CENTER);
		lblSinkPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblSinkPrice.setText("Sink Price");
		lblSinkPrice.setBounds(0, 0, 155, 26);
		formToolkit.adapt(lblSinkPrice, true, true);
		
		Composite RedemptionDate = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		RedemptionDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		RedemptionDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		RedemptionDate.setBounds(13, 482, 167, 114);
		formToolkit.paintBordersFor(RedemptionDate);
		
		Label lblRedemptionDate = new Label(RedemptionDate, SWT.BORDER);
		lblRedemptionDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblRedemptionDate.setAlignment(SWT.CENTER);
		lblRedemptionDate.setText("Redemption Date");
		lblRedemptionDate.setBounds(0, 0, 165, 23);
		formToolkit.adapt(lblRedemptionDate, true, true);
		
		
		Label lblMinRedemptionDate = formToolkit.createLabel(RedemptionDate, "Min", SWT.BORDER);
		lblMinRedemptionDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinRedemptionDate.setBounds(0, 40, 32, 23);
		
		Label lblMaxRedemptionDate = formToolkit.createLabel(RedemptionDate, "Max", SWT.BORDER);
		lblMaxRedemptionDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxRedemptionDate.setBounds(0, 82, 38, 20);
		
		maxRedemptionDate = new DateChooserCombo(RedemptionDate, SWT.NONE);
		maxRedemptionDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxRedemptionDate.setBounds(50, 79, 105, 23);
		formToolkit.adapt(maxRedemptionDate);
		formToolkit.paintBordersFor(maxRedemptionDate);
		
		minRedemptionDate = new DateChooserCombo(RedemptionDate, SWT.NONE);
		minRedemptionDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minRedemptionDate.setBounds(50, 34, 105, 23);
		formToolkit.adapt(minRedemptionDate);
		formToolkit.paintBordersFor(minRedemptionDate);
		
		
		Composite RedemptionPrice = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		RedemptionPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		RedemptionPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		RedemptionPrice.setBounds(186, 482, 178, 114);
		formToolkit.paintBordersFor(RedemptionPrice);
		
		maxRedemptionPrice = new Spinner(RedemptionPrice, SWT.BORDER | SWT.DROP_DOWN);
		maxRedemptionPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxRedemptionPrice.setMaximum(2000000000);
		maxRedemptionPrice.setDigits(4);
		maxRedemptionPrice.setBounds(60, 78, 102, 22);
		formToolkit.adapt(maxRedemptionPrice);
		formToolkit.paintBordersFor(maxRedemptionPrice);
		
		Label lblMaxRedemptionPrice = new Label(RedemptionPrice, SWT.NONE);
		lblMaxRedemptionPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxRedemptionPrice.setText("Max");
		lblMaxRedemptionPrice.setBounds(0, 78, 43, 22);
		formToolkit.adapt(lblMaxRedemptionPrice, true, true);
		
		Label lblMinRedemptionPrice = new Label(RedemptionPrice, SWT.NONE);
		lblMinRedemptionPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinRedemptionPrice.setText("Min");
		lblMinRedemptionPrice.setBounds(0, 38, 33, 22);
		formToolkit.adapt(lblMinRedemptionPrice, true, true);
		
		minRedemptionPrice = new Spinner(RedemptionPrice, SWT.BORDER | SWT.WRAP);
		minRedemptionPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minRedemptionPrice.setMaximum(2000000000);
		minRedemptionPrice.setDigits(4);
		minRedemptionPrice.setBounds(60, 38, 102, 22);
		formToolkit.adapt(minRedemptionPrice);
		formToolkit.paintBordersFor(minRedemptionPrice);
		
		Label lblRedemptionPrice = new Label(RedemptionPrice, SWT.NONE);
		lblRedemptionPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblRedemptionPrice.setAlignment(SWT.CENTER);
		lblRedemptionPrice.setText("Redemption Price");
		lblRedemptionPrice.setBounds(0, 0, 174, 22);
		formToolkit.adapt(lblRedemptionPrice, true, true);
		
		btnActiveMaturities = new SwitchButton(BondInfoComposite, SWT.BORDER | SWT.CHECK);
		btnActiveMaturities.setText("Active Maturities Only");
		btnActiveMaturities.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnActiveMaturities.setBounds(971, 737, 251, 37);
		formToolkit.adapt(btnActiveMaturities, true, true);
		btnActiveMaturities.setSelection(true);
		
		
		Composite AdditionalCredit = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		AdditionalCredit.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_FOREGROUND));
		AdditionalCredit.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		AdditionalCredit.setBackground(SWTResourceManager.getColor(32, 44, 57));
		AdditionalCredit.setBounds(167, 602, 202, 165);
		formToolkit.paintBordersFor(AdditionalCredit);
		
		Label lblAdditionalCredit = new Label(AdditionalCredit, SWT.BORDER | SWT.CENTER);
		lblAdditionalCredit.setText("Additional Credit");
		lblAdditionalCredit.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblAdditionalCredit.setBounds(0, 0, 200, 26);
		formToolkit.adapt(lblAdditionalCredit, true, true);
		
		ListViewer additionalCreditViewer = new ListViewer(AdditionalCredit, SWT.BORDER | SWT.V_SCROLL  | SWT.H_SCROLL| SWT.MULTI);
		addlCreditList = additionalCreditViewer.getList();
		addlCreditList.setItems(addlCreditString);
		addlCreditList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		addlCreditList.setBounds(0, 32, 200, 119);
		

		
		Composite bondInfoTradingInfoConfig = formToolkit.createComposite(BondInfoComposite, SWT.BORDER);
		bondInfoTradingInfoConfig.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		bondInfoTradingInfoConfig.setBackground(SWTResourceManager.getColor(186,86,36));
		bondInfoTradingInfoConfig.setBounds(598, 482, 875, 222);
		formToolkit.paintBordersFor(bondInfoTradingInfoConfig);
		
		Composite TradeDate = formToolkit.createComposite(bondInfoTradingInfoConfig, SWT.BORDER);
		TradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TradeDate.setBounds(10, 49, 171, 114);
		TradeDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(TradeDate);
		
		Label lblTradeDate = new Label(TradeDate, SWT.BORDER);
		lblTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTradeDate.setAlignment(SWT.CENTER);
		lblTradeDate.setText("Trade Date");
		lblTradeDate.setBounds(0, 0, 169, 24);
		formToolkit.adapt(lblTradeDate, true, true);
		
		Label lblMinTradeDate = formToolkit.createLabel(TradeDate, "Min", SWT.BORDER);
		lblMinTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinTradeDate.setBounds(0, 39, 33, 24);
		
		Label lblMaxTradeDate = formToolkit.createLabel(TradeDate, "Max", SWT.BORDER);
		lblMaxTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxTradeDate.setBounds(0, 76, 43, 24);
		
		minTradeDate = new DateChooserCombo(TradeDate, SWT.NONE);
		minTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minTradeDate.setBounds(53, 38, 106, 25);
		formToolkit.adapt(minTradeDate);
		formToolkit.paintBordersFor(minTradeDate);
		
		maxTradeDate = new DateChooserCombo(TradeDate, SWT.NONE);
		maxTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxTradeDate.setBounds(53, 80, 106, 25);
		formToolkit.adapt(maxTradeDate);
		formToolkit.paintBordersFor(maxTradeDate);
		
		Composite ParAmountTraded = formToolkit.createComposite(bondInfoTradingInfoConfig, SWT.BORDER);
		ParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		ParAmountTraded.setBounds(187, 49, 192, 114);
		ParAmountTraded.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(ParAmountTraded);
		
		maxParAmountTraded = new Spinner(ParAmountTraded, SWT.BORDER);
		maxParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxParAmountTraded.setMaximum(2000000000);
		maxParAmountTraded.setBounds(58, 78, 120, 22);
		formToolkit.adapt(maxParAmountTraded);
		formToolkit.paintBordersFor(maxParAmountTraded);
		
		minParAmountTraded = new Spinner(ParAmountTraded, SWT.BORDER);
		minParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minParAmountTraded.setMaximum(2000000000);
		minParAmountTraded.setBounds(58, 37, 120, 22);
		formToolkit.adapt(minParAmountTraded);
		formToolkit.paintBordersFor(minParAmountTraded);
		
		Label lblParAmountTraded = new Label(ParAmountTraded, SWT.NONE);
		lblParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblParAmountTraded.setAlignment(SWT.CENTER);
		lblParAmountTraded.setText("Par Amount Traded");
		lblParAmountTraded.setBounds(0, 0, 188, 24);
		formToolkit.adapt(lblParAmountTraded, true, true);
		
		Label lblMinParAmountTraded = new Label(ParAmountTraded, SWT.NONE);
		lblMinParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinParAmountTraded.setText("Min");
		lblMinParAmountTraded.setBounds(2, 40, 33, 22);
		formToolkit.adapt(lblMinParAmountTraded, true, true);
		
		Label lblMaxParAmountTraded = new Label(ParAmountTraded, SWT.NONE);
		lblMaxParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxParAmountTraded.setText("Max");
		lblMaxParAmountTraded.setBounds(2, 78, 33, 22);
		formToolkit.adapt(lblMaxParAmountTraded, true, true);
		
		Label lblTradingConfigurations = new Label(bondInfoTradingInfoConfig, SWT.NONE);
		lblTradingConfigurations.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 13, SWT.BOLD));
		lblTradingConfigurations.setAlignment(SWT.CENTER);
		lblTradingConfigurations.setBounds(287, 0, 347, 25);
		formToolkit.adapt(lblTradingConfigurations, true, true);
		lblTradingConfigurations.setText("Trading Configurations");
		
		btnCalculateTradingYield = new Button(bondInfoTradingInfoConfig, SWT.BORDER | SWT.CHECK);
		btnCalculateTradingYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnCalculateTradingYield.setBounds(345, 171, 223, 34);
		btnCalculateTradingYield.setText("Calculate Trading Yield");
		formToolkit.adapt(btnCalculateTradingYield, true, true);
		
		btnTradingChecks = new Button(bondInfoTradingInfoConfig, SWT.BORDER | SWT.CHECK);
		btnTradingChecks.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnTradingChecks.setAlignment(SWT.CENTER);
		btnTradingChecks.setBounds(10, 175, 171, 26);
		btnTradingChecks.setText("Trading Checks");
		formToolkit.adapt(btnTradingChecks, true, true);
		
		Composite ParDollarPrice = formToolkit.createComposite(bondInfoTradingInfoConfig, SWT.BORDER);
		ParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		ParDollarPrice.setBounds(385, 49, 167, 114);
		ParDollarPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(ParDollarPrice);
		
		maxParDollarPrice = new Spinner(ParDollarPrice, SWT.BORDER);
		maxParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxParDollarPrice.setMaximum(2000000000);
		maxParDollarPrice.setDigits(4);
		maxParDollarPrice.setBounds(59, 78, 94, 22);
		formToolkit.adapt(maxParDollarPrice);
		formToolkit.paintBordersFor(maxParDollarPrice);
		
		Label lblMaxParDollarPrice = new Label(ParDollarPrice, SWT.NONE);
		lblMaxParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxParDollarPrice.setForeground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		lblMaxParDollarPrice.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		lblMaxParDollarPrice.setText("Max");
		lblMaxParDollarPrice.setBounds(1, 78, 40, 22);
		formToolkit.adapt(lblMaxParDollarPrice, true, true);
		
		Label lblMinParDollarPrice = new Label(ParDollarPrice, SWT.NONE);
		lblMinParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinParDollarPrice.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		lblMinParDollarPrice.setText("Min");
		lblMinParDollarPrice.setBounds(1, 40, 40, 22);
		formToolkit.adapt(lblMinParDollarPrice, true, true);
		
		minParDollarPrice = new Spinner(ParDollarPrice, SWT.BORDER | SWT.WRAP);
		minParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minParDollarPrice.setPageIncrement(2000000000);
		minParDollarPrice.setMaximum(999999999);
		minParDollarPrice.setDigits(4);
		minParDollarPrice.setBounds(59, 37, 94, 22);
		formToolkit.adapt(minParDollarPrice);
		formToolkit.paintBordersFor(minParDollarPrice);
		
		Label lblParDollarPrice = new Label(ParDollarPrice, SWT.NONE);
		lblParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblParDollarPrice.setAlignment(SWT.CENTER);
		lblParDollarPrice.setText("Par Dollar Price");
		lblParDollarPrice.setBounds(0, 0, 163, 24);
		formToolkit.adapt(lblParDollarPrice, true, true);
		
		Composite ParTradedYield = formToolkit.createComposite(bondInfoTradingInfoConfig, SWT.BORDER);
		ParTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		ParTradedYield.setBounds(558, 49, 158, 114);
		ParTradedYield.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(ParTradedYield);
		
		maxTradedYield = new Spinner(ParTradedYield, SWT.BORDER);
		maxTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxTradedYield.setMaximum(2000000000);
		maxTradedYield.setDigits(4);
		maxTradedYield.setBounds(48, 79, 96, 22);
		formToolkit.adapt(maxTradedYield);
		formToolkit.paintBordersFor(maxTradedYield);
		
		Label lblMinTradedYield = new Label(ParTradedYield, SWT.NONE);
		lblMinTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinTradedYield.setText("Min");
		lblMinTradedYield.setBounds(0, 41, 33, 22);
		formToolkit.adapt(lblMinTradedYield, true, true);
		
		minTradedYield = new Spinner(ParTradedYield, SWT.BORDER | SWT.WRAP);
		minTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minTradedYield.setMaximum(2000000000);
		minTradedYield.setDigits(4);
		minTradedYield.setBounds(48, 38, 96, 22);
		formToolkit.adapt(minTradedYield);
		formToolkit.paintBordersFor(minTradedYield);
		
		Label lblParTradedYield = new Label(ParTradedYield, SWT.NONE);
		lblParTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblParTradedYield.setAlignment(SWT.CENTER);
		lblParTradedYield.setText("Par Traded Yield");
		lblParTradedYield.setBounds(0, 0, 154, 24);
		formToolkit.adapt(lblParTradedYield, true, true);
		
		Label lblMaxTradedYield = new Label(ParTradedYield, SWT.NONE);
		lblMaxTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxTradedYield.setText("Max");
		lblMaxTradedYield.setBounds(0, 79, 33, 22);
		formToolkit.adapt(lblMaxTradedYield, true, true);
		
		btnUploadMmd = formToolkit.createButton(bondInfoTradingInfoConfig, "Upload MMD", SWT.BORDER);
		btnUploadMmd.addSelectionListener(new SelectionAdapter() {
			@Override
			
			public void widgetSelected(SelectionEvent e) {
				  FileDialog fileDialog = new FileDialog(bondInfoTradingInfoConfig.getShell());
				    /*fileDialog.setFilterNames(new String[] { "Microsoft Excel Macro (*.xlsm)",
				            "Microsoft Excel Spreadsheet Files (*.xls)", "Microsoft Open XML Spreadsheet (*.xlsx)", "Comma Separated Values Files (*.csv)",
				            "All Files (*.*)" });
					*/
				    fileDialog.setFilterExtensions(new String[] { "*.xlsm", "*.xls", "*.xlsx", "*.csv", "*.*" });
				    String filename = fileDialog.open();
				    btnUploadMmd.setData(filename);
			}
			
		});
		
		btnUploadMmd.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnUploadMmd.setBounds(197, 169, 132, 36);
		
		Composite TradeType = formToolkit.createComposite(bondInfoTradingInfoConfig, SWT.BORDER);
		TradeType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TradeType.setBackground(SWTResourceManager.getColor(32, 44, 57));
		TradeType.setBounds(722, 49, 123, 114);
		formToolkit.paintBordersFor(TradeType);
		
		Label lblTradeType = new Label(TradeType, SWT.NONE);
		lblTradeType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTradeType.setAlignment(SWT.CENTER);
		lblTradeType.setText("Trade Type");
		lblTradeType.setBounds(0, 0, 121, 24);
		formToolkit.adapt(lblTradeType, true, true);
		
		btnTradeTypeD = new Button(TradeType, SWT.CHECK);
		btnTradeTypeD.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(btnTradeTypeD.getSelection());
			}
		});
		btnTradeTypeD.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnTradeTypeD.setText("D");
		btnTradeTypeD.setBounds(6, 36, 105, 18);
		formToolkit.adapt(btnTradeTypeD, true, true);
		
		btnTradeTypeS = new Button(TradeType, SWT.CHECK);
		btnTradeTypeS.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnTradeTypeS.setText("S");
		btnTradeTypeS.setBounds(6, 60, 105, 18);
		formToolkit.adapt(btnTradeTypeS, true, true);
		
		btnTradeTypeP = new Button(TradeType, SWT.CHECK);
		btnTradeTypeP.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnTradeTypeP.setText("P");
		btnTradeTypeP.setBounds(6, 84, 105, 18);
		formToolkit.adapt(btnTradeTypeP, true, true);
		
		Button btnClearInputs = formToolkit.createButton(BondInfoComposite, "Clear Inputs", SWT.BORDER);
		btnClearInputs.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Selected Clear Inputs");
				bondInfoClear();
				
			}
		});
		btnClearInputs.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnClearInputs.setBounds(683, 734, 121, 40);
		
		TabItem tbtmQuickBond = new TabItem(tabFolder, SWT.NONE);
		tbtmQuickBond.setText("Quick Bond Search");
		
		Composite QuickBondSearch = new Composite(tabFolder, SWT.BORDER);
		tbtmQuickBond.setControl(QuickBondSearch);
		QuickBondSearch.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondSearch.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		QuickBondSearch.setForeground(SWTResourceManager.getColor(SWT.COLOR_TITLE_FOREGROUND));
		formToolkit.paintBordersFor(QuickBondSearch);
		
		Composite QuickBondTradingConfig = formToolkit.createComposite(QuickBondSearch, SWT.BORDER);
		QuickBondTradingConfig.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondTradingConfig.setBackground(SWTResourceManager.getColor(186,86,36));
		QuickBondTradingConfig.setBounds(98, 282, 1160, 256);
		formToolkit.paintBordersFor(QuickBondTradingConfig);
		
		Composite QuickBondTradeType = formToolkit.createComposite(QuickBondTradingConfig, SWT.BORDER);
		QuickBondTradeType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondTradeType.setBounds(1041, 49, 105, 114);
		QuickBondTradeType.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(QuickBondTradeType);
		
		Label lblQuickBondTradeType = new Label(QuickBondTradeType, SWT.NONE);
		lblQuickBondTradeType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblQuickBondTradeType.setAlignment(SWT.CENTER);
		lblQuickBondTradeType.setText("Trade Type");
		lblQuickBondTradeType.setBounds(0, 0, 101, 22);
		formToolkit.adapt(lblQuickBondTradeType, true, true);
		
		btnQuickBondTradeTypeD = new Button(QuickBondTradeType, SWT.CHECK);
		btnQuickBondTradeTypeD.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondTradeTypeD.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnQuickBondTradeTypeD.setText("D");
		btnQuickBondTradeTypeD.setBounds(6, 36, 85, 18);
		formToolkit.adapt(btnQuickBondTradeTypeD, true, true);
		
		btnQuickBondTradeTypeS = new Button(QuickBondTradeType, SWT.CHECK);
		btnQuickBondTradeTypeS.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondTradeTypeS.setText("S");
		btnQuickBondTradeTypeS.setBounds(6, 60, 85, 18);
		formToolkit.adapt(btnQuickBondTradeTypeS, true, true);
		
		btnQuickBondTradeTypeP = new Button(QuickBondTradeType, SWT.CHECK);
		btnQuickBondTradeTypeP.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondTradeTypeP.setText("P");
		btnQuickBondTradeTypeP.setBounds(6, 84, 85, 18);
		formToolkit.adapt(btnQuickBondTradeTypeP, true, true);
		
		Composite QuickBondParTradedYield = formToolkit.createComposite(QuickBondTradingConfig, SWT.BORDER);
		QuickBondParTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondParTradedYield.setBounds(867, 49, 168, 114);
		QuickBondParTradedYield.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(QuickBondParTradedYield);
		
		maxQuickBondTradedYield = new Spinner(QuickBondParTradedYield, SWT.BORDER);
		maxQuickBondTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxQuickBondTradedYield.setMaximum(2000000000);
		maxQuickBondTradedYield.setDigits(4);
		maxQuickBondTradedYield.setBounds(50, 82, 104, 22);
		formToolkit.adapt(maxQuickBondTradedYield);
		formToolkit.paintBordersFor(maxQuickBondTradedYield);
		
		Label lblMinQuickBondTradedYield = new Label(QuickBondParTradedYield, SWT.NONE);
		lblMinQuickBondTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinQuickBondTradedYield.setText("Min");
		lblMinQuickBondTradedYield.setBounds(2, 41, 42, 19);
		formToolkit.adapt(lblMinQuickBondTradedYield, true, true);
		
		minQuickBondTradedYield = new Spinner(QuickBondParTradedYield, SWT.BORDER | SWT.WRAP);
		minQuickBondTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minQuickBondTradedYield.setMaximum(2000000000);
		minQuickBondTradedYield.setDigits(4);
		minQuickBondTradedYield.setBounds(50, 41, 104, 22);
		formToolkit.adapt(minQuickBondTradedYield);
		formToolkit.paintBordersFor(minQuickBondTradedYield);
		
		Label lblQuickBondParTradedYield = new Label(QuickBondParTradedYield, SWT.NONE);
		lblQuickBondParTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblQuickBondParTradedYield.setAlignment(SWT.CENTER);
		lblQuickBondParTradedYield.setText("Par Traded Yield");
		lblQuickBondParTradedYield.setBounds(0, 0, 164, 22);
		formToolkit.adapt(lblQuickBondParTradedYield, true, true);
		
		Label lblMaxQuickBondTradedYield = new Label(QuickBondParTradedYield, SWT.NONE);
		lblMaxQuickBondTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxQuickBondTradedYield.setText("Max");
		lblMaxQuickBondTradedYield.setBounds(2, 82, 42, 19);
		formToolkit.adapt(lblMaxQuickBondTradedYield, true, true);
		
		Composite QuickBondParDollarPrice = formToolkit.createComposite(QuickBondTradingConfig, SWT.BORDER);
		QuickBondParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondParDollarPrice.setBounds(690, 49, 171, 114);
		QuickBondParDollarPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(QuickBondParDollarPrice);
		
		maxQuickBondParDollarPrice = new Spinner(QuickBondParDollarPrice, SWT.BORDER);
		maxQuickBondParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxQuickBondParDollarPrice.setMaximum(2000000000);
		maxQuickBondParDollarPrice.setDigits(4);
		maxQuickBondParDollarPrice.setBounds(47, 78, 110, 22);
		formToolkit.adapt(maxQuickBondParDollarPrice);
		formToolkit.paintBordersFor(maxQuickBondParDollarPrice);
		
		Label lblMaxQuickBondParDollarPrice = new Label(QuickBondParDollarPrice, SWT.NONE);
		lblMaxQuickBondParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxQuickBondParDollarPrice.setText("Max");
		lblMaxQuickBondParDollarPrice.setBounds(5, 78, 36, 22);
		formToolkit.adapt(lblMaxQuickBondParDollarPrice, true, true);
		
		Label lblMinQuickBondParDollarPrice = new Label(QuickBondParDollarPrice, SWT.NONE);
		lblMinQuickBondParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinQuickBondParDollarPrice.setText("Min");
		lblMinQuickBondParDollarPrice.setBounds(5, 37, 36, 23);
		formToolkit.adapt(lblMinQuickBondParDollarPrice, true, true);
		
		minQuickBondParDollarPrice = new Spinner(QuickBondParDollarPrice, SWT.BORDER | SWT.WRAP);
		minQuickBondParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minQuickBondParDollarPrice.setMaximum(2000000000);
		minQuickBondParDollarPrice.setDigits(4);
		minQuickBondParDollarPrice.setBounds(47, 37, 110, 22);
		formToolkit.adapt(minQuickBondParDollarPrice);
		formToolkit.paintBordersFor(minQuickBondParDollarPrice);
		
		Label lblQuickBondParDollarPrice = new Label(QuickBondParDollarPrice, SWT.NONE);
		lblQuickBondParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblQuickBondParDollarPrice.setAlignment(SWT.CENTER);
		lblQuickBondParDollarPrice.setText("Par Dollar Price");
		lblQuickBondParDollarPrice.setBounds(0, 0, 167, 22);
		formToolkit.adapt(lblQuickBondParDollarPrice, true, true);
		
		Composite QuickBondParAmountTraded = formToolkit.createComposite(QuickBondTradingConfig, SWT.BORDER);
		QuickBondParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondParAmountTraded.setBounds(516, 49, 168, 114);
		QuickBondParAmountTraded.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(QuickBondParAmountTraded);
		
		maxQuickBondParAmountTraded = new Spinner(QuickBondParAmountTraded, SWT.BORDER);
		maxQuickBondParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxQuickBondParAmountTraded.setMaximum(2000000000);
		maxQuickBondParAmountTraded.setBounds(45, 82, 112, 22);
		formToolkit.adapt(maxQuickBondParAmountTraded);
		formToolkit.paintBordersFor(maxQuickBondParAmountTraded);
		
		minQuickBondParAmountTraded = new Spinner(QuickBondParAmountTraded, SWT.BORDER);
		minQuickBondParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minQuickBondParAmountTraded.setMaximum(2000000000);
		minQuickBondParAmountTraded.setBounds(45, 41, 112, 22);
		formToolkit.adapt(minQuickBondParAmountTraded);
		formToolkit.paintBordersFor(minQuickBondParAmountTraded);
		
		Label lblQuickBondParAmountTraded = new Label(QuickBondParAmountTraded, SWT.NONE);
		lblQuickBondParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblQuickBondParAmountTraded.setAlignment(SWT.CENTER);
		lblQuickBondParAmountTraded.setText("Par Amount Traded");
		lblQuickBondParAmountTraded.setBounds(0, 0, 164, 22);
		formToolkit.adapt(lblQuickBondParAmountTraded, true, true);
		
		Label lblMinQuckBondParAmountTraded = new Label(QuickBondParAmountTraded, SWT.NONE);
		lblMinQuckBondParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinQuckBondParAmountTraded.setText("Min");
		lblMinQuckBondParAmountTraded.setBounds(2, 45, 32, 18);
		formToolkit.adapt(lblMinQuckBondParAmountTraded, true, true);
		
		Label lblMaxQuickBondParAmountTraded = new Label(QuickBondParAmountTraded, SWT.NONE);
		lblMaxQuickBondParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxQuickBondParAmountTraded.setText("Max");
		lblMaxQuickBondParAmountTraded.setBounds(2, 82, 37, 18);
		formToolkit.adapt(lblMaxQuickBondParAmountTraded, true, true);
		
		Composite QuickBondTradeDate = formToolkit.createComposite(QuickBondTradingConfig, SWT.BORDER);
		QuickBondTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondTradeDate.setBounds(10, 49, 168, 114);
		QuickBondTradeDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(QuickBondTradeDate);
		
		Label lblQuickBondTradeDate = new Label(QuickBondTradeDate, SWT.BORDER);
		lblQuickBondTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblQuickBondTradeDate.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblQuickBondTradeDate.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_DARK_SHADOW));
		lblQuickBondTradeDate.setAlignment(SWT.CENTER);
		lblQuickBondTradeDate.setText("Trade Date");
		lblQuickBondTradeDate.setBounds(-2, 0, 166, 22);
		formToolkit.adapt(lblQuickBondTradeDate, true, true);
		
		Label lblMinQuickBondCallDate = formToolkit.createLabel(QuickBondTradeDate, "Min", SWT.BORDER);
		lblMinQuickBondCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinQuickBondCallDate.setBounds(-2, 36, 33, 22);
		
		Label lblMaxQuickBondCallDate = formToolkit.createLabel(QuickBondTradeDate, "Max", SWT.BORDER);
		lblMaxQuickBondCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxQuickBondCallDate.setBounds(-2, 83, 38, 22);
		
		minQuickBondTradeDate = new DateChooserCombo(QuickBondTradeDate, SWT.NONE);
		minQuickBondTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minQuickBondTradeDate.setBounds(51, 36, 103, 22);
		formToolkit.adapt(minQuickBondTradeDate);
		formToolkit.paintBordersFor(minQuickBondTradeDate);
		
		maxQuickBondTradeDate = new DateChooserCombo(QuickBondTradeDate, SWT.NONE);
		maxQuickBondTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxQuickBondTradeDate.setBounds(51, 83, 103, 22);
		formToolkit.adapt(maxQuickBondTradeDate);
		formToolkit.paintBordersFor(maxQuickBondTradeDate);
		
		btnQuickBondCalculateTradingYield = new Button(QuickBondTradingConfig, SWT.BORDER | SWT.CHECK);
		btnQuickBondCalculateTradingYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondCalculateTradingYield.setAlignment(SWT.CENTER);
		btnQuickBondCalculateTradingYield.setBounds(372, 193, 219, 32);
		btnQuickBondCalculateTradingYield.setText("Calculate Trading Yield");
		formToolkit.adapt(btnQuickBondCalculateTradingYield, true, true);
		
		btnQuickBondTradingChecks = new Button(QuickBondTradingConfig, SWT.BORDER | SWT.CHECK);
		btnQuickBondTradingChecks.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondTradingChecks.setAlignment(SWT.CENTER);
		btnQuickBondTradingChecks.setBounds(187, 193, 179, 32);
		btnQuickBondTradingChecks.setText("Trading Checks");
		formToolkit.adapt(btnQuickBondTradingChecks, true, true);
		
		btnQuickBondUploadMmd = formToolkit.createButton(QuickBondTradingConfig, "Upload MMD", SWT.BORDER);
		btnQuickBondUploadMmd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			    FileDialog fileDialog = new FileDialog(QuickBondTradingConfig.getShell());
			    /*fileDialog.setFilterNames(new String[] { "Microsoft Excel Macro (*.xlsm)",
			            "Microsoft Excel Spreadsheet Files (*.xls)","Microsoft Excel OPEN XML Spreadsheet Files (*.xlsx)", "Comma Separated Values Files (*.csv)",
			            "All Files (*.*)" });
				*/
			    fileDialog.setFilterExtensions(new String[] { "*.xlsm", "*.xls","*.xlsx", "*.csv", "*.*" });
			    String filename = fileDialog.open();
			    btnQuickBondUploadMmd.setData(filename);
			    
			}
		});
		btnQuickBondUploadMmd.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondUploadMmd.setBounds(597, 189, 114, 36);
		
		Label lblQuickBondTradingConfigurations = new Label(QuickBondTradingConfig, SWT.BORDER);
		lblQuickBondTradingConfigurations.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblQuickBondTradingConfigurations.setAlignment(SWT.CENTER);
		lblQuickBondTradingConfigurations.setBounds(434, 0, 271, 22);
		lblQuickBondTradingConfigurations.setText("Trading Configurations");
		formToolkit.adapt(lblQuickBondTradingConfigurations, true, true);
		
		Composite QuickBondMaturityDate = formToolkit.createComposite(QuickBondTradingConfig, SWT.BORDER);
		QuickBondMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondMaturityDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		QuickBondMaturityDate.setBounds(184, 49, 160, 114);
		formToolkit.paintBordersFor(QuickBondMaturityDate);
		
		Label lblQuickBondMaturityDate = new Label(QuickBondMaturityDate, SWT.BORDER);
		lblQuickBondMaturityDate.setText("Maturity Date");
		lblQuickBondMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblQuickBondMaturityDate.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblQuickBondMaturityDate.setAlignment(SWT.CENTER);
		lblQuickBondMaturityDate.setBounds(0, 0, 156, 22);
		formToolkit.adapt(lblQuickBondMaturityDate, true, true);
		
		Label lblMinQuickBondMaturityDate = formToolkit.createLabel(QuickBondMaturityDate, "Min", SWT.BORDER);
		lblMinQuickBondMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinQuickBondMaturityDate.setBounds(0, 35, 32, 27);
		
		Label lblMaxQuickBondMaturityDate = formToolkit.createLabel(QuickBondMaturityDate, "Max", SWT.BORDER);
		lblMaxQuickBondMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxQuickBondMaturityDate.setBounds(0, 77, 37, 23);
		
		minQuickBondMaturityDate = new DateChooserCombo(QuickBondMaturityDate, SWT.NONE);
		minQuickBondMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minQuickBondMaturityDate.setBounds(43, 35, 103, 27);
		formToolkit.adapt(minQuickBondMaturityDate);
		formToolkit.paintBordersFor(minQuickBondMaturityDate);
		
		maxQuickBondMaturityDate = new DateChooserCombo(QuickBondMaturityDate, SWT.NONE);
		maxQuickBondMaturityDate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		maxQuickBondMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxQuickBondMaturityDate.setBounds(43, 73, 103, 27);
		formToolkit.adapt(maxQuickBondMaturityDate);
		formToolkit.paintBordersFor(maxQuickBondMaturityDate);
		
		Composite QuickBondCoupon = formToolkit.createComposite(QuickBondTradingConfig, SWT.BORDER);
		QuickBondCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondCoupon.setBackground(SWTResourceManager.getColor(32, 44, 57));
		QuickBondCoupon.setBounds(350, 49, 160, 114);
		formToolkit.paintBordersFor(QuickBondCoupon);
		
		maxQuickBondCoupon = new Spinner(QuickBondCoupon, SWT.BORDER);
		maxQuickBondCoupon.setMaximum(2000000000);
		maxQuickBondCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxQuickBondCoupon.setDigits(4);
		maxQuickBondCoupon.setBounds(52, 79, 94, 22);
		formToolkit.adapt(maxQuickBondCoupon);
		formToolkit.paintBordersFor(maxQuickBondCoupon);
		
		Label lblMinQuickBondCoupon = new Label(QuickBondCoupon, SWT.NONE);
		lblMinQuickBondCoupon.setText("Min");
		lblMinQuickBondCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinQuickBondCoupon.setBounds(2, 38, 37, 22);
		formToolkit.adapt(lblMinQuickBondCoupon, true, true);
		
		minQuickBondCoupon = new Spinner(QuickBondCoupon, SWT.BORDER | SWT.WRAP);
		minQuickBondCoupon.setMaximum(2000000000);
		minQuickBondCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minQuickBondCoupon.setDigits(4);
		minQuickBondCoupon.setBounds(52, 38, 94, 22);
		formToolkit.adapt(minQuickBondCoupon);
		formToolkit.paintBordersFor(minQuickBondCoupon);
		
		Label lblQuickBondCoupon = new Label(QuickBondCoupon, SWT.NONE);
		lblQuickBondCoupon.setText("Coupon");
		lblQuickBondCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblQuickBondCoupon.setAlignment(SWT.CENTER);
		lblQuickBondCoupon.setBounds(0, 0, 156, 22);
		formToolkit.adapt(lblQuickBondCoupon, true, true);
		
		Label lblMaxQuickBondCoupon = new Label(QuickBondCoupon, SWT.NONE);
		lblMaxQuickBondCoupon.setText("Max");
		lblMaxQuickBondCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxQuickBondCoupon.setBounds(2, 79, 37, 22);
		formToolkit.adapt(lblMaxQuickBondCoupon, true, true);
		
		Composite QuickBondCusipSearch = formToolkit.createComposite(QuickBondSearch, SWT.BORDER);
		QuickBondCusipSearch.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		QuickBondCusipSearch.setBackground(SWTResourceManager.getColor(32,54,56));
		QuickBondCusipSearch.setBounds(253, 78, 842, 185);
		formToolkit.paintBordersFor(QuickBondCusipSearch);
		
		btnQuickBondCusipList = formToolkit.createButton(QuickBondCusipSearch, "Upload Seperate Cusip File", SWT.BORDER);
		btnQuickBondCusipList.setData(cusipDef);
		btnQuickBondCusipList.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			    FileDialog fileDialog = new FileDialog(QuickBondCusipSearch.getShell());
			    /*fileDialog.setFilterNames(new String[] {  "Comma Separated Values Files (*.csv)", 
			            "Microsoft Excel Spreadsheet Files (*.xls)","Microsoft OPEN XML Spreadsheet Files (*.xlsx)", "Microsoft Excel Macro (*.xlsm)",
			            "All Files (*.*)" });
				*/
			    fileDialog.setFilterExtensions(new String[] { "*.csv", "*.xls","*.xlsx", "*.xlsm", "*.*" });
			    String filename = fileDialog.open();
			    btnQuickBondCusipList.setData(filename);
			
			}});
		btnQuickBondCusipList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondCusipList.setBounds(279, 133, 255, 38);
		
		Label lblNewLabel = formToolkit.createLabel(QuickBondCusipSearch, "Type in Cusip 6 or 9 by Comma", SWT.BORDER | SWT.CENTER);
		lblNewLabel.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblNewLabel.setAlignment(SWT.CENTER);
		lblNewLabel.setBounds(252, 0, 303, 26);
		
		txtQuickBondCusipSearch = formToolkit.createText(QuickBondCusipSearch, "New Text", SWT.BORDER);
		txtQuickBondCusipSearch.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		txtQuickBondCusipSearch.setText("");
		txtQuickBondCusipSearch.setBounds(153, 68, 506, 38);
		
	
		
		Button btnQuickBondClearInputs = formToolkit.createButton(QuickBondSearch, "Clear Inputs", SWT.BORDER);
		btnQuickBondClearInputs.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				quickBondClear();
			}
		});
		btnQuickBondClearInputs.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondClearInputs.setBounds(691, 642, 150, 47);
		
		btnQuickBondActiveMaturities = new SwitchButton(QuickBondSearch, SWT.BORDER);
		btnQuickBondActiveMaturities.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondActiveMaturities.setText("Active Maturities Only");
		btnQuickBondActiveMaturities.setSelection(true);
		btnQuickBondActiveMaturities.setBounds(396, 642, 251, 47);
		formToolkit.adapt(btnQuickBondActiveMaturities);
		formToolkit.paintBordersFor(btnQuickBondActiveMaturities);
		
		
		Button btnQuickBondSubmit = formToolkit.createButton(QuickBondSearch, "Submit", SWT.BORDER);
		btnQuickBondSubmit.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnQuickBondSubmit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println(" Quick Bond Info");

				quickBond quickBondInfoForm = new quickBond();		
				try {
					quickBondInfoForm = quickBondSubmit(quickBondInfoForm);
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			   
			    
			    try {
				    tableMethods t = null;
					t = new tableMethods();
					LinkedHashMap<String, Table> tables = t.QuickbondInfoQuery(quickBondInfoForm);
					if(tables.isEmpty() == false) {
					    DirectoryDialog dialog = new DirectoryDialog(BondInfoComposite.getShell());
						String filename = dialog.open();
						int count = 0;
						if( (filename != null) && !filename.equals(cusipDef) && !tables.isEmpty()) {
							for(String key: tables.keySet()) {
								System.out.println(filename);
								if(tables.get(key).isEmpty() == false) {
									//CsvWriteOptions.Builder builder = CsvWriteOptions.builder(filename + "/" + key + ".csv").separator(',');
      							    //	CsvWriteOptions options = builder.build();
  									tables.get(key).write().csv(filename + "/" + key + ".csv");
      									count += 1;
								}

								
								
							}
							MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
							if(count == 0) {
								Messagedialog.setMessage("No Results Found, Check Console for queries");	
	
							}
							else {
								Messagedialog.setMessage("Confirmed Results, Wrote " + count + " Tables");
							}
						    t = null;
							Messagedialog.setText("Results Written to: " + filename);
							Messagedialog.open();
					}
					}
					else {
						MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
						Messagedialog.setMessage("No Results Found, Check Console for queries");	
						Messagedialog.open();

					}
					
				} catch (SQLException | BAMBOBException | ParseException | InvalidFormatException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				quickBondInfoForm = null;

			}
				
			
		});
		btnQuickBondSubmit.setBounds(588, 725, 117, 47);
		
				Button btnQuickBondGetAGM = formToolkit.createButton(QuickBondSearch, "New Secondary AGM Weekly Report", SWT.BORDER);
				btnQuickBondGetAGM.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
					   	BD2 currdate = new BD2();
				    	BD2 date = BD2.addDays(currdate, -1);
				    	int daysBefore = 5;
				    	while(daysBefore > 1) {
				    	
				    		while(!isBusinessDay(date)) {
				    		date = BD2.addDays(date, -1);
				    		}
				    	date = BD2.addDays(date, -1);
				    	daysBefore = daysBefore -1;
				    	
				    	}
				    	String currdateString = BD2.BD2toDate(currdate).toString();				

				    	String prevDate = BD2.BD2toDate(date).toString();				
						quickBond quickBondInfoForm = new quickBond();	
						ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(prevDate,currdateString,"R"));
						quickBondInfoForm.UpdatedDate = dateArray;
						quickBondInfoForm.addArg(quickBondInfoForm.bondInfoDictionary, quickBondInfoForm.getUpdatedDate(), "update_date_d");
						System.out.println("Updated Date: " + quickBondInfoForm.getUpdatedDate().toString());
						
						int[] sel = new int[] {3};
						ArrayList<String> listArray = selectArrayList(sel, bondInsuranceCodes);			
						quickBondInfoForm.setInsurance(listArray);
						quickBondInfoForm.bondInfoTrading.setTradingChecks(true);
					    try {
						    tableMethods t = null;
							t = new tableMethods();
							File f = new File(System.getProperty("java.class.path"));
					    	File dir = f.getAbsoluteFile().getParentFile();
					    	String path = dir.toString();
					    	//SIMILIAR PROTOCOLL TO PROPERTIES
							//quickBondInfoForm.setAGMFilter("agm_portfolio.csv");
							quickBondInfoForm.setAGMFilter(path + "/agm_portfolio.csv");

							
							LinkedHashMap<String, Table> tables = t.QuickbondInfoQuery(quickBondInfoForm);
							if(tables.isEmpty() == false) {
							    DirectoryDialog dialog = new DirectoryDialog(BondInfoComposite.getShell());
								String filename = dialog.open();
								if( (filename != null) && !filename.equals(cusipDef) && !tables.isEmpty()) {
									int count = 0;
									for(String key: tables.keySet()) {
										System.out.println(filename);
										if(tables.get(key).isEmpty() == false) {
											//CsvWriteOptions.Builder builder = CsvWriteOptions.builder(filename + "/" + key + ".csv").separator(',');
	      							    	//CsvWriteOptions options = builder.build();
          									tables.get(key).write().csv(filename + "/" + key + ".csv");
	      									count += 1;
										}
										
					
										
									}
									MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
									if(count == 0) {
										Messagedialog.setMessage("No Results Found, Check Console for queries");	
									}
									else {
										Messagedialog.setMessage("Confirmed Results, Wrote " + count + " Tables");
										Messagedialog.setText("Results Written to: " + filename);

									}
									t = null;
									Messagedialog.open();
							}
							}
							
							else {
								MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
								Messagedialog.setMessage("No Results Found, Check Console for queries");	
								Messagedialog.open();

							}
							
						} catch (SQLException | BAMBOBException | ParseException | InvalidFormatException | IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						quickBondInfoForm = null;

					}
					
					
				});
				btnQuickBondGetAGM.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
				btnQuickBondGetAGM.setBounds(160, 555, 305, 62);
				


				
				
				btnQuickBondUploadMmd.setData(cusipDef);
				
				btnQuickBondFilterCusips = new Button(QuickBondTradingConfig, SWT.BORDER | SWT.CHECK);
				btnQuickBondFilterCusips.setText("Filter Cusips");
				btnQuickBondFilterCusips.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
				btnQuickBondFilterCusips.setAlignment(SWT.CENTER);
				btnQuickBondFilterCusips.setBounds(717, 197, 168, 25);
				formToolkit.adapt(btnQuickBondFilterCusips, true, true);
				btnQuickBondCusipList.setData(cusipDef);
				
				Button btnQuickBondGetAGMT1 = formToolkit.createButton(QuickBondSearch, "Secondary AGM Daily Report T2", SWT.BORDER);
				btnQuickBondGetAGMT1.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
					   	BD2 currdate = new BD2();
				    	BD2 date = BD2.addDays(currdate, -1);
				    	int daysBefore = 2;
				    	while(daysBefore > 1) {
				    	
				    		while(!isBusinessDay(date)) {
				    		date = BD2.addDays(date, -1);
				    		}
				    	date = BD2.addDays(date, -1);
				    	daysBefore = daysBefore -1;
				    	
				    	}
				    	String currdateString = BD2.BD2toDate(currdate).toString();				

				    	String prevDate = BD2.BD2toDate(date).toString();				
						quickBond quickBondInfoForm = new quickBond();	
						ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(prevDate,currdateString,"R"));
						quickBondInfoForm.bondInfoTrading.tradeDate  = dateArray;
						quickBondInfoForm.addArg(quickBondInfoForm.bondInfoTrading.tradeInfoDictionary, quickBondInfoForm.bondInfoTrading.getTradeDate(), "trade_date");

						quickBondInfoForm.bondInfoTrading.tradingChecks = true;
						quickBondInfoForm.bondInfoTrading.cusipFilter = true;
						System.out.println("TRADE Date: " + quickBondInfoForm.bondInfoTrading.getTradeDate().toString());
						
						int[] sel = new int[] {3};
						ArrayList<String> listArray = selectArrayList(sel, bondInsuranceCodes);			
						quickBondInfoForm.setInsurance(listArray);
						quickBondInfoForm.bondInfoTrading.setTradingChecks(true);
						quickBondInfoForm.bondInfoTrading.setCalculateTradingYield(btnQuickBondCalculateTradingYield.getSelection());
					    try {
						    tableMethods t = null;
							t = new tableMethods();
							
							LinkedHashMap<String, Table> tables = t.QuickbondInfoQuery(quickBondInfoForm);
							if(tables.isEmpty() == false) {
							    DirectoryDialog dialog = new DirectoryDialog(BondInfoComposite.getShell());
								String filename = dialog.open();
								if( (filename != null) && !filename.equals(cusipDef) && !tables.isEmpty()) {
									int count = 0;
									for(String key: tables.keySet()) {
										System.out.println(filename);
										if(tables.get(key).isEmpty() == false) {
											//CsvWriteOptions.Builder builder = CsvWriteOptions.builder(filename + "/" + key + ".csv").separator(',');
              							    //	CsvWriteOptions options = builder.build();
          									tables.get(key).write().csv(filename + "/" + key + ".csv");
              									count += 1;
										}
										
									}
									t = null;

									MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
									if(count == 0) {
										Messagedialog.setMessage("No Results Found, Check Console for queries");	
									}
									else {
										Messagedialog.setMessage("Confirmed Results, Wrote " + count + " Tables");
										Messagedialog.setText("Results Written to: " + filename);

									}
									Messagedialog.open();
							}
							}
							
							else {
								MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
								Messagedialog.setMessage("No Results Found, Check Console for queries");	
								Messagedialog.open();

							}
							
						} catch (SQLException | BAMBOBException | ParseException | InvalidFormatException | IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						quickBondInfoForm = null;

					}
				});
				btnQuickBondGetAGMT1.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
				btnQuickBondGetAGMT1.setBounds(484, 555, 297, 62);
				
				Button btnQuickBondGetAGMT5 = formToolkit.createButton(QuickBondSearch, "Secondary AGM Daily Report T5", SWT.BORDER);
				btnQuickBondGetAGMT5.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
					   	BD2 currdate = new BD2();
				    	BD2 date = BD2.addDays(currdate, -1);
				    	int daysBefore = 5;
				    	while(daysBefore > 1) {
				    	
				    		while(!isBusinessDay(date)) {
				    		date = BD2.addDays(date, -1);
				    		}
				    	date = BD2.addDays(date, -1);
				    	daysBefore = daysBefore -1;
				    	
				    	}
				    	String currdateString = BD2.BD2toDate(currdate).toString();				

				    	String prevDate = BD2.BD2toDate(date).toString();				
						quickBond quickBondInfoForm = new quickBond();	
						ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(prevDate,currdateString,"R"));
						quickBondInfoForm.bondInfoTrading.tradeDate  = dateArray;
						quickBondInfoForm.addArg(quickBondInfoForm.bondInfoTrading.tradeInfoDictionary, quickBondInfoForm.bondInfoTrading.getTradeDate(), "trade_date");

						quickBondInfoForm.bondInfoTrading.tradingChecks = true;
						quickBondInfoForm.bondInfoTrading.cusipFilter = true;
						System.out.println("TRADE Date: " + quickBondInfoForm.bondInfoTrading.getTradeDate().toString());
						
						int[] sel = new int[] {3};
						ArrayList<String> listArray = selectArrayList(sel, bondInsuranceCodes);			
						quickBondInfoForm.setInsurance(listArray);
						quickBondInfoForm.bondInfoTrading.setTradingChecks(true);
						quickBondInfoForm.bondInfoTrading.setCalculateTradingYield(btnQuickBondCalculateTradingYield.getSelection());

					    try {
						    tableMethods t = null;
							t = new tableMethods();
							
							LinkedHashMap<String, Table> tables = t.QuickbondInfoQuery(quickBondInfoForm);
							if(tables.isEmpty() == false) {
							    DirectoryDialog dialog = new DirectoryDialog(BondInfoComposite.getShell());
								String filename = dialog.open();
								if( (filename != null) && !filename.equals(cusipDef) && !tables.isEmpty()) {
									int count = 0;
									for(String key: tables.keySet()) {
										System.out.println(filename);
										if(tables.get(key).isEmpty() == false) {
									    	//CsvWriteOptions.Builder builder = CsvWriteOptions.builder(filename + "/" + key + ".csv").separator(',');
              							    //	CsvWriteOptions options = builder.build();
          									tables.get(key).write().csv(filename + "/" + key + ".csv");
              									count += 1;
										}
									}
									MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
									if(count == 0) {
										Messagedialog.setMessage("No Results Found, Check Console for queries");	
									}
									else {
										Messagedialog.setMessage("Confirmed Results, Wrote " + count + " Tables");
										Messagedialog.setText("Results Written to: " + filename);

									}
									t = null;
									Messagedialog.open();
							}
							}
							
							else {
								MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
								Messagedialog.setMessage("No Results Found, Check Console for queries");	
								Messagedialog.open();

							}
							
						} catch (SQLException | BAMBOBException | ParseException | InvalidFormatException | IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						quickBondInfoForm = null;

						
						
						
						
					}
				});
				btnQuickBondGetAGMT5.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
				btnQuickBondGetAGMT5.setBounds(795, 555, 297, 62);
		
		
		
		TabItem tbtmTradingCusipVisualiser = new TabItem(tabFolder, 0);
		tbtmTradingCusipVisualiser.setText("Trading Cusip Visualiser");
		
		Composite TradingCusipVisualiser = formToolkit.createComposite(tabFolder, SWT.BORDER);
		TradingCusipVisualiser.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		tbtmTradingCusipVisualiser.setControl(TradingCusipVisualiser);
		TradingCusipVisualiser.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		formToolkit.paintBordersFor(TradingCusipVisualiser);
		

		
		Composite TCVTradingConfigurations = formToolkit.createComposite(TradingCusipVisualiser, SWT.BORDER);
		TCVTradingConfigurations.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVTradingConfigurations.setBackground(SWTResourceManager.getColor(186,86,36));
		TCVTradingConfigurations.setBounds(158, 288, 1223, 256);
		formToolkit.paintBordersFor(TCVTradingConfigurations);
		
		Composite TCVParDollarPrice = formToolkit.createComposite(TCVTradingConfigurations, SWT.BORDER);
		TCVParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVParDollarPrice.setBounds(743, 51, 177, 114);
		TCVParDollarPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(TCVParDollarPrice);
		
		maxTCVParDollarPrice = new Spinner(TCVParDollarPrice, SWT.BORDER);
		maxTCVParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxTCVParDollarPrice.setMaximum(2000000000);
		maxTCVParDollarPrice.setDigits(4);
		maxTCVParDollarPrice.setBounds(50, 78, 113, 22);
		formToolkit.adapt(maxTCVParDollarPrice);
		formToolkit.paintBordersFor(maxTCVParDollarPrice);
		
		Label lblMaxTCVParDollarPrice = new Label(TCVParDollarPrice, SWT.NONE);
		lblMaxTCVParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxTCVParDollarPrice.setText("Max");
		lblMaxTCVParDollarPrice.setBounds(1, 78, 36, 22);
		formToolkit.adapt(lblMaxTCVParDollarPrice, true, true);
		
		Label lblMinTCVParDollarPrice = new Label(TCVParDollarPrice, SWT.NONE);
		lblMinTCVParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinTCVParDollarPrice.setText("Min");
		lblMinTCVParDollarPrice.setBounds(1, 40, 32, 22);
		formToolkit.adapt(lblMinTCVParDollarPrice, true, true);
		
		minTCVParDollarPrice = new Spinner(TCVParDollarPrice, SWT.BORDER | SWT.WRAP);
		minTCVParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minTCVParDollarPrice.setMaximum(2000000000);
		minTCVParDollarPrice.setDigits(4);
		minTCVParDollarPrice.setBounds(50, 37, 113, 22);
		formToolkit.adapt(minTCVParDollarPrice);
		formToolkit.paintBordersFor(minTCVParDollarPrice);
		
		Label lblTCVParDollarPrice = new Label(TCVParDollarPrice, SWT.NONE);
		lblTCVParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTCVParDollarPrice.setAlignment(SWT.CENTER);
		lblTCVParDollarPrice.setText("Par Dollar Price");
		lblTCVParDollarPrice.setBounds(0, 0, 173, 24);
		formToolkit.adapt(lblTCVParDollarPrice, true, true);
		
		Composite TCVParAmountTraded = formToolkit.createComposite(TCVTradingConfigurations, SWT.BORDER);
		TCVParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVParAmountTraded.setBounds(545, 51, 192, 114);
		TCVParAmountTraded.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(TCVParAmountTraded);
		
		maxTCVParAmountTraded = new Spinner(TCVParAmountTraded, SWT.BORDER);
		maxTCVParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxTCVParAmountTraded.setMaximum(2000000000);
		maxTCVParAmountTraded.setBounds(45, 82, 129, 22);
		formToolkit.adapt(maxTCVParAmountTraded);
		formToolkit.paintBordersFor(maxTCVParAmountTraded);
		
		minTCVParAmountTraded = new Spinner(TCVParAmountTraded, SWT.BORDER);
		minTCVParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minTCVParAmountTraded.setMaximum(2000000000);
		minTCVParAmountTraded.setBounds(45, 41, 129, 22);
		formToolkit.adapt(minTCVParAmountTraded);
		formToolkit.paintBordersFor(minTCVParAmountTraded);
		
		Label lblTCVParAmountTraded = new Label(TCVParAmountTraded, SWT.NONE);
		lblTCVParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTCVParAmountTraded.setAlignment(SWT.CENTER);
		lblTCVParAmountTraded.setText("Par Amount Traded");
		lblTCVParAmountTraded.setBounds(0, 0, 188, 24);
		formToolkit.adapt(lblTCVParAmountTraded, true, true);
		
		Label lblMinTCVParAmountTraded = new Label(TCVParAmountTraded, SWT.NONE);
		lblMinTCVParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinTCVParAmountTraded.setText("Min");
		lblMinTCVParAmountTraded.setBounds(0, 41, 32, 22);
		formToolkit.adapt(lblMinTCVParAmountTraded, true, true);
		
		Label lblMaxTCVParAmountTraded = new Label(TCVParAmountTraded, SWT.NONE);
		lblMaxTCVParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxTCVParAmountTraded.setText("Max");
		lblMaxTCVParAmountTraded.setBounds(0, 82, 32, 22);
		formToolkit.adapt(lblMaxTCVParAmountTraded, true, true);
		
		Composite TCVTradeDate = formToolkit.createComposite(TCVTradingConfigurations, SWT.BORDER);
		TCVTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVTradeDate.setBounds(10, 51, 169, 114);
		TCVTradeDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(TCVTradeDate);
		
		Label lblTCVTradeDate = new Label(TCVTradeDate, SWT.BORDER);
		lblTCVTradeDate.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblTCVTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTCVTradeDate.setAlignment(SWT.CENTER);
		lblTCVTradeDate.setText("Trade Date");
		lblTCVTradeDate.setBounds(0, 0, 165, 24);
		formToolkit.adapt(lblTCVTradeDate, true, true);
		
		Label lblMinTCVTradeDate = formToolkit.createLabel(TCVTradeDate, "Min", SWT.BORDER);
		lblMinTCVTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinTCVTradeDate.setBounds(0, 35, 32, 28);
		
		Label lblMaxTradeCallDate = formToolkit.createLabel(TCVTradeDate, "Max", SWT.BORDER);
		lblMaxTradeCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxTradeCallDate.setBounds(0, 73, 38, 27);
		
		minTCVTradeDate = new DateChooserCombo(TCVTradeDate, SWT.NONE);
		minTCVTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minTCVTradeDate.setBounds(54, 35, 103, 27);
		formToolkit.adapt(minTCVTradeDate);
		formToolkit.paintBordersFor(minTCVTradeDate);
		
		maxTCVTradeDate = new DateChooserCombo(TCVTradeDate, SWT.NONE);
		maxTCVTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxTCVTradeDate.setBounds(54, 73, 103, 27);
		formToolkit.adapt(maxTCVTradeDate);
		formToolkit.paintBordersFor(maxTCVTradeDate);

		
		
		Label lblTCVTradingConfigurations = new Label(TCVTradingConfigurations, SWT.NONE);
		lblTCVTradingConfigurations.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTCVTradingConfigurations.setBounds(479, 0, 246, 29);
		lblTCVTradingConfigurations.setText("Trading Configurations");
		lblTCVTradingConfigurations.setAlignment(SWT.CENTER);
		formToolkit.adapt(lblTCVTradingConfigurations, true, true);
		
		Composite TCVParTradedYield = formToolkit.createComposite(TCVTradingConfigurations, SWT.BORDER);
		TCVParTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVParTradedYield.setBounds(928, 51, 171, 114);
		TCVParTradedYield.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(TCVParTradedYield);
		
		maxTCVTradedYield = new Spinner(TCVParTradedYield, SWT.BORDER);
		maxTCVTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxTCVTradedYield.setMaximum(2000000000);
		maxTCVTradedYield.setDigits(4);
		maxTCVTradedYield.setBounds(57, 79, 100, 22);
		formToolkit.adapt(maxTCVTradedYield);
		formToolkit.paintBordersFor(maxTCVTradedYield);
		
		Label lblMinTCVTradedYield = new Label(TCVParTradedYield, SWT.NONE);
		lblMinTCVTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinTCVTradedYield.setText("Min");
		lblMinTCVTradedYield.setBounds(0, 41, 30, 22);
		formToolkit.adapt(lblMinTCVTradedYield, true, true);
		
		minTCVTradedYield = new Spinner(TCVParTradedYield, SWT.BORDER | SWT.WRAP);
		minTCVTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minTCVTradedYield.setMaximum(2000000000);
		minTCVTradedYield.setDigits(4);
		minTCVTradedYield.setBounds(57, 38, 100, 22);
		formToolkit.adapt(minTCVTradedYield);
		formToolkit.paintBordersFor(minTCVTradedYield);
		
		Label lblTCVParTradedYield = new Label(TCVParTradedYield, SWT.NONE);
		lblTCVParTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTCVParTradedYield.setAlignment(SWT.CENTER);
		lblTCVParTradedYield.setText("Par Traded Yield");
		lblTCVParTradedYield.setBounds(0, 0, 167, 24);
		formToolkit.adapt(lblTCVParTradedYield, true, true);
		
		Label lblMaxTCVTradedYield = new Label(TCVParTradedYield, SWT.NONE);
		lblMaxTCVTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxTCVTradedYield.setText("Max");
		lblMaxTCVTradedYield.setBounds(2, 79, 36, 22);
		formToolkit.adapt(lblMaxTCVTradedYield, true, true);
		
		Composite TCVTradeType = formToolkit.createComposite(TCVTradingConfigurations, SWT.BORDER);
		TCVTradeType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVTradeType.setBounds(1106, 51, 103, 114);
		TCVTradeType.setBackground(SWTResourceManager.getColor(32, 44, 57));
		formToolkit.paintBordersFor(TCVTradeType);
		
		Label lblTCVTradeType = new Label(TCVTradeType, SWT.NONE);
		lblTCVTradeType.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		lblTCVTradeType.setAlignment(SWT.CENTER);
		lblTCVTradeType.setText("Trade Type");
		lblTCVTradeType.setBounds(0, 0, 101, 24);
		formToolkit.adapt(lblTCVTradeType, true, true);
		
		btnTCVTradeTypeD = new Button(TCVTradeType, SWT.CHECK);
		btnTCVTradeTypeD.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		btnTCVTradeTypeD.setText("D");
		btnTCVTradeTypeD.setBounds(6, 36, 85, 18);
		formToolkit.adapt(btnTCVTradeTypeD, true, true);
		
		btnTCVTradeTypeS = new Button(TCVTradeType, SWT.CHECK);
		btnTCVTradeTypeS.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		btnTCVTradeTypeS.setText("S");
		btnTCVTradeTypeS.setBounds(6, 60, 85, 18);
		formToolkit.adapt(btnTCVTradeTypeS, true, true);
		
		btnTCVTradeTypeP = new Button(TCVTradeType, SWT.CHECK);
		btnTCVTradeTypeP.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		btnTCVTradeTypeP.setText("P");
		btnTCVTradeTypeP.setBounds(6, 84, 85, 18);
		formToolkit.adapt(btnTCVTradeTypeP, true, true);
		
		Composite TCVMaturityDate = formToolkit.createComposite(TCVTradingConfigurations, SWT.BORDER);
		TCVMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVMaturityDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		TCVMaturityDate.setBounds(185, 51, 171, 114);
		formToolkit.paintBordersFor(TCVMaturityDate);
		
		Label lblTCVMaturityDate = new Label(TCVMaturityDate, SWT.BORDER);
		lblTCVMaturityDate.setText("Maturity Date");
		lblTCVMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTCVMaturityDate.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblTCVMaturityDate.setAlignment(SWT.CENTER);
		lblTCVMaturityDate.setBounds(0, 0, 167, 24);
		formToolkit.adapt(lblTCVMaturityDate, true, true);
		
		Label lblMinTCVMaturityDate = formToolkit.createLabel(TCVMaturityDate, "Min", SWT.BORDER);
		lblMinTCVMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinTCVMaturityDate.setBounds(0, 38, 32, 24);
		
		Label lblMaxTCVMaturityDate = formToolkit.createLabel(TCVMaturityDate, "Max", SWT.BORDER);
		lblMaxTCVMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxTCVMaturityDate.setBounds(0, 73, 32, 27);
		
		minTCVMaturityDate = new DateChooserCombo(TCVMaturityDate, SWT.NONE);
		minTCVMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minTCVMaturityDate.setBounds(48, 35, 103, 27);
		formToolkit.adapt(minTCVMaturityDate);
		formToolkit.paintBordersFor(minTCVMaturityDate);
		
		maxTCVMaturityDate = new DateChooserCombo(TCVMaturityDate, SWT.NONE);
		maxTCVMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxTCVMaturityDate.setBounds(48, 73, 103, 27);
		formToolkit.adapt(maxTCVMaturityDate);
		formToolkit.paintBordersFor(maxTCVMaturityDate);
		
		Composite TCVCoupon = formToolkit.createComposite(TCVTradingConfigurations, SWT.BORDER);
		TCVCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVCoupon.setBackground(SWTResourceManager.getColor(32, 44, 57));
		TCVCoupon.setBounds(362, 51, 177, 114);
		formToolkit.paintBordersFor(TCVCoupon);
		
		maxTCVCoupon = new Spinner(TCVCoupon, SWT.BORDER);
		maxTCVCoupon.setMaximum(2000000000);
		maxTCVCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		maxTCVCoupon.setDigits(4);
		maxTCVCoupon.setBounds(61, 79, 102, 22);
		formToolkit.adapt(maxTCVCoupon);
		formToolkit.paintBordersFor(maxTCVCoupon);
		
		Label lblMinTCVCoupon = new Label(TCVCoupon, SWT.NONE);
		lblMinTCVCoupon.setText("Min");
		lblMinTCVCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMinTCVCoupon.setBounds(0, 41, 30, 22);
		formToolkit.adapt(lblMinTCVCoupon, true, true);
		
		minTCVCoupon = new Spinner(TCVCoupon, SWT.BORDER | SWT.WRAP);
		minTCVCoupon.setMaximum(2000000000);
		minTCVCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		minTCVCoupon.setDigits(4);
		minTCVCoupon.setBounds(61, 38, 102, 22);
		formToolkit.adapt(minTCVCoupon);
		formToolkit.paintBordersFor(minTCVCoupon);
		
		Label lblTCVCoupon = new Label(TCVCoupon, SWT.NONE);
		lblTCVCoupon.setText("Coupon");
		lblTCVCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTCVCoupon.setAlignment(SWT.CENTER);
		lblTCVCoupon.setBounds(0, 0, 173, 24);
		formToolkit.adapt(lblTCVCoupon, true, true);
		
		Label lblMaxTCVCoupon = new Label(TCVCoupon, SWT.NONE);
		lblMaxTCVCoupon.setText("Max");
		lblMaxTCVCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblMaxTCVCoupon.setBounds(2, 79, 36, 22);
		formToolkit.adapt(lblMaxTCVCoupon, true, true);
		
		btnTCVUploadMmd = formToolkit.createButton(TCVTradingConfigurations, "Upload MMD", SWT.BORDER);
		btnTCVUploadMmd.setLocation(535, 204);
		btnTCVUploadMmd.setSize(125, 38);
		btnTCVUploadMmd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				  FileDialog fileDialog = new FileDialog(TradingCusipVisualiser.getShell());
				    /*fileDialog.setFilterNames(new String[] { "Microsoft Excel Macro (*.xlsm)",
				            "Microsoft Excel Spreadsheet Files (*.xls)", "Microsoft Open XML Spreadsheet (*.xlsx)", "Comma Separated Values Files (*.csv)",
				            "All Files (*.*)" });
					*/
				    fileDialog.setFilterExtensions(new String[] { "*.xlsm", "*.xls", "*.xlsx", "*.csv", "*.*" });
				    String filename = fileDialog.open();
				    btnTCVUploadMmd.setData(filename);
			}
		});
		btnTCVUploadMmd.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		

		btnTCVUploadMmd.setData(cusipDef);
		
		Composite TCVCusipSearch = formToolkit.createComposite(TradingCusipVisualiser, SWT.BORDER);
		TCVCusipSearch.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		TCVCusipSearch.setBackground(SWTResourceManager.getColor(32,54,56));
		TCVCusipSearch.setBounds(365, 82, 804, 187);
		formToolkit.paintBordersFor(TCVCusipSearch);
		
		btnTCVCusipList = formToolkit.createButton(TCVCusipSearch, "Upload Seperate Cusip File", SWT.BORDER);
		btnTCVCusipList.setData(cusipDef);
		btnTCVCusipList.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				  FileDialog fileDialog = new FileDialog(TCVCusipSearch.getShell());
				    /*fileDialog.setFilterNames(new String[] { "Comma Separated Values Files  (*.csv)",
				            "Microsoft Excel Spreadsheet Files (*.xls)", "Microsoft Excel Open XML Spreadsheet Files (*.xlsx)", "Microsoft Excel Macro (*.xlsm)",
				            "All Files (*.*)" });
					*/
				    fileDialog.setFilterExtensions(new String[] { "*.csv","*.xls", "*.xlsx", "*.xlsm", "*.*" });
				    String filename = fileDialog.open();
				    btnTCVCusipList.setData(filename);
			
			}
		});
		btnTCVCusipList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnTCVCusipList.setBounds(258, 141, 263, 32);
		
		Label lblTypeInCusip = formToolkit.createLabel(TCVCusipSearch, "Type in Cusip 6 or 9 by Comma", SWT.BORDER | SWT.CENTER);
		lblTypeInCusip.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		lblTypeInCusip.setBounds(230, 0, 324, 22);
		
		txtTCVCusipSearch = formToolkit.createText(TCVCusipSearch, "New Text", SWT.BORDER);
		txtTCVCusipSearch.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		txtTCVCusipSearch.setText("");
		txtTCVCusipSearch.setBounds(126, 68, 474, 32);
		
		
		
		Button btnBondInfoSubmit = formToolkit.createButton(BondInfoComposite, "Submit", SWT.BORDER);
		btnBondInfoSubmit.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Bond Info");

				bondInfo bondInfoForm = new bondInfo();		
				bondInfoForm = bondInfoSubmit(bondInfoForm);
			   
			    
			    try {
				    tableMethods t = null;
					t = new tableMethods();
					System.out.println("MMD: " + bondInfoForm.bondInfoTrading.getMMDTable());
					LinkedHashMap<String, Table> tables = t.bondInfoQuery(bondInfoForm);
					if(tables.isEmpty() == false) {
					    DirectoryDialog dialog = new DirectoryDialog(BondInfoComposite.getShell());
							String filename = dialog.open();
							if( (filename != null) && !filename.equals(cusipDef) && !tables.isEmpty()) {
								
								int count = 0;
								for(String key: tables.keySet()) {
									System.out.println(filename);
									if(tables.get(key).isEmpty() == false) {
											//CsvWriteOptions.Builder builder = CsvWriteOptions.builder(filename + "/" + key + ".csv").separator(',');
          							    	//CsvWriteOptions options = builder.build();
          									tables.get(key).write().csv(filename + "/" + key + ".csv");
          									count += 1;
									}
								}
								MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
								if(count == 0) {
									Messagedialog.setMessage("No Results Found, Check Console for queries");	
								}
								else {
									Messagedialog.setMessage("Confirmed Results, Wrote " + count + " Tables");

								}
								t = null;
								Messagedialog.setText("Results Written to: " + filename);
								Messagedialog.open();
								

					}
					}
					else {
						MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
						Messagedialog.setMessage("No Results Found, Check Console for queries");	
						Messagedialog.open();

					}
					bondInfoForm = null;
				} catch (SQLException | BAMBOBException | ParseException | InvalidFormatException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
		});
		
	    
		btnBondInfoSubmit.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnBondInfoSubmit.setBounds(835, 732, 96, 42);
		
		Button btnTCVSubmit = formToolkit.createButton(TradingCusipVisualiser, "Submit", SWT.BORDER);
		btnTCVSubmit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("TCV Form");
				tradingCusipVisualiser TCVBondForm = new tradingCusipVisualiser();
				try {
					TCVBondForm = TCVSubmit(TCVBondForm);
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			   
			    try {
				    tableMethods t = null;
					t = new tableMethods();
					LinkedHashMap<String, Table> tables = t.TCVInfoQuery(TCVBondForm);
					if(tables.isEmpty() == false) {
					    DirectoryDialog dialog = new DirectoryDialog(BondInfoComposite.getShell());
						String filename = dialog.open();
						if( (filename != null) && !filename.equals(cusipDef) && !tables.isEmpty()) {
							int count = 0;
							for(String key: tables.keySet()) {
								System.out.println(filename);
								if(tables.get(key).isEmpty() == false) {
									//CsvWriteOptions.Builder builder = CsvWriteOptions.builder(filename + "/" + key + ".csv").separator(',');
      							    //	CsvWriteOptions options = builder.build();
      									tables.get(key).write().csv(filename + "/" + key + ".csv");
      									count += 1;
								}
							}
							MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
							if(count == 0) {
								Messagedialog.setMessage("No Results Found, Check Console for queries");	
							}
							else {
								Messagedialog.setMessage("Confirmed Results, Wrote " + count + " Tables");
							}
							t = null;
							Messagedialog.open();
					}
					}
					
					else {
						MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
						Messagedialog.setMessage("No Results Found, Check Console for queries");	
						Messagedialog.open();

					}
					
					TCVBondForm = null;
				} catch (SQLException | BAMBOBException | ParseException | InvalidFormatException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		
		
		
		
		btnTCVSubmit.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 15, SWT.NORMAL));
		btnTCVSubmit.setBounds(551, 617, 125, 60);
		
		
		btnUploadMmd.setData(cusipDef);
		
		btnFilterCusips = new Button(bondInfoTradingInfoConfig, SWT.BORDER | SWT.CHECK);
		btnFilterCusips.setText("Filter Cusips");
		btnFilterCusips.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		btnFilterCusips.setAlignment(SWT.CENTER);
		btnFilterCusips.setBounds(610, 171, 167, 34);
		formToolkit.adapt(btnFilterCusips, true, true);
		
		
		
		btnTCVCusipList.setData(cusipDef);
		
		Button btnTCVlearInputs = formToolkit.createButton(TradingCusipVisualiser, "Clear Inputs", SWT.BORDER);
		btnTCVlearInputs.setBounds(755, 627, 137, 44);
		btnTCVlearInputs.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TCVClear();
			}
		});
		btnTCVlearInputs.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		
		               		TabItem CheapTradeTab = new TabItem(tabFolder, SWT.NONE);
		               		CheapTradeTab.setText("Cheap Trade Search");
		               		
		               		Composite CheapTradeComposite = new Composite(tabFolder, SWT.BORDER);
		               		CheapTradeTab.setControl(CheapTradeComposite);
		               		CheapTradeComposite.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		               		CheapTradeComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		               		formToolkit.paintBordersFor(CheapTradeComposite);
		               		
		               		Composite CheapTradeMaturityDate = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		CheapTradeMaturityDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		CheapTradeMaturityDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		CheapTradeMaturityDate.setBounds(93, 26, 202, 114);
		               		formToolkit.paintBordersFor(CheapTradeMaturityDate);
		               		
		               		Label lblMaturityDate_1_1 = new Label(CheapTradeMaturityDate, SWT.BORDER);
		               		lblMaturityDate_1_1.setText("Maturity Date");
		               		lblMaturityDate_1_1.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		lblMaturityDate_1_1.setAlignment(SWT.CENTER);
		               		lblMaturityDate_1_1.setBounds(0, 0, 198, 23);
		               		formToolkit.adapt(lblMaturityDate_1_1, true, true);
		               		
		               		Label lblCheapTradeMinMatDate = formToolkit.createLabel(CheapTradeMaturityDate, "Min", SWT.BORDER);
		               		lblCheapTradeMinMatDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		lblCheapTradeMinMatDate.setBounds(10, 36, 33, 23);
		               		
		               		Label lblCheapTradeMaxMatDate = formToolkit.createLabel(CheapTradeMaturityDate, "Max", SWT.BORDER);
		               		lblCheapTradeMaxMatDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		lblCheapTradeMaxMatDate.setBounds(10, 78, 44, 22);
		               		
		               		 minCheapTradeMatDate = new DateChooserCombo(CheapTradeMaturityDate, SWT.NONE);
		               		 minCheapTradeMatDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		 minCheapTradeMatDate.setBounds(76, 37, 112, 22);
		               		 formToolkit.adapt(minCheapTradeMatDate);
		               		 formToolkit.paintBordersFor(minCheapTradeMatDate);
		               		 
		               		  maxCheapTradeMatDate = new DateChooserCombo(CheapTradeMaturityDate, SWT.NONE);
		               		  maxCheapTradeMatDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		  maxCheapTradeMatDate.setBounds(76, 78, 112, 22);
		               		  formToolkit.adapt(maxCheapTradeMatDate);
		               		  formToolkit.paintBordersFor(maxCheapTradeMatDate);
		               		  
		               		  Composite CheapTradeDatedDate = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		  CheapTradeDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		  CheapTradeDatedDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		  CheapTradeDatedDate.setBounds(301, 26, 187, 114);
		               		  formToolkit.paintBordersFor(CheapTradeDatedDate);
		               		  
		               		  Label lblCheapTradeDated_Date = new Label(CheapTradeDatedDate, SWT.NONE);
		               		  lblCheapTradeDated_Date.setText("Dated Date");
		               		  lblCheapTradeDated_Date.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		  lblCheapTradeDated_Date.setAlignment(SWT.CENTER);
		               		  lblCheapTradeDated_Date.setBounds(0, 0, 183, 23);
		               		  formToolkit.adapt(lblCheapTradeDated_Date, true, true);
		               		  
		               		  Label lblCheapTradeMinDatedDate = new Label(CheapTradeDatedDate, SWT.NONE);
		               		  lblCheapTradeMinDatedDate.setText("Min");
		               		  lblCheapTradeMinDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		  lblCheapTradeMinDatedDate.setBounds(0, 29, 38, 23);
		               		  formToolkit.adapt(lblCheapTradeMinDatedDate, true, true);
		               		  
		               		  Label lblCheapTradeMaxDatedDate = new Label(CheapTradeDatedDate, SWT.NONE);
		               		  lblCheapTradeMaxDatedDate.setText("Max");
		               		  lblCheapTradeMaxDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		  lblCheapTradeMaxDatedDate.setBounds(2, 75, 36, 24);
		               		  formToolkit.adapt(lblCheapTradeMaxDatedDate, true, true);
		               		  
		               		   maxCheapTradeDatedDate = new DateChooserCombo(CheapTradeDatedDate, SWT.NONE);
		               		   maxCheapTradeDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		   maxCheapTradeDatedDate.setBounds(61, 75, 112, 24);
		               		   formToolkit.adapt(maxCheapTradeDatedDate);
		               		   formToolkit.paintBordersFor(maxCheapTradeDatedDate);
		               		   
		               		    minCheapTradeDatedDate = new DateChooserCombo(CheapTradeDatedDate, SWT.NONE);
		               		    minCheapTradeDatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		    minCheapTradeDatedDate.setBounds(61, 30, 112, 24);
		               		    formToolkit.adapt(minCheapTradeDatedDate);
		               		    formToolkit.paintBordersFor(minCheapTradeDatedDate);
		               		    
		               		    Composite CheapTradeCoupon = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		    CheapTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		    CheapTradeCoupon.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		    CheapTradeCoupon.setBounds(494, 26, 167, 114);
		               		    formToolkit.paintBordersFor(CheapTradeCoupon);
		               		    
		               		     maxCheapTradeCoupon = new Spinner(CheapTradeCoupon, SWT.BORDER);
		               		     maxCheapTradeCoupon.setMaximum(2000000000);
		               		     maxCheapTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		     maxCheapTradeCoupon.setDigits(4);
		               		     maxCheapTradeCoupon.setBounds(55, 79, 100, 22);
		               		     formToolkit.adapt(maxCheapTradeCoupon);
		               		     formToolkit.paintBordersFor(maxCheapTradeCoupon);
		               		     
		               		     Label lblCheapTradeMaxCoupon = new Label(CheapTradeCoupon, SWT.NONE);
		               		     lblCheapTradeMaxCoupon.setText("Max");
		               		     lblCheapTradeMaxCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		     lblCheapTradeMaxCoupon.setBounds(1, 82, 36, 19);
		               		     formToolkit.adapt(lblCheapTradeMaxCoupon, true, true);
		               		     
		               		     Label lblCheapTradeMinCoupon = new Label(CheapTradeCoupon, SWT.NONE);
		               		     lblCheapTradeMinCoupon.setText("Min");
		               		     lblCheapTradeMinCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		     lblCheapTradeMinCoupon.setBounds(1, 41, 36, 24);
		               		     formToolkit.adapt(lblCheapTradeMinCoupon, true, true);
		               		     
		               		      minCheapTradeCoupon = new Spinner(CheapTradeCoupon, SWT.BORDER | SWT.WRAP);
		               		      minCheapTradeCoupon.setMaximum(2000000000);
		               		      minCheapTradeCoupon.setSelection(0);
		               		      minCheapTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		      minCheapTradeCoupon.setDigits(4);
		               		      minCheapTradeCoupon.setBounds(55, 38, 100, 22);
		               		      formToolkit.adapt(minCheapTradeCoupon);
		               		      formToolkit.paintBordersFor(minCheapTradeCoupon);
		               		      
		               		      Label lblCheapTradeCoupon = new Label(CheapTradeCoupon, SWT.NONE);
		               		      lblCheapTradeCoupon.setText("Coupon");
		               		      lblCheapTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		      lblCheapTradeCoupon.setAlignment(SWT.CENTER);
		               		      lblCheapTradeCoupon.setBounds(0, 0, 163, 23);
		               		      formToolkit.adapt(lblCheapTradeCoupon, true, true);
		               		      
		               		      Composite CheapTradeYield = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		      CheapTradeYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		      CheapTradeYield.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		      CheapTradeYield.setBounds(667, 26, 173, 114);
		               		      formToolkit.paintBordersFor(CheapTradeYield);
		               		      
		               		       maxCheapTradeYield = new Spinner(CheapTradeYield, SWT.BORDER);
		               		       maxCheapTradeYield.setMaximum(2000000000);
		               		       maxCheapTradeYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		       maxCheapTradeYield.setDigits(4);
		               		       maxCheapTradeYield.setBounds(47, 74, 116, 22);
		               		       formToolkit.adapt(maxCheapTradeYield);
		               		       formToolkit.paintBordersFor(maxCheapTradeYield);
		               		       
		               		       Label lblCheapTradeMinYield = new Label(CheapTradeYield, SWT.NONE);
		               		       lblCheapTradeMinYield.setText("Min");
		               		       lblCheapTradeMinYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		       lblCheapTradeMinYield.setBounds(0, 41, 33, 19);
		               		       formToolkit.adapt(lblCheapTradeMinYield, true, true);
		               		       
		               		        minCheapTradeYield = new Spinner(CheapTradeYield, SWT.BORDER | SWT.WRAP);
		               		        minCheapTradeYield.setMaximum(2000000000);
		               		        minCheapTradeYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		        minCheapTradeYield.setDigits(4);
		               		        minCheapTradeYield.setBounds(47, 38, 116, 22);
		               		        formToolkit.adapt(minCheapTradeYield);
		               		        formToolkit.paintBordersFor(minCheapTradeYield);
		               		        
		               		        Label lblCheapTradeYield = new Label(CheapTradeYield, SWT.NONE);
		               		        lblCheapTradeYield.setText("Yield");
		               		        lblCheapTradeYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		        lblCheapTradeYield.setAlignment(SWT.CENTER);
		               		        lblCheapTradeYield.setBounds(0, 0, 169, 23);
		               		        formToolkit.adapt(lblCheapTradeYield, true, true);
		               		        
		               		        Label lblMaxCheapTradeYield = new Label(CheapTradeYield, SWT.NONE);
		               		        lblMaxCheapTradeYield.setText("Max");
		               		        lblMaxCheapTradeYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		        lblMaxCheapTradeYield.setBounds(0, 77, 33, 19);
		               		        formToolkit.adapt(lblMaxCheapTradeYield, true, true);
		               		        
		               		        Composite CheapTradeParAmount = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		        CheapTradeParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		        CheapTradeParAmount.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		        CheapTradeParAmount.setBounds(846, 26, 226, 114);
		               		        formToolkit.paintBordersFor(CheapTradeParAmount);
		               		        
		               		         maxCheapTradeParAmount = new Spinner(CheapTradeParAmount, SWT.BORDER);
		               		         maxCheapTradeParAmount.setMaximum(2000000000);
		               		         maxCheapTradeParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		         maxCheapTradeParAmount.setBounds(56, 74, 156, 22);
		               		         formToolkit.adapt(maxCheapTradeParAmount);
		               		         formToolkit.paintBordersFor(maxCheapTradeParAmount);
		               		         
		               		          minCheapTradeParAmount = new Spinner(CheapTradeParAmount, SWT.BORDER);
		               		          minCheapTradeParAmount.setMaximum(2000000000);
		               		          minCheapTradeParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		          minCheapTradeParAmount.setBounds(56, 38, 156, 22);
		               		          formToolkit.adapt(minCheapTradeParAmount);
		               		          formToolkit.paintBordersFor(minCheapTradeParAmount);
		               		          
		               		          Label lblCheapTradeParAmount = new Label(CheapTradeParAmount, SWT.NONE);
		               		          lblCheapTradeParAmount.setText("Par Amount");
		               		          lblCheapTradeParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		          lblCheapTradeParAmount.setAlignment(SWT.CENTER);
		               		          lblCheapTradeParAmount.setBounds(0, 0, 222, 23);
		               		          formToolkit.adapt(lblCheapTradeParAmount, true, true);
		               		          
		               		          Label lblMinCheapTradeParAmount = new Label(CheapTradeParAmount, SWT.NONE);
		               		          lblMinCheapTradeParAmount.setText("Min");
		               		          lblMinCheapTradeParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		          lblMinCheapTradeParAmount.setBounds(0, 38, 34, 21);
		               		          formToolkit.adapt(lblMinCheapTradeParAmount, true, true);
		               		          
		               		          Label lblMaxCheapTradeParAmount = new Label(CheapTradeParAmount, SWT.NONE);
		               		          lblMaxCheapTradeParAmount.setText("Max");
		               		          lblMaxCheapTradeParAmount.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		          lblMaxCheapTradeParAmount.setBounds(0, 74, 34, 22);
		               		          formToolkit.adapt(lblMaxCheapTradeParAmount, true, true);
		               		          
		               		          Composite CheapTradePrice = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		          CheapTradePrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		          CheapTradePrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		          CheapTradePrice.setBounds(1078, 26, 181, 114);
		               		          formToolkit.paintBordersFor(CheapTradePrice);
		               		          
		               		           maxCheapTradePrice = new Spinner(CheapTradePrice, SWT.BORDER);
		               		           maxCheapTradePrice.setMaximum(2000000000);
		               		           maxCheapTradePrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		           maxCheapTradePrice.setDigits(4);
		               		           maxCheapTradePrice.setBounds(63, 77, 104, 22);
		               		           formToolkit.adapt(maxCheapTradePrice);
		               		           formToolkit.paintBordersFor(maxCheapTradePrice);
		               		           
		               		           Label lblMaxCheapTradePrice = new Label(CheapTradePrice, SWT.NONE);
		               		           lblMaxCheapTradePrice.setText("Max");
		               		           lblMaxCheapTradePrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		           lblMaxCheapTradePrice.setBounds(1, 82, 37, 18);
		               		           formToolkit.adapt(lblMaxCheapTradePrice, true, true);
		               		           
		               		           Label lblMinCheapTradePrice = new Label(CheapTradePrice, SWT.NONE);
		               		           lblMinCheapTradePrice.setText("Min");
		               		           lblMinCheapTradePrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		           lblMinCheapTradePrice.setBounds(1, 41, 37, 19);
		               		           formToolkit.adapt(lblMinCheapTradePrice, true, true);
		               		           
		               		            minCheapTradePrice = new Spinner(CheapTradePrice, SWT.BORDER | SWT.WRAP);
		               		            minCheapTradePrice.setMaximum(2000000000);
		               		            minCheapTradePrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		            minCheapTradePrice.setDigits(4);
		               		            minCheapTradePrice.setBounds(63, 41, 104, 22);
		               		            formToolkit.adapt(minCheapTradePrice);
		               		            formToolkit.paintBordersFor(minCheapTradePrice);
		               		            
		               		            Label lblCheapTradePrice = new Label(CheapTradePrice, SWT.NONE);
		               		            lblCheapTradePrice.setText("Price");
		               		            lblCheapTradePrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		            lblCheapTradePrice.setAlignment(SWT.CENTER);
		               		            lblCheapTradePrice.setBounds(0, 0, 177, 23);
		               		            formToolkit.adapt(lblCheapTradePrice, true, true);
		               		            
		               		            Composite CheapTradeBankQualified = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		            CheapTradeBankQualified.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		            CheapTradeBankQualified.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		            CheapTradeBankQualified.setBounds(1043, 341, 129, 125);
		               		            formToolkit.paintBordersFor(CheapTradeBankQualified);
		               		            
		               		            Label lblCheapTradeBankQualified = new Label(CheapTradeBankQualified, SWT.NONE);
		               		            lblCheapTradeBankQualified.setText("Bank Qualified");
		               		            lblCheapTradeBankQualified.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		            lblCheapTradeBankQualified.setAlignment(SWT.CENTER);
		               		            lblCheapTradeBankQualified.setBounds(0, 0, 125, 21);
		               		            formToolkit.adapt(lblCheapTradeBankQualified, true, true);
		               		            
		               		            btnCheapTradeYesBankQual = new Button(CheapTradeBankQualified, SWT.CHECK);
		               		            btnCheapTradeYesBankQual.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		            btnCheapTradeYesBankQual.setText("Yes");
		               		            btnCheapTradeYesBankQual.setAlignment(SWT.CENTER);
		               		            btnCheapTradeYesBankQual.setBounds(5, 42, 106, 18);
		               		            formToolkit.adapt(btnCheapTradeYesBankQual, true, true);
		               		            
		               		            btnCheapTradeNoBankQual = new Button(CheapTradeBankQualified, SWT.CHECK);
		               		            btnCheapTradeNoBankQual.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		            btnCheapTradeNoBankQual.setText("No");
		               		            btnCheapTradeNoBankQual.setAlignment(SWT.CENTER);
		               		            btnCheapTradeNoBankQual.setBounds(5, 72, 106, 18);
		               		            formToolkit.adapt(btnCheapTradeNoBankQual, true, true);
		               		            
		               		            Composite CheapTradeTaxExempt = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		            CheapTradeTaxExempt.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		            CheapTradeTaxExempt.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		            CheapTradeTaxExempt.setBounds(1265, 26, 159, 114);
		               		            formToolkit.paintBordersFor(CheapTradeTaxExempt);
		               		            
		               		            Label lblCheapTradeTaxExempt = new Label(CheapTradeTaxExempt, SWT.NONE);
		               		            lblCheapTradeTaxExempt.setText("Tax Exempt");
		               		            lblCheapTradeTaxExempt.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		            lblCheapTradeTaxExempt.setAlignment(SWT.CENTER);
		               		            lblCheapTradeTaxExempt.setBounds(0, 0, 155, 23);
		               		            formToolkit.adapt(lblCheapTradeTaxExempt, true, true);
		               		            
		               		             btnCheapTradeTaxExempt = new Button(CheapTradeTaxExempt, SWT.CHECK);
		               		             btnCheapTradeTaxExempt.setText("Tax Exempt");
		               		             btnCheapTradeTaxExempt.setAlignment(SWT.CENTER);
		               		             btnCheapTradeTaxExempt.setBounds(15, 34, 130, 18);
		               		             formToolkit.adapt(btnCheapTradeTaxExempt, true, true);
		               		             
		               		              btnCheapTradeNotTaxExempt = new Button(CheapTradeTaxExempt, SWT.CHECK);
		               		              btnCheapTradeNotTaxExempt.setText("Not Tax Exempt");
		               		              btnCheapTradeNotTaxExempt.setAlignment(SWT.CENTER);
		               		              btnCheapTradeNotTaxExempt.setBounds(15, 58, 130, 18);
		               		              formToolkit.adapt(btnCheapTradeNotTaxExempt, true, true);
		               		              
		               		               btnCheapTradeAMT = new Button(CheapTradeTaxExempt, SWT.CHECK);
		               		               btnCheapTradeAMT.setText("AMT");
		               		               btnCheapTradeAMT.setAlignment(SWT.CENTER);
		               		               btnCheapTradeAMT.setBounds(15, 82, 130, 18);
		               		               formToolkit.adapt(btnCheapTradeAMT, true, true);
		               		               
		               		               Composite CheapTradeCouponCode = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               CheapTradeCouponCode.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeCouponCode.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeCouponCode.setBounds(94, 162, 202, 163);
		               		               formToolkit.paintBordersFor(CheapTradeCouponCode);
		               		               
		               		               Label lblCheapTradeCouponCode = new Label(CheapTradeCouponCode, SWT.BORDER);
		               		               lblCheapTradeCouponCode.setText("Coupon Code");
		               		               lblCheapTradeCouponCode.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeCouponCode.setAlignment(SWT.CENTER);
		               		               lblCheapTradeCouponCode.setBounds(0, 0, 198, 27);
		               		               formToolkit.adapt(lblCheapTradeCouponCode, true, true);
		               		               
		               		               ListViewer CheapTradeCouponCodeViewer = new ListViewer(CheapTradeCouponCode, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		               		               CheapTradeCouponCodeList = CheapTradeCouponCodeViewer.getList();
		               		               CheapTradeCouponCodeList.setItems(couponCodeString);
		               		               CheapTradeCouponCodeList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               CheapTradeCouponCodeList.setBounds(10, 33, 178, 116);
		               		               
		               		               Composite CheapTradeDebtType = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               CheapTradeDebtType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeDebtType.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeDebtType.setBounds(302, 162, 202, 163);
		               		               formToolkit.paintBordersFor(CheapTradeDebtType);
		               		               
		               		               Label lblCheapTradeDebtType = new Label(CheapTradeDebtType, SWT.BORDER);
		               		               lblCheapTradeDebtType.setText("Debt Type");
		               		               lblCheapTradeDebtType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeDebtType.setAlignment(SWT.CENTER);
		               		               lblCheapTradeDebtType.setBounds(0, 0, 198, 24);
		               		               formToolkit.adapt(lblCheapTradeDebtType, true, true);
		               		               
		               		               ListViewer CheapTradeDebtTypeViewer = new ListViewer(CheapTradeDebtType, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL| SWT.MULTI);
		               		               CheapTradeDebtTypeList = CheapTradeDebtTypeViewer.getList();
		               		               CheapTradeDebtTypeList.setItems(debtTypeString);
		               		               CheapTradeDebtTypeList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               CheapTradeDebtTypeList.setBounds(10, 30, 178, 119);
		               		               
		               		               Composite CheapTradeStateSelection = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               CheapTradeStateSelection.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeStateSelection.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeStateSelection.setBounds(510, 162, 180, 163);
		               		               formToolkit.paintBordersFor(CheapTradeStateSelection);
		               		               
		               		               Label lblCheapTradeState = new Label(CheapTradeStateSelection, SWT.BORDER);
		               		               lblCheapTradeState.setText("State");
		               		               lblCheapTradeState.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeState.setAlignment(SWT.CENTER);
		               		               lblCheapTradeState.setBounds(0, 0, 176, 27);
		               		               formToolkit.adapt(lblCheapTradeState, true, true);
		               		               
		               		               ListViewer CheapTradeStateViewer = new ListViewer(CheapTradeStateSelection, SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL | SWT.MULTI);
		               		               CheapTradeStateList = CheapTradeStateViewer.getList();
		               		               CheapTradeStateList.setItems(stateNameString);
		               		               CheapTradeStateList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               CheapTradeStateList.setBounds(10, 34, 156, 115);
		               		               
		               		               Composite CheapTradeUseProceeds = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               CheapTradeUseProceeds.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeUseProceeds.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeUseProceeds.setBounds(696, 162, 210, 163);
		               		               formToolkit.paintBordersFor(CheapTradeUseProceeds);
		               		               
		               		               Label lblCheapTradeUseProceeds = new Label(CheapTradeUseProceeds, SWT.BORDER | SWT.CENTER);
		               		               lblCheapTradeUseProceeds.setText("Use of Proceeds");
		               		               lblCheapTradeUseProceeds.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeUseProceeds.setBounds(0, 0, 206, 27);
		               		               formToolkit.adapt(lblCheapTradeUseProceeds, true, true);
		               		               
		               		               ListViewer CheapTradeUseProceedsViewer = new ListViewer(CheapTradeUseProceeds, SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL | SWT.MULTI);
		               		               CheapTradeUseProceedsList = CheapTradeUseProceedsViewer.getList();
		               		               CheapTradeUseProceedsList.setItems(useProceedsString);
		               		               CheapTradeUseProceedsList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               CheapTradeUseProceedsList.setBounds(10, 33, 186, 116);
		               		               
		               		               Composite CheapTradeInsurance = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               CheapTradeInsurance.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeInsurance.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeInsurance.setBounds(912, 162, 231, 163);
		               		               formToolkit.paintBordersFor(CheapTradeInsurance);
		               		               
		               		               Label lblCheapTradeInsurance = new Label(CheapTradeInsurance, SWT.BORDER);
		               		               lblCheapTradeInsurance.setText("Insurance");
		               		               lblCheapTradeInsurance.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeInsurance.setAlignment(SWT.CENTER);
		               		               lblCheapTradeInsurance.setBounds(0, 0, 227, 27);
		               		               formToolkit.adapt(lblCheapTradeInsurance, true, true);
		               		               
		               		               ListViewer CheapTradeInsuranceViewer = new ListViewer(CheapTradeInsurance, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		               		               CheapTradeInsuranceList = CheapTradeInsuranceViewer.getList();
		               		               CheapTradeInsuranceList.setItems(bondInsuranceNameString);
		               		               CheapTradeInsuranceList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               CheapTradeInsuranceList.setBounds(10, 34, 207, 115);
		               		               
	
		               		               Composite SPRating = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               SPRating.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               SPRating.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               SPRating.setBounds(1295, 162, 129, 163);
		               		               formToolkit.paintBordersFor(SPRating);
		               		               
		               		               Label lblSPRating = new Label(SPRating, SWT.BORDER | SWT.CENTER);
		               		               lblSPRating.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblSPRating.setText("SP Rating");
		               		               lblSPRating.setBounds(0, 0, 125, 24);
		               		               formToolkit.adapt(lblSPRating, true, true);
		               		               
		               		               ListViewer spRatingViewer = new ListViewer(SPRating, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL| SWT.MULTI);
		               		               spRatingList = spRatingViewer.getList();
		               		               spRatingList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               spRatingList.setItems(spRatingString);
		               		               spRatingList.setBounds(10, 41, 105, 108);
		               		               
		               		               Composite MoodyRatings = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               MoodyRatings.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               MoodyRatings.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               MoodyRatings.setBounds(1149, 162, 140, 163);
		               		               formToolkit.paintBordersFor(MoodyRatings);
		               		               
		               		               Label lblMoodyRatings = new Label(MoodyRatings, SWT.BORDER);
		               		               lblMoodyRatings.setText("Moodys Ratings");
		               		               lblMoodyRatings.setBounds(0, 0, 136, 24);
		               		               lblMoodyRatings.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               formToolkit.adapt(lblMoodyRatings, true, true);
		               		               
		               		               ListViewer moodyRatingViewer = new ListViewer(MoodyRatings, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL| SWT.MULTI);
		               		               moodyRatingList = moodyRatingViewer.getList();
		               		               moodyRatingList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               moodyRatingList.setItems(moodyRatingString);
		               		               moodyRatingList.setBounds(10, 40, 116, 109);
		               		               
		               		               
		               		               
		               		               
		               		               Composite NextCheapTradeCallDate = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               NextCheapTradeCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               NextCheapTradeCallDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               NextCheapTradeCallDate.setBounds(688, 341, 173, 125);
		               		               formToolkit.paintBordersFor(NextCheapTradeCallDate);
		               		               
		               		               Label lblNextCheapTradeCallDate = new Label(NextCheapTradeCallDate, SWT.BORDER | SWT.CENTER);
		               		               lblNextCheapTradeCallDate.setText("Next Call Date");
		               		               lblNextCheapTradeCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblNextCheapTradeCallDate.setBounds(0, 0, 169, 21);
		               		               formToolkit.adapt(lblNextCheapTradeCallDate, true, true);
		               		               
		               		               Label lblNextMinCheapTradeCallDate = formToolkit.createLabel(NextCheapTradeCallDate, "Min", SWT.BORDER);
		               		               lblNextMinCheapTradeCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblNextMinCheapTradeCallDate.setBounds(0, 44, 38, 21);
		               		               
		               		               Label lblNextMaxCheapTradeCallDate = formToolkit.createLabel(NextCheapTradeCallDate, "Max", SWT.BORDER);
		               		               lblNextMaxCheapTradeCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblNextMaxCheapTradeCallDate.setBounds(0, 85, 38, 26);
		               		               
		               		               minCheapTradeNextCallDate = new DateChooserCombo(NextCheapTradeCallDate, SWT.NONE);
		               		               minCheapTradeNextCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               minCheapTradeNextCallDate.setBounds(62, 44, 97, 21);
		               		               formToolkit.adapt(minCheapTradeNextCallDate);
		               		               formToolkit.paintBordersFor(minCheapTradeNextCallDate);
		               		               
		               		               maxCheapTradeNextCallDate = new DateChooserCombo(NextCheapTradeCallDate, SWT.NONE);
		               		               maxCheapTradeNextCallDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               maxCheapTradeNextCallDate.setBounds(61, 84, 98, 22);
		               		               formToolkit.adapt(maxCheapTradeNextCallDate);
		               		               formToolkit.paintBordersFor(maxCheapTradeNextCallDate);
		               		               
		               		               Composite NextCheapTradeCallPrice = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               NextCheapTradeCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               NextCheapTradeCallPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               NextCheapTradeCallPrice.setBounds(870, 341, 167, 125);
		               		               formToolkit.paintBordersFor(NextCheapTradeCallPrice);
		               		               
		               		               maxCheapTradeNextCallPrice = new Spinner(NextCheapTradeCallPrice, SWT.BORDER);
		               		               maxCheapTradeNextCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               maxCheapTradeNextCallPrice.setMaximum(2000000000);
		               		               maxCheapTradeNextCallPrice.setDigits(4);
		               		               maxCheapTradeNextCallPrice.setBounds(56, 78, 97, 22);
		               		               formToolkit.adapt(maxCheapTradeNextCallPrice);
		               		               formToolkit.paintBordersFor(maxCheapTradeNextCallPrice);
		               		               
		               		               Label lblNextMaxCheapTradeCallPrice = new Label(NextCheapTradeCallPrice, SWT.NONE);
		               		               lblNextMaxCheapTradeCallPrice.setText("Max");
		               		               lblNextMaxCheapTradeCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblNextMaxCheapTradeCallPrice.setBounds(1, 78, 36, 22);
		               		               formToolkit.adapt(lblNextMaxCheapTradeCallPrice, true, true);
		               		               
		               		               Label lblNextMinCallPrice_1 = new Label(NextCheapTradeCallPrice, SWT.NONE);
		               		               lblNextMinCallPrice_1.setText("Min");
		               		               lblNextMinCallPrice_1.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblNextMinCallPrice_1.setBounds(1, 37, 36, 22);
		               		               formToolkit.adapt(lblNextMinCallPrice_1, true, true);
		               		               
		               		               minCheapTradeNextCallPrice = new Spinner(NextCheapTradeCallPrice, SWT.BORDER | SWT.WRAP);
		               		               minCheapTradeNextCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               minCheapTradeNextCallPrice.setMaximum(2000000000);
		               		               minCheapTradeNextCallPrice.setDigits(4);
		               		               minCheapTradeNextCallPrice.setBounds(56, 37, 97, 22);
		               		               formToolkit.adapt(minCheapTradeNextCallPrice);
		               		               formToolkit.paintBordersFor(minCheapTradeNextCallPrice);
		               		               
		               		               Label lblNextCheapTradeCallPrice = new Label(NextCheapTradeCallPrice, SWT.CENTER);
		               		               lblNextCheapTradeCallPrice.setText("Next Call Price");
		               		               lblNextCheapTradeCallPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblNextCheapTradeCallPrice.setBounds(0, 0, 163, 21);
		               		               formToolkit.adapt(lblNextCheapTradeCallPrice, true, true);
		               		               
		               		               Composite CheapTradeUpdatedDate = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               CheapTradeUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeUpdatedDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeUpdatedDate.setBounds(1178, 341, 167, 125);
		               		               formToolkit.paintBordersFor(CheapTradeUpdatedDate);
		               		               
		               		               Label lblCheapTradeUpdateDate = new Label(CheapTradeUpdatedDate, SWT.BORDER);
		               		               lblCheapTradeUpdateDate.setText("Updated Date");
		               		               lblCheapTradeUpdateDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeUpdateDate.setAlignment(SWT.CENTER);
		               		               lblCheapTradeUpdateDate.setBounds(0, 0, 165, 21);
		               		               formToolkit.adapt(lblCheapTradeUpdateDate, true, true);
		               		               
		               		               Label lblMinCheapTradeUpdatedDate = formToolkit.createLabel(CheapTradeUpdatedDate, "Min", SWT.BORDER);
		               		               lblMinCheapTradeUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMinCheapTradeUpdatedDate.setBounds(0, 40, 38, 21);
		               		               
		               		               Label lblMaxCheapTradeUpdatedDate = formToolkit.createLabel(CheapTradeUpdatedDate, "Max", SWT.BORDER);
		               		               lblMaxCheapTradeUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMaxCheapTradeUpdatedDate.setBounds(0, 88, 38, 20);
		               		               
		               		               maxCheapTradeUpdatedDate = new DateChooserCombo(CheapTradeUpdatedDate, SWT.NONE);
		               		               maxCheapTradeUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               maxCheapTradeUpdatedDate.setBounds(44, 85, 105, 23);
		               		               formToolkit.adapt(maxCheapTradeUpdatedDate);
		               		               formToolkit.paintBordersFor(maxCheapTradeUpdatedDate);
		               		               
		               		               minCheapTradeUpdatedDate = new DateChooserCombo(CheapTradeUpdatedDate, SWT.NONE);
		               		               minCheapTradeUpdatedDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               minCheapTradeUpdatedDate.setBounds(44, 40, 105, 23);
		               		               formToolkit.adapt(minCheapTradeUpdatedDate);
		               		               formToolkit.paintBordersFor(minCheapTradeUpdatedDate);
		               		               
		               		               
		               		               Composite CheapTradeAdditionalCredit = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               CheapTradeAdditionalCredit.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeAdditionalCredit.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeAdditionalCredit.setBounds(456, 331, 226, 147);
		               		               formToolkit.paintBordersFor(CheapTradeAdditionalCredit);
		               		               
		               		               Label lblCheapTradeAdditionalCredit = new Label(CheapTradeAdditionalCredit, SWT.BORDER | SWT.CENTER);
		               		               lblCheapTradeAdditionalCredit.setText("Additional Credit");
		               		               lblCheapTradeAdditionalCredit.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeAdditionalCredit.setBounds(0, 0, 222, 23);
		               		               formToolkit.adapt(lblCheapTradeAdditionalCredit, true, true);
		               		               
		               		               ListViewer CheapTradeAddlCreditViewer = new ListViewer(CheapTradeAdditionalCredit, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		               		               CheapTradeAddlCreditList = CheapTradeAddlCreditViewer.getList();
		               		               CheapTradeAddlCreditList.setItems(addlCreditString);
		               		               CheapTradeAddlCreditList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               CheapTradeAddlCreditList.setBounds(10, 29, 202, 104);
		               		               
		               		               Composite cheapTradeCapitalPurposeSelection = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               cheapTradeCapitalPurposeSelection.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               cheapTradeCapitalPurposeSelection.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               cheapTradeCapitalPurposeSelection.setBounds(219, 331, 231, 147);
		               		               formToolkit.paintBordersFor(cheapTradeCapitalPurposeSelection);
		               		               
		               		               Label lblCheapTradeCapitalPurpose = new Label(cheapTradeCapitalPurposeSelection, SWT.BORDER);
		               		               lblCheapTradeCapitalPurpose.setText("Capital Purpose");
		               		               lblCheapTradeCapitalPurpose.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeCapitalPurpose.setAlignment(SWT.CENTER);
		               		               lblCheapTradeCapitalPurpose.setBounds(0, 0, 227, 21);
		               		               formToolkit.adapt(lblCheapTradeCapitalPurpose, true, true);
		               		               
		               		               ListViewer cheapTradeCapitalPurposeViewer = new ListViewer(cheapTradeCapitalPurposeSelection, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		               		               CheapTradeCapitalPurposeList = cheapTradeCapitalPurposeViewer.getList();
		               		               CheapTradeCapitalPurposeList.setItems(capitalPurposeString);
		               		               CheapTradeCapitalPurposeList.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 9, SWT.NORMAL));
		               		               CheapTradeCapitalPurposeList.setBounds(10, 27, 207, 106);
		               		               
		               		               btnCheapTradeActiveMaturities = new SwitchButton(CheapTradeComposite, SWT.BORDER);
		               		               btnCheapTradeActiveMaturities.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               btnCheapTradeActiveMaturities.setBounds(712, 745, 267, 37);
		               		               btnCheapTradeActiveMaturities.setText("Active Maturities Only");
		               		               btnCheapTradeActiveMaturities.setSelection(true);
		               		               formToolkit.adapt(btnCheapTradeActiveMaturities);
		               		               formToolkit.paintBordersFor(btnCheapTradeActiveMaturities);
		               		               
		               		               Composite CheapTradeTradingInfoConfig = formToolkit.createComposite(CheapTradeComposite, SWT.BORDER);
		               		               CheapTradeTradingInfoConfig.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeTradingInfoConfig.setBackground(SWTResourceManager.getColor(186, 86, 36));
		               		               CheapTradeTradingInfoConfig.setBounds(94, 492, 1336, 232);
		               		               formToolkit.paintBordersFor(CheapTradeTradingInfoConfig);
		               		               
		               		               Composite CheapTradeTradeDate = formToolkit.createComposite(CheapTradeTradingInfoConfig, SWT.BORDER);
		               		               CheapTradeTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeTradeDate.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeTradeDate.setBounds(10, 49, 199, 126);
		               		               formToolkit.paintBordersFor(CheapTradeTradeDate);
		               		               
		               		               Label lblCheapTradeTradeDate = new Label(CheapTradeTradeDate, SWT.BORDER);
		               		               lblCheapTradeTradeDate.setText("Trade Date");
		               		               lblCheapTradeTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeTradeDate.setAlignment(SWT.CENTER);
		               		               lblCheapTradeTradeDate.setBounds(0, 0, 195, 25);
		               		               formToolkit.adapt(lblCheapTradeTradeDate, true, true);
		               		               
		               		               Label lblMinCheapTradeTradeDate = formToolkit.createLabel(CheapTradeTradeDate, "Min", SWT.BORDER);
		               		               lblMinCheapTradeTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMinCheapTradeTradeDate.setBounds(0, 44, 39, 25);
		               		               
		               		               Label lblMaxCheapTradeTradeDate = formToolkit.createLabel(CheapTradeTradeDate, "Max", SWT.BORDER);
		               		               lblMaxCheapTradeTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMaxCheapTradeTradeDate.setBounds(0, 86, 39, 25);
		               		               
		               		               minCheapTradeTradeDate = new DateChooserCombo(CheapTradeTradeDate, SWT.NONE);
		               		               minCheapTradeTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               minCheapTradeTradeDate.setBounds(60, 44, 125, 25);
		               		               formToolkit.adapt(minCheapTradeTradeDate);
		               		               formToolkit.paintBordersFor(minCheapTradeTradeDate);
		               		               
		               		               maxCheapTradeTradeDate = new DateChooserCombo(CheapTradeTradeDate, SWT.NONE);
		               		               maxCheapTradeTradeDate.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               maxCheapTradeTradeDate.setBounds(60, 86, 125, 25);
		               		               formToolkit.adapt(maxCheapTradeTradeDate);
		               		               formToolkit.paintBordersFor(maxCheapTradeTradeDate);
		               		               
		               		               Composite CheapTradeParAmountTraded = formToolkit.createComposite(CheapTradeTradingInfoConfig, SWT.BORDER);
		               		               CheapTradeParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeParAmountTraded.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeParAmountTraded.setBounds(215, 49, 224, 126);
		               		               formToolkit.paintBordersFor(CheapTradeParAmountTraded);
		               		               
		               		               maxCheapTradeParAmountTraded = new Spinner(CheapTradeParAmountTraded, SWT.BORDER);
		               		               maxCheapTradeParAmountTraded.setMaximum(2000000000);
		               		               maxCheapTradeParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               maxCheapTradeParAmountTraded.setBounds(59, 84, 151, 22);
		               		               formToolkit.adapt(maxCheapTradeParAmountTraded);
		               		               formToolkit.paintBordersFor(maxCheapTradeParAmountTraded);
		               		               
		               		               minCheapTradeParAmountTraded = new Spinner(CheapTradeParAmountTraded, SWT.BORDER);
		               		               minCheapTradeParAmountTraded.setLocation(59, 44);
		               		               minCheapTradeParAmountTraded.setSize(151, 22);
		               		               minCheapTradeParAmountTraded.setMaximum(2000000000);
		               		               minCheapTradeParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               formToolkit.adapt(minCheapTradeParAmountTraded);
		               		               formToolkit.paintBordersFor(minCheapTradeParAmountTraded);
		               		               
		               		               Label lblCheapTradeParAmountTraded = new Label(CheapTradeParAmountTraded, SWT.NONE);
		               		               lblCheapTradeParAmountTraded.setText("Par Amount Traded");
		               		               lblCheapTradeParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeParAmountTraded.setAlignment(SWT.CENTER);
		               		               lblCheapTradeParAmountTraded.setBounds(2, 0, 218, 24);
		               		               formToolkit.adapt(lblCheapTradeParAmountTraded, true, true);
		               		               
		               		               Label lblMinCheapTradeParAmountTraded = new Label(CheapTradeParAmountTraded, SWT.NONE);
		               		               lblMinCheapTradeParAmountTraded.setText("Min");
		               		               lblMinCheapTradeParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMinCheapTradeParAmountTraded.setBounds(2, 44, 33, 22);
		               		               formToolkit.adapt(lblMinCheapTradeParAmountTraded, true, true);
		               		               
		               		               Label lblMaxCheapTradeParAmountTraded = new Label(CheapTradeParAmountTraded, SWT.NONE);
		               		               lblMaxCheapTradeParAmountTraded.setText("Max");
		               		               lblMaxCheapTradeParAmountTraded.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMaxCheapTradeParAmountTraded.setBounds(2, 86, 33, 26);
		               		               formToolkit.adapt(lblMaxCheapTradeParAmountTraded, true, true);
		               		               
		               		               Label lblCheapTradeTradingConfigurations = new Label(CheapTradeTradingInfoConfig, SWT.NONE);
		               		               lblCheapTradeTradingConfigurations.setText("Trading Configurations");
		               		               lblCheapTradeTradingConfigurations.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeTradingConfigurations.setAlignment(SWT.CENTER);
		               		               lblCheapTradeTradingConfigurations.setBounds(468, 0, 239, 22);
		               		               formToolkit.adapt(lblCheapTradeTradingConfigurations, true, true);
		               		               
		               		               btnCheapTradeCalculateTradingYield = new Button(CheapTradeTradingInfoConfig, SWT.BORDER | SWT.CHECK);
		               		               btnCheapTradeCalculateTradingYield.setText("Calculate Trading Yield");
		               		               btnCheapTradeCalculateTradingYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               btnCheapTradeCalculateTradingYield.setBounds(487, 181, 239, 36);
		               		               formToolkit.adapt(btnCheapTradeCalculateTradingYield, true, true);
		               		               
		               		               Composite CheapTradeParDollarPrice = formToolkit.createComposite(CheapTradeTradingInfoConfig, SWT.BORDER);
		               		               CheapTradeParDollarPrice.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		               		               CheapTradeParDollarPrice.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeParDollarPrice.setBounds(445, 49, 171, 126);
		               		               formToolkit.paintBordersFor(CheapTradeParDollarPrice);
		               		               
		               		               maxCheapTradeParDollarPrice = new Spinner(CheapTradeParDollarPrice, SWT.BORDER);
		               		               maxCheapTradeParDollarPrice.setMaximum(2000000000);
		               		               maxCheapTradeParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               maxCheapTradeParDollarPrice.setDigits(4);
		               		               maxCheapTradeParDollarPrice.setBounds(47, 87, 110, 22);
		               		               formToolkit.adapt(maxCheapTradeParDollarPrice);
		               		               formToolkit.paintBordersFor(maxCheapTradeParDollarPrice);
		               		               
		               		               Label lblMaxCheapTradeParDollarPrice = new Label(CheapTradeParDollarPrice, SWT.NONE);
		               		               lblMaxCheapTradeParDollarPrice.setText("Max");
		               		               lblMaxCheapTradeParDollarPrice.setForeground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		               		               lblMaxCheapTradeParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMaxCheapTradeParDollarPrice.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		               		               lblMaxCheapTradeParDollarPrice.setBounds(1, 86, 40, 23);
		               		               formToolkit.adapt(lblMaxCheapTradeParDollarPrice, true, true);
		               		               
		               		               Label lblMinCheapTradeParDollarPrice = new Label(CheapTradeParDollarPrice, SWT.NONE);
		               		               lblMinCheapTradeParDollarPrice.setText("Min");
		               		               lblMinCheapTradeParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMinCheapTradeParDollarPrice.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		               		               lblMinCheapTradeParDollarPrice.setBounds(1, 46, 40, 22);
		               		               formToolkit.adapt(lblMinCheapTradeParDollarPrice, true, true);
		               		               
		               		               minCheapTradeParDollarPrice = new Spinner(CheapTradeParDollarPrice, SWT.BORDER | SWT.WRAP);
		               		               minCheapTradeParDollarPrice.setPageIncrement(2000000000);
		               		               minCheapTradeParDollarPrice.setMaximum(999999999);
		               		               minCheapTradeParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               minCheapTradeParDollarPrice.setDigits(4);
		               		               minCheapTradeParDollarPrice.setBounds(47, 46, 110, 22);
		               		               formToolkit.adapt(minCheapTradeParDollarPrice);
		               		               formToolkit.paintBordersFor(minCheapTradeParDollarPrice);
		               		               
		               		               Label lblCheapTradeParDollarPrice = new Label(CheapTradeParDollarPrice, SWT.NONE);
		               		               lblCheapTradeParDollarPrice.setText("Par Dollar Price");
		               		               lblCheapTradeParDollarPrice.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeParDollarPrice.setAlignment(SWT.CENTER);
		               		               lblCheapTradeParDollarPrice.setBounds(0, 0, 167, 22);
		               		               formToolkit.adapt(lblCheapTradeParDollarPrice, true, true);
		               		               
		               		               Composite CheapTradeParTradedYield = formToolkit.createComposite(CheapTradeTradingInfoConfig, SWT.BORDER);
		               		               CheapTradeParTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               CheapTradeParTradedYield.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               CheapTradeParTradedYield.setBounds(803, 49, 174, 126);
		               		               formToolkit.paintBordersFor(CheapTradeParTradedYield);
		               		               
		               		               maxCheapTradeTradedYield = new Spinner(CheapTradeParTradedYield, SWT.BORDER);
		               		               maxCheapTradeTradedYield.setMaximum(2000000000);
		               		               maxCheapTradeTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               maxCheapTradeTradedYield.setDigits(4);
		               		               maxCheapTradeTradedYield.setBounds(47, 90, 113, 22);
		               		               formToolkit.adapt(maxCheapTradeTradedYield);
		               		               formToolkit.paintBordersFor(maxCheapTradeTradedYield);
		               		               
		               		               Label lblMinCheapTradeTradedYield = new Label(CheapTradeParTradedYield, SWT.NONE);
		               		               lblMinCheapTradeTradedYield.setText("Min");
		               		               lblMinCheapTradeTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMinCheapTradeTradedYield.setBounds(0, 46, 41, 25);
		               		               formToolkit.adapt(lblMinCheapTradeTradedYield, true, true);
		               		               
		               		               minCheapTradeTradedYield = new Spinner(CheapTradeParTradedYield, SWT.BORDER | SWT.WRAP);
		               		               minCheapTradeTradedYield.setMaximum(2000000000);
		               		               minCheapTradeTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               minCheapTradeTradedYield.setDigits(4);
		               		               minCheapTradeTradedYield.setBounds(47, 49, 113, 22);
		               		               formToolkit.adapt(minCheapTradeTradedYield);
		               		               formToolkit.paintBordersFor(minCheapTradeTradedYield);
		               		               
		               		               Label lblCheapTradeParTradedYield = new Label(CheapTradeParTradedYield, SWT.NONE);
		               		               lblCheapTradeParTradedYield.setText("Par Traded Yield");
		               		               lblCheapTradeParTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblCheapTradeParTradedYield.setAlignment(SWT.CENTER);
		               		               lblCheapTradeParTradedYield.setBounds(0, 0, 170, 22);
		               		               formToolkit.adapt(lblCheapTradeParTradedYield, true, true);
		               		               
		               		               Label lblMaxCheapTradeTradedYield = new Label(CheapTradeParTradedYield, SWT.NONE);
		               		               lblMaxCheapTradeTradedYield.setText("Max");
		               		               lblMaxCheapTradeTradedYield.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               lblMaxCheapTradeTradedYield.setBounds(0, 87, 41, 25);
		               		               formToolkit.adapt(lblMaxCheapTradeTradedYield, true, true);
		               		               
		               		               btnCheapTradeUploadMmd = formToolkit.createButton(CheapTradeTradingInfoConfig, "Upload MMD", SWT.BORDER);
		               		               btnCheapTradeUploadMmd.setData(cusipDef);
		               		               
		               		               		btnCheapTradeUploadMmd.addSelectionListener(new SelectionAdapter() {
		               		               			@Override
		               		               			public void widgetSelected(SelectionEvent e) {
		               		               				  FileDialog fileDialog = new FileDialog(bondInfoTradingInfoConfig.getShell());
		               		               				    /*fileDialog.setFilterNames(new String[] { "Microsoft Excel Macro (*.xlsm)",
		               		               				            "Microsoft Excel Spreadsheet Files (*.xls)", "Microsoft Open XML Spreadsheet (*.xlsx)", "Comma Separated Values Files (*.csv)",
		               		               				            "All Files (*.*)" });
		               		               					*/
		               		               				    fileDialog.setFilterExtensions(new String[] { "*.xlsm", "*.xls", "*.xlsx", "*.csv", "*.*" });
		               		               				    String filename = fileDialog.open();
		               		               				    btnCheapTradeUploadMmd.setData(filename);
		               		               				    System.out.println(filename);
		               		               				    
		               		               			}
		               		               		});
		               		               		btnCheapTradeUploadMmd.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		btnCheapTradeUploadMmd.setBounds(340, 180, 123, 38);
		               		               		
		               		               		Composite CheapTradeTradeType = formToolkit.createComposite(CheapTradeTradingInfoConfig, SWT.BORDER);
		               		               		CheapTradeTradeType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		CheapTradeTradeType.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               		CheapTradeTradeType.setBounds(983, 49, 123, 126);
		               		               		formToolkit.paintBordersFor(CheapTradeTradeType);
		               		               		
		               		               		Label lblCheapTradeTradeType = new Label(CheapTradeTradeType, SWT.NONE);
		               		               		lblCheapTradeTradeType.setText("Trade Type");
		               		               		lblCheapTradeTradeType.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		lblCheapTradeTradeType.setAlignment(SWT.CENTER);
		               		               		lblCheapTradeTradeType.setBounds(0, 0, 121, 24);
		               		               		formToolkit.adapt(lblCheapTradeTradeType, true, true);
		               		               		
		               		               		btnCheapTradeTradeTypeD = new Button(CheapTradeTradeType, SWT.CHECK);
		               		               		btnCheapTradeTradeTypeD.setText("D");
		               		               		btnCheapTradeTradeTypeD.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		               		               		btnCheapTradeTradeTypeD.setBounds(6, 36, 105, 18);
		               		               		formToolkit.adapt(btnCheapTradeTradeTypeD, true, true);
		               		               		
		               		               		btnCheapTradeTradeTypeS = new Button(CheapTradeTradeType, SWT.CHECK);
		               		               		btnCheapTradeTradeTypeS.setText("S");
		               		               		btnCheapTradeTradeTypeS.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		               		               		btnCheapTradeTradeTypeS.setBounds(6, 60, 105, 18);
		               		               		formToolkit.adapt(btnCheapTradeTradeTypeS, true, true);
		               		               		
		               		               		btnCheapTradeTradeTypeP = new Button(CheapTradeTradeType, SWT.CHECK);
		               		               		btnCheapTradeTradeTypeP.setText("P");
		               		               		btnCheapTradeTradeTypeP.setFont(SWTResourceManager.getFont("Times New Roman", 13, SWT.NORMAL));
		               		               		btnCheapTradeTradeTypeP.setBounds(6, 84, 105, 18);
		               		               		formToolkit.adapt(btnCheapTradeTradeTypeP, true, true);
		               		               		
		               		               		Composite MMDSpread = formToolkit.createComposite(CheapTradeTradingInfoConfig, SWT.BORDER);
		               		               		MMDSpread.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		MMDSpread.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               		MMDSpread.setBounds(1118, 49, 188, 128);
		               		               		formToolkit.paintBordersFor(MMDSpread);
		               		               		
		               		               		maxMMDSpread = new Spinner(MMDSpread, SWT.BORDER);
		               		               		maxMMDSpread.setMaximum(2000000000);
		               		               		maxMMDSpread.setMinimum(-1000000000);
		               		               		maxMMDSpread.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		maxMMDSpread.setDigits(4);
		               		               		maxMMDSpread.setBounds(59, 81, 115, 22);
		               		               		formToolkit.adapt(maxMMDSpread);
		               		               		formToolkit.paintBordersFor(maxMMDSpread);
		               		               		
		               		               		Label lblMinMMDSpread = new Label(MMDSpread, SWT.NONE);
		               		               		lblMinMMDSpread.setText("Min");
		               		               		lblMinMMDSpread.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		lblMinMMDSpread.setBounds(0, 40, 33, 22);
		               		               		formToolkit.adapt(lblMinMMDSpread, true, true);
		               		               		
		               		               		minMMDSpread = new Spinner(MMDSpread, SWT.BORDER | SWT.WRAP);
		               		               		minMMDSpread.setMaximum(2000000000);
		               		               		minMMDSpread.setMinimum(-1000000000);
		               		               		minMMDSpread.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		minMMDSpread.setDigits(4);
		               		               		minMMDSpread.setBounds(59, 40, 115, 22);
		               		               		formToolkit.adapt(minMMDSpread);
		               		               		formToolkit.paintBordersFor(minMMDSpread);
		               		               		
		               		               		Label lblMMDSpread = new Label(MMDSpread, SWT.NONE);
		               		               		lblMMDSpread.setText("Points Between MMD");
		               		               		lblMMDSpread.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		lblMMDSpread.setAlignment(SWT.CENTER);
		               		               		lblMMDSpread.setBounds(0, 0, 184, 22);
		               		               		formToolkit.adapt(lblMMDSpread, true, true);
		               		               		
		               		               		Label lblMaxMMDSpread = new Label(MMDSpread, SWT.NONE);
		               		               		lblMaxMMDSpread.setText("Max");
		               		               		lblMaxMMDSpread.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		lblMaxMMDSpread.setBounds(0, 81, 41, 22);
		               		               		formToolkit.adapt(lblMaxMMDSpread, true, true);
		               		               		
		               		               		Composite CheapTradeTradeCoupon = formToolkit.createComposite(CheapTradeTradingInfoConfig, SWT.BORDER);
		               		               		CheapTradeTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		CheapTradeTradeCoupon.setBackground(SWTResourceManager.getColor(32, 44, 57));
		               		               		CheapTradeTradeCoupon.setBounds(620, 49, 177, 124);
		               		               		formToolkit.paintBordersFor(CheapTradeTradeCoupon);
		               		               		
		               		               		maxCheapTradeTradeCoupon = new Spinner(CheapTradeTradeCoupon, SWT.BORDER);
		               		               		maxCheapTradeTradeCoupon.setMaximum(2000000000);
		               		               		maxCheapTradeTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		maxCheapTradeTradeCoupon.setDigits(4);
		               		               		maxCheapTradeTradeCoupon.setBounds(61, 79, 102, 22);
		               		               		formToolkit.adapt(maxCheapTradeTradeCoupon);
		               		               		formToolkit.paintBordersFor(maxCheapTradeTradeCoupon);
		               		               		
		               		               		Label lblMinCheapTradeTradeCoupon = new Label(CheapTradeTradeCoupon, SWT.NONE);
		               		               		lblMinCheapTradeTradeCoupon.setText("Min");
		               		               		lblMinCheapTradeTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		lblMinCheapTradeTradeCoupon.setBounds(0, 41, 30, 22);
		               		               		formToolkit.adapt(lblMinCheapTradeTradeCoupon, true, true);
		               		               		
		               		               		minCheapTradeTradeCoupon = new Spinner(CheapTradeTradeCoupon, SWT.BORDER | SWT.WRAP);
		               		               		minCheapTradeTradeCoupon.setMaximum(2000000000);
		               		               		minCheapTradeTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		minCheapTradeTradeCoupon.setDigits(4);
		               		               		minCheapTradeTradeCoupon.setBounds(61, 38, 102, 22);
		               		               		formToolkit.adapt(minCheapTradeTradeCoupon);
		               		               		formToolkit.paintBordersFor(minCheapTradeTradeCoupon);
		               		               		
		               		               		Label lblCheapTradeTradeCoupon = new Label(CheapTradeTradeCoupon, SWT.NONE);
		               		               		lblCheapTradeTradeCoupon.setText("Coupon");
		               		               		lblCheapTradeTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		lblCheapTradeTradeCoupon.setAlignment(SWT.CENTER);
		               		               		lblCheapTradeTradeCoupon.setBounds(0, 0, 173, 24);
		               		               		formToolkit.adapt(lblCheapTradeTradeCoupon, true, true);
		               		               		
		               		               		Label lblMaxCheapTradeTradeCoupon = new Label(CheapTradeTradeCoupon, SWT.NONE);
		               		               		lblMaxCheapTradeTradeCoupon.setText("Max");
		               		               		lblMaxCheapTradeTradeCoupon.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		lblMaxCheapTradeTradeCoupon.setBounds(2, 79, 36, 22);
		               		               		formToolkit.adapt(lblMaxCheapTradeTradeCoupon, true, true);
		               		               		
		               		               		Button btnCheapTradeClearInputs = formToolkit.createButton(CheapTradeComposite, "Clear Inputs", SWT.BORDER);
		               		               		btnCheapTradeClearInputs.addSelectionListener(new SelectionAdapter() {
		               		               			@Override
		               		               			public void widgetSelected(SelectionEvent e) {
		               		               				cheapTradeClear();
		               		               			}
		               		               		});
		               		               		btnCheapTradeClearInputs.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		btnCheapTradeClearInputs.setBounds(459, 742, 112, 40);
		               		               		
		               		               		Button btnCheapTradeSubmit = formToolkit.createButton(CheapTradeComposite, "Submit", SWT.BORDER);
		               		               		btnCheapTradeSubmit.addSelectionListener(new SelectionAdapter() {
		               		               			@Override
		               		               			public void widgetSelected(SelectionEvent e) {
		               		               				System.out.println("Cheap Trade Search");

		               		               				cheapTrade cheapTradeForm = new cheapTrade();		
		               		               			   
		               		               			    
		               		               			    try {
		               		               					cheapTradeForm = cheapTradeSubmit(cheapTradeForm);
		               		               				    tableMethods t = null;
		               		               					t = new tableMethods();
		               		               					System.out.println("MMD: " + cheapTradeForm.bondInfoTrading.getMMDTable());
		               		               					LinkedHashMap<String, Table> tables = t.cheapTradeQuery(cheapTradeForm);
		               		               					t = null;
		               		               					if(tables.isEmpty() == false) {
		               		               					    DirectoryDialog dialog = new DirectoryDialog(BondInfoComposite.getShell());
		               		               							String filename = dialog.open();
		               		               							if( (filename != null) && !filename.equals(cusipDef) && !tables.isEmpty()) {
		               		               								
		               		               								int count = 0;
		               		               					
		               		               								for(String key: tables.keySet()) {
		               		               									System.out.println(filename);
		               		               									if(tables.get(key).isEmpty() == false) {
		               		               							    	//CsvWriteOptions.Builder builder = CsvWriteOptions.builder(filename + "/" + key + ".csv").separator(',');
		               		               							    	//CsvWriteOptions options = builder.build();
		               		           									tables.get(key).write().csv(filename + "/" + key + ".csv");
		               		               										 count += 1;
		               		               									}
		               		               								}
		               		               								MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
		               		               								if(count == 0) {
		               		               									Messagedialog.setMessage("No Results Found, Check Console for queries");	
		               		               								}
		               		               								else {
		               		               									Messagedialog.setMessage("Confirmed Results, Wrote " + count + " Tables");
		               		               									Messagedialog.setText("Results Written to: " + filename);
		               		               								}
		               		               								Messagedialog.open();
		               		               								

		               		               					}
		               		               					}
		               		               					else {
		               		               						MessageBox Messagedialog = new MessageBox(BondInfoComposite.getShell(), SWT.ICON_INFORMATION | SWT.OK);
		               		               						Messagedialog.setMessage("No Results Found, Check Console for queries");	
		               		               						Messagedialog.open();

		               		               					}
		               		               					
		               		               					
		               		               					cheapTradeForm = null;
		               		               				} catch (SQLException | BAMBOBException | ParseException | InvalidFormatException | IOException e1) {
		               		               					// TODO Auto-generated catch block
		               		               					e1.printStackTrace();
		               		               				}
		               		               				
		               		               			}
		               		               		});
		               		               		btnCheapTradeSubmit.setFont(SWTResourceManager.getFont(".AppleSystemUIFont", 11, SWT.NORMAL));
		               		               		btnCheapTradeSubmit.setBounds(602, 741, 80, 41);
		


		
		parent.setVisible(true);
		
		
		
	}

	@Override
	protected void checkSubclass() {

		// Disable the check that prevents subclassing of SWT components
	}
	
	public bondInfo bondInfoSubmit(bondInfo bondInfoForm) {

		System.out.println("Bond Info Submit");
		if( (minMatDate.getText().equals(dateDef) & maxMatDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minMatDate.getText(),maxMatDate.getText(),"R"));
			bondInfoForm.setMatDate(dateArray);
		}
		
		if( (minDatedDate.getText().equals(dateDef) & maxDatedDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minDatedDate.getText(),maxDatedDate.getText(),"R"));
			bondInfoForm.setDatedDate(dateArray);
			
		}

		if((minCoupon.getSelection() == spinDef & maxCoupon.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCoupon.getText(),maxCoupon.getText(),"R"));
			bondInfoForm.setCoupon(spinArray);
			
		}


		if((minYield.getSelection() == spinDef & maxYield.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minYield.getText(),maxYield.getText(),"R"));
			bondInfoForm.setYield(spinArray);
		}
		

		if(((minPrice.getSelection() == spinDef) & (maxPrice.getSelection() == spinDef)) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minPrice.getText(),maxPrice.getText(),"R"));
			bondInfoForm.setPrice(spinArray);
			
			
		}
		if( (btnTaxExempt.getSelection()  |  btnNotTaxExempt.getSelection() | btnAMT.getSelection())) {
			boolean b1 = btnTaxExempt.getSelection(); 
			boolean b2 = btnNotTaxExempt.getSelection(); 
			boolean b3 = btnAMT.getSelection(); 

			ArrayList<Boolean> buttonArray = new ArrayList<>(Arrays.asList(b1,b2,b3));
			bondInfoForm.setTaxExempt(buttonArray);
			
		}
		
		if(Arrays.deepToString(couponCodeList.getSelection()).equals(listDef) == false) {
			
			int[] selectedIndices = couponCodeList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, couponCodes);

			
			bondInfoForm.setCouponCode(listArray);

			
		}
		if(Arrays.deepToString(debtTypeList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = debtTypeList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, debtTypeCodes);
			bondInfoForm.setDebtType(listArray);

			
		}
		if(Arrays.deepToString(stateList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = stateList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, stateCodes);			
			bondInfoForm.setState(listArray);

			
		}
		if(Arrays.deepToString(useProceedsList.getSelection()).equals(listDef) == false) {
			
			int[] selectedIndices = useProceedsList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, useProceedsCodes);			

			bondInfoForm.setUseProceeds(listArray);

		}
		if(Arrays.deepToString(insuranceList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = insuranceList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, bondInsuranceCodes);			
			bondInfoForm.setInsurance(listArray);

			
		}
		
		if(Arrays.deepToString(bondInfoSPRatingList.getSelection()).equals(listDef) == false) {
		System.out.println("SP Rating List Selected");
		int[] selectedIndices = bondInfoSPRatingList.getSelectionIndices();
		ArrayList<String> listArray = selectArrayList(selectedIndices, spRatingCodes);			
		bondInfoForm.SPRating = listArray;
		}
		
		if(Arrays.deepToString(bondInfoMoodyRatingList.getSelection()).equals(listDef) == false) {
		System.out.println("Moody Rating List Selected");
		int[] selectedIndices = bondInfoMoodyRatingList.getSelectionIndices();
		ArrayList<String> listArray = selectArrayList(selectedIndices, moodyRatingCodes);	
		bondInfoForm.MoodyRating = listArray;
		} 
		
		if(Arrays.deepToString(addlCreditList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = addlCreditList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, addlCreditCodes);
			bondInfoForm.setAddlCredit(listArray);
		}
		
		
		if(Arrays.deepToString(capitalPurposeList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = capitalPurposeList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, capitalPurposeCodes);
			bondInfoForm.setCapitalPurpose(listArray);
		}
		
		
		

		
		if((btnYesBankQual.getSelection()  |  btnNoBankQual.getSelection())) {
			boolean b1 = btnYesBankQual.getSelection(); 
			boolean b2 = btnNoBankQual.getSelection(); 

			ArrayList<Boolean> buttonArray = new ArrayList<>(Arrays.asList(b1,b2));
			bondInfoForm.setBankQualified(buttonArray);
			
			
		}
		

		if((minCurrIntRate.getSelection() == spinDef & maxCurrIntRate.getSelection() == spinDef) == false) {

			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCurrIntRate.getText(),maxCurrIntRate.getText(),"R"));
			bondInfoForm.setCurrInterestRate(spinArray);
			
		}
		if( (minUpdatedDate.getText().equals(dateDef) & maxUpdatedDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minUpdatedDate.getText(),maxUpdatedDate.getText(),"R"));
			System.out.println("Update Date:" + dateArray.toString());		
			bondInfoForm.setUpdatedDate(dateArray);
			
		}
		
		if( (minNextCallDate.getText().equals(dateDef) & maxNextCallDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minNextCallDate.getText(),maxNextCallDate.getText(),"R"));
			bondInfoForm.setNextCallDate(dateArray);
			
		}
		

		if((minNextCallPrice.getSelection() == spinDef & maxNextCallPrice.getSelection() == spinDef) == false) {

			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minNextCallPrice.getText(),maxNextCallPrice.getText(),"R"));
			bondInfoForm.setNextCallPrice(spinArray);
			
		}
		
		if( (minCallDate.getText().equals(dateDef) & maxCallDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minCallDate.getText(),maxCallDate.getText(),"R"));
			bondInfoForm.setCallDate(dateArray);
			
		}
		

		if((minCallPrice.getSelection() == spinDef & maxCallPrice.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCallPrice.getText(),maxCallPrice.getText(),"R"));
			bondInfoForm.setCallPrice(spinArray);
			
		}
		
		if( (minPartialCallDate.getText().equals(dateDef) & maxPartialCallDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minPartialCallDate.getText(),maxPartialCallDate.getText(),"R"));
			bondInfoForm.setPartialCallDate(dateArray);
			
		}
		

		if((minPartialCallRate.getSelection() == spinDef & maxPartialCallRate.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minPartialCallRate.getText(),maxPartialCallRate.getText(),"R"));
			bondInfoForm.setPartialCallRate(spinArray);
			
			
		}
		
		if( (minSinkDate.getText().equals(dateDef) & maxSinkDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minSinkDate.getText(),maxSinkDate.getText(),"R"));
			bondInfoForm.setSinkDate(dateArray);
			
		}
		

		if((minSinkPrice.getSelection() == spinDef & maxSinkPrice.getSelection() == spinDef) == false) {

			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minSinkPrice.getText(),maxSinkPrice.getText(),"R"));
			bondInfoForm.setCallPrice(spinArray);

			
		}
		
		if( (minRedemptionDate.getText().equals(dateDef) & maxRedemptionDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minRedemptionDate.getText(),maxRedemptionDate.getText(),"R"));
			bondInfoForm.setRedemptionDate(dateArray);

			
		}
		

		if((minRedemptionPrice.getSelection() == spinDef & minRedemptionPrice.getSelection() == spinDef) == false) {

			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minRedemptionPrice.getText(),maxRedemptionPrice.getText(),"R"));
			bondInfoForm.setRedemptionPrice(spinArray);

			
		}
		if(btnActiveMaturities.getSelection()  == false) {
			System.out.println("Active Maturities ON");
			bondInfoForm.setActiveMaturities(!btnActiveMaturities.getSelection());
		}
		
		if(btnTradingChecks.getSelection()) {
			System.out.println("Trading Selection Toggled");
			bondInfoForm.bondInfoTrading.setTradingChecks(btnTradingChecks.getSelection());
			System.out.println(bondInfoForm.bondInfoTrading.isTradingChecks());
			bondInfoForm.bondInfoTrading.setCusipFilter(btnFilterCusips.getSelection());

			if(btnCalculateTradingYield.getSelection()) {
				
				bondInfoForm.bondInfoTrading.setCalculateTradingYield(btnCalculateTradingYield.getSelection());
			}
			
			if(btnUploadMmd.getData() == null | btnUploadMmd.getData().toString().equals(cusipDef)) {
				System.out.println("No MMD Table Uploaded");
			}
			else {
				bondInfoForm.bondInfoTrading.setMMDTable(btnUploadMmd.getData().toString());
			}
			
				
			if((minTradeDate.getText().equals(dateDef) & maxTradeDate.getText().equals(dateDef)) == false) {
				ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minTradeDate.getText(),maxTradeDate.getText(),"R"));
				bondInfoForm.bondInfoTrading.setTradeDate(dateArray);
	
				}

			if((minParAmountTraded.getSelection() == spinDef & maxParAmountTraded.getSelection() == spinDef) == false) {

				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minParAmountTraded.getText(),maxParAmountTraded.getText(),"R"));
				bondInfoForm.bondInfoTrading.setParAmount(spinArray);
				
			}
			
			if((minParDollarPrice.getSelection() == spinDef & maxParDollarPrice.getSelection() == spinDef) == false) {
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minParDollarPrice.getText(),maxParDollarPrice.getText(),"R"));
				bondInfoForm.bondInfoTrading.setParDollarPrice(spinArray);
				
			}
			if((minTradedYield.getSelection() == spinDef & maxTradedYield.getSelection() == spinDef) == false) {
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minTradedYield.getText(),maxTradedYield.getText(),"R"));
				bondInfoForm.bondInfoTrading.setParTradedYield(spinArray);

				
			}
			if( (btnTradeTypeD.getSelection()  |  btnTradeTypeS.getSelection() | btnTradeTypeP.getSelection())) {
				boolean b1 = btnTradeTypeD.getSelection(); 
				boolean b2 = btnTradeTypeS.getSelection(); 
				boolean b3 = btnTradeTypeP.getSelection(); 

				ArrayList<Boolean> buttonArray = new ArrayList<>(Arrays.asList(b1,b2,b3));
				bondInfoForm.bondInfoTrading.setTradeType(buttonArray);

				
			}
			
			
		}
		
		return(bondInfoForm);
	}
	public quickBond quickBondSubmit(quickBond quickBondForm) throws IOException {
					
		if(txtQuickBondCusipSearch.getText().equals(cusipDef) == false) {
				quickBondForm.setCusipText(txtQuickBondCusipSearch.getText());
			}
	
		if(btnQuickBondCusipList.getData().toString().equals(cusipDef) == false) {
			quickBondForm.setCusipFile(btnQuickBondCusipList.getData().toString());

			}
			
		if(btnQuickBondActiveMaturities.getSelection() == false) {		
			quickBondForm.setActiveMaturities(!btnActiveMaturities.getSelection());
		}
			
			
		if(btnQuickBondTradingChecks.getSelection()) {
			quickBondForm.bondInfoTrading.setTradingChecks(btnQuickBondTradingChecks.getSelection());
			quickBondForm.bondInfoTrading.setCusipFilter(btnQuickBondFilterCusips.getSelection());

			
			if(btnQuickBondCalculateTradingYield.getSelection()) {
				
				quickBondForm.bondInfoTrading.setCalculateTradingYield(btnQuickBondCalculateTradingYield.getSelection());

				
			}
			if(btnQuickBondUploadMmd.getData() == null | btnQuickBondUploadMmd.getData().toString().equals(cusipDef) ) {
				System.out.println("No MMD Table Uploaded");
			}
			else {
				quickBondForm.bondInfoTrading.setMMDTable(btnQuickBondUploadMmd.getData().toString());
			}
			
			
				
			if((minQuickBondTradeDate.getText().equals(dateDef) & maxQuickBondTradeDate.getText().equals(dateDef)) == false) {
				ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minQuickBondTradeDate.getText(),maxQuickBondTradeDate.getText(),"R"));
				quickBondForm.bondInfoTrading.setTradeDate(dateArray);
	
				}
			
			if((minQuickBondMaturityDate.getText().equals(dateDef) & maxQuickBondMaturityDate.getText().equals(dateDef)) == false) {
				ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minQuickBondMaturityDate.getText(),maxQuickBondMaturityDate.getText(),"R"));
				quickBondForm.bondInfoTrading.setMaturityDate(dateArray);
				}
					
			if((minQuickBondCoupon.getText().equals(spinDef) & maxQuickBondCoupon.getText().equals(spinDef) == false)) {
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minQuickBondCoupon.getText(),maxQuickBondCoupon.getText(),"R"));
				quickBondForm.bondInfoTrading.setCoupon(spinArray);
				
			}
			

			if( (minQuickBondParAmountTraded.getSelection() == spinDef & maxQuickBondParAmountTraded.getSelection() == spinDef) == false) {
				System.out.println(minQuickBondParAmountTraded.getSelection());
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minQuickBondParAmountTraded.getText(),maxQuickBondParAmountTraded.getText(),"R"));
				System.out.println("Par Activated");
				System.out.println(spinArray);
				quickBondForm.bondInfoTrading.setParAmount(spinArray);
				
			}
			
			

			if((minQuickBondParDollarPrice.getSelection() == spinDef & maxQuickBondParDollarPrice.getSelection() == spinDef) == false) {
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minQuickBondParDollarPrice.getText(),maxQuickBondParDollarPrice.getText(),"R"));
				quickBondForm.bondInfoTrading.setParDollarPrice(spinArray);
				
			}
			if((minQuickBondTradedYield.getSelection() == spinDef & maxQuickBondTradedYield.getSelection() == spinDef) == false) {
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minQuickBondTradedYield.getText(),maxQuickBondTradedYield.getText(),"R"));
				quickBondForm.bondInfoTrading.setParTradedYield(spinArray);

				
			}
			if( (btnQuickBondTradeTypeD.getSelection()  |  btnQuickBondTradeTypeS.getSelection() | btnQuickBondTradeTypeP.getSelection())) {
				boolean b1 = btnQuickBondTradeTypeD.getSelection(); 
				boolean b2 = btnQuickBondTradeTypeS.getSelection(); 
				boolean b3 = btnQuickBondTradeTypeP.getSelection(); 

				ArrayList<Boolean> buttonArray = new ArrayList<>(Arrays.asList(b1,b2,b3));
				quickBondForm.bondInfoTrading.setTradeType(buttonArray);

				
			}
			
			
		}
		return(quickBondForm);
	}
	public tradingCusipVisualiser TCVSubmit(tradingCusipVisualiser TCVBondForm) throws IOException {
			TCVBondForm.bondInfoTrading.setTradingChecks(true);
			
			if(txtTCVCusipSearch.getText().equals(cusipDef) == false) {
				TCVBondForm.setCusipText(txtTCVCusipSearch.getText());

			}
	
			if(btnTCVCusipList.getData().toString().equals(cusipDef) == false) {
				TCVBondForm.setCusipFile(btnTCVCusipList.getData().toString());

			}
			
				
			TCVBondForm.bondInfoTrading.setCalculateTradingYield(true);
			
			if(btnTCVUploadMmd.getData() == null | btnTCVUploadMmd.getData().toString().equals(cusipDef)) {
			System.out.println("No MMD Table Uploaded");
			}
			else {
				TCVBondForm.bondInfoTrading.setMMDTable(btnTCVUploadMmd.getData().toString());
			}
			
			
		
			System.out.println("TCV Trade");
			

			if ((minTCVTradeDate.getText().equals(dateDef) & maxTCVTradeDate.getText().equals(dateDef)) == false)  {
				ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minTCVTradeDate.getText(),maxTCVTradeDate.getText(),"R"));
				TCVBondForm.bondInfoTrading.setTradeDate(dateArray);
				}
			
			if((minTCVMaturityDate.getText().equals(dateDef) && maxTCVMaturityDate.getText().equals(dateDef)) == false) {
				ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minTCVMaturityDate.getText(),maxTCVMaturityDate.getText(),"R"));
				TCVBondForm.bondInfoTrading.setMaturityDate(dateArray);
				}
			
			if((minTCVCoupon.getSelection() == spinDef && maxTCVCoupon.getSelection() == spinDef) == false) {
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minTCVCoupon.getText(),maxTCVCoupon.getText(),"R"));


				
				TCVBondForm.bondInfoTrading.setCoupon(spinArray);
				
			}
			if((minTCVParAmountTraded.getSelection() == spinDef && maxTCVParAmountTraded.getSelection() == spinDef) == false) {
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minTCVParAmountTraded.getText(),maxTCVParAmountTraded.getText(),"R"));
				TCVBondForm.bondInfoTrading.setParAmount(spinArray);
				
			}
			
			if((minTCVParDollarPrice.getSelection() == spinDef && maxTCVParDollarPrice.getSelection() == spinDef) == false) {

				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minTCVParDollarPrice.getText(),maxTCVParDollarPrice.getText(),"R"));
				TCVBondForm.bondInfoTrading.setParDollarPrice(spinArray);
				
			}
			
			
			if((minTCVTradedYield.getSelection() == spinDef && maxTCVTradedYield.getSelection() == spinDef) == false) {
			
				System.out.println(minTCVTradedYield.getText());
				System.out.println(maxTCVTradedYield.getText());

				
				ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minTCVTradedYield.getText(),maxTCVTradedYield.getText(),"R"));
				TCVBondForm.bondInfoTrading.setParTradedYield(spinArray);

				
			}
			if( (btnTCVTradeTypeD.getSelection()  |  btnTCVTradeTypeS.getSelection() | btnTCVTradeTypeP.getSelection())) {
				boolean b1 = btnTCVTradeTypeD.getSelection(); 
				boolean b2 = btnTCVTradeTypeS.getSelection(); 
				boolean b3 = btnTCVTradeTypeP.getSelection(); 

				ArrayList<Boolean> buttonArray = new ArrayList<>(Arrays.asList(b1,b2,b3));
				TCVBondForm.bondInfoTrading.setTradeType(buttonArray);

				
			}
			
			
		return(TCVBondForm);
	}

	public cheapTrade cheapTradeSubmit(cheapTrade cheapTradeForm) throws IOException {
		System.out.println("Cheap Trade Info Submit");
		if( (minCheapTradeMatDate.getText().equals(dateDef) & maxCheapTradeMatDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minCheapTradeMatDate.getText(),maxCheapTradeMatDate.getText(),"R"));
			cheapTradeForm.setMatDate(dateArray);
		}
		
		if( (minCheapTradeDatedDate.getText().equals(dateDef) & maxCheapTradeDatedDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minCheapTradeDatedDate.getText(),maxCheapTradeDatedDate.getText(),"R"));
			cheapTradeForm.setDatedDate(dateArray);
			
		}

		if((minCheapTradeCoupon.getSelection() == spinDef & maxCheapTradeCoupon.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCheapTradeCoupon.getText(),maxCheapTradeCoupon.getText(),"R"));
			cheapTradeForm.setCoupon(spinArray);
			
		}


		if((minCheapTradeYield.getSelection() == spinDef & maxCheapTradeYield.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCheapTradeYield.getText(),maxCheapTradeYield.getText(),"R"));
			cheapTradeForm.setYield(spinArray);
		}
		

		if(((minCheapTradePrice.getSelection() == spinDef) & (maxCheapTradePrice.getSelection() == spinDef)) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCheapTradePrice.getText(),maxCheapTradePrice.getText(),"R"));
			cheapTradeForm.setPrice(spinArray);
			
		}
		
		
		if( (btnCheapTradeTaxExempt.getSelection()  |  btnCheapTradeNotTaxExempt.getSelection() | btnCheapTradeAMT.getSelection())) {
			boolean b1 = btnCheapTradeTaxExempt.getSelection(); 
			boolean b2 = btnCheapTradeNotTaxExempt.getSelection(); 
			boolean b3 = btnCheapTradeAMT.getSelection(); 

			ArrayList<Boolean> buttonArray = new ArrayList<>(Arrays.asList(b1,b2,b3));
			cheapTradeForm.setTaxExempt(buttonArray);
			
		}
		
		if(Arrays.deepToString(CheapTradeCouponCodeList.getSelection()).equals(listDef) == false) {
			
			int[] selectedIndices = CheapTradeCouponCodeList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, couponCodes);

			
			cheapTradeForm.setCouponCode(listArray);

			
		}
		if(Arrays.deepToString(CheapTradeDebtTypeList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = CheapTradeDebtTypeList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, debtTypeCodes);
			cheapTradeForm.setDebtType(listArray);

			
		}
		if(Arrays.deepToString(CheapTradeStateList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = CheapTradeStateList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, stateCodes);			
			cheapTradeForm.setState(listArray);

			
		}
		if(Arrays.deepToString(CheapTradeUseProceedsList.getSelection()).equals(listDef) == false) {
			
			int[] selectedIndices = CheapTradeUseProceedsList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, useProceedsCodes);			
			cheapTradeForm.setUseProceeds(listArray);

		}
		if(Arrays.deepToString(CheapTradeInsuranceList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = CheapTradeInsuranceList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, bondInsuranceCodes);			
			cheapTradeForm.setInsurance(listArray);
		}
		if(Arrays.deepToString(CheapTradeAddlCreditList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = CheapTradeAddlCreditList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, addlCreditCodes);			
			cheapTradeForm.setAddlCredit(listArray);
		}
		
		if(Arrays.deepToString(CheapTradeCapitalPurposeList.getSelection()).equals(listDef) == false) {
			int[] selectedIndices = CheapTradeCapitalPurposeList.getSelectionIndices();
			ArrayList<String> listArray = selectArrayList(selectedIndices, capitalPurposeCodes);			
			cheapTradeForm.setCapitalPurpose(listArray);
		}
		
		
		if(Arrays.deepToString(spRatingList.getSelection()).equals(listDef) == false) {
		System.out.println("SP Rating List Selected");
		int[] selectedIndices = spRatingList.getSelectionIndices();
		ArrayList<String> listArray = selectArrayList(selectedIndices, spRatingCodes);			
		cheapTradeForm.SPRating = listArray;
		}
		
		if(Arrays.deepToString(moodyRatingList.getSelection()).equals(listDef) == false) {
		System.out.println("Moody Rating List Selected");
		int[] selectedIndices = moodyRatingList.getSelectionIndices();
		ArrayList<String> listArray = selectArrayList(selectedIndices, moodyRatingCodes);	
		cheapTradeForm.MoodyRating = listArray;
		} 
		
		
		if( (minCheapTradeUpdatedDate.getText().equals(dateDef) & maxCheapTradeUpdatedDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minCheapTradeUpdatedDate.getText(),maxCheapTradeUpdatedDate.getText(),"R"));
			System.out.println("Update Date:" + dateArray.toString());		
			cheapTradeForm.setUpdatedDate(dateArray);
			
		}
		
		if( (minCheapTradeNextCallDate.getText().equals(dateDef) & maxCheapTradeNextCallDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minCheapTradeNextCallDate.getText(),maxCheapTradeNextCallDate.getText(),"R"));
			cheapTradeForm.setNextCallDate(dateArray);
			
		}
		

		if((minCheapTradeNextCallPrice.getSelection() == spinDef & maxCheapTradeNextCallPrice.getSelection() == spinDef) == false) {

			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCheapTradeNextCallPrice.getText(),maxCheapTradeNextCallPrice.getText(),"R"));
			cheapTradeForm.setNextCallPrice(spinArray);
			
		}
		
		
		if(btnCheapTradeActiveMaturities.getSelection()  == false) {
			System.out.println("Active Maturities ON");
			cheapTradeForm.setActiveMaturities(!btnCheapTradeActiveMaturities.getSelection());
		}
		
		
		System.out.println("Trading Selection Toggled");
		cheapTradeForm.bondInfoTrading.setTradingChecks(true);

		if(btnCheapTradeCalculateTradingYield.getSelection()) {
			
			cheapTradeForm.bondInfoTrading.setCalculateTradingYield(btnCheapTradeCalculateTradingYield.getSelection());
		}
	
		if(btnCheapTradeUploadMmd.getData() == null | btnCheapTradeUploadMmd.getData().toString().equals(cusipDef)) {
			System.out.println("No MMD Table Uploaded");
		}
		else {
			cheapTradeForm.bondInfoTrading.setMMDTable(btnCheapTradeUploadMmd.getData().toString());
		}
		
		
		if((minCheapTradeTradeDate.getText().equals(dateDef) & maxCheapTradeTradeDate.getText().equals(dateDef)) == false) {
			ArrayList<String> dateArray = new ArrayList<>(Arrays.asList(minCheapTradeTradeDate.getText(),maxCheapTradeTradeDate.getText(),"R"));
			cheapTradeForm.bondInfoTrading.setTradeDate(dateArray);

			}

		if((minCheapTradeParAmountTraded.getSelection() == spinDef & maxCheapTradeParAmountTraded.getSelection() == spinDef) == false) {

			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCheapTradeParAmountTraded.getText(),maxCheapTradeParAmountTraded.getText(),"R"));
			cheapTradeForm.bondInfoTrading.setParAmount(spinArray);
			
		}
		
		if((minCheapTradeParDollarPrice.getSelection() == spinDef & maxCheapTradeParDollarPrice.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCheapTradeParDollarPrice.getText(),maxCheapTradeParDollarPrice.getText(),"R"));
			cheapTradeForm.bondInfoTrading.setParDollarPrice(spinArray);
			
		}
		if((minCheapTradeTradedYield.getSelection() == spinDef & maxCheapTradeTradedYield.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCheapTradeTradedYield.getText(),maxCheapTradeTradedYield.getText(),"R"));
			cheapTradeForm.bondInfoTrading.setParTradedYield(spinArray);

		}
		if((minCheapTradeTradeCoupon.getSelection() == spinDef & maxCheapTradeTradeCoupon.getSelection() == spinDef) == false) {
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(minCheapTradeTradeCoupon.getText(),maxCheapTradeTradeCoupon.getText(),"R"));
			cheapTradeForm.bondInfoTrading.setCoupon(spinArray);

		}
		
		
		
		if((minMMDSpread.getSelection() == spinDef & maxMMDSpread.getSelection() == spinDef) == false) {
			String min = "";
			String max = "";
			if(maxMMDSpread.getSelection() != spinDef) {
				max = maxMMDSpread.getText();
				
			}
			if(minMMDSpread.getSelection() != spinDef) {
				min = minMMDSpread.getText();
				
			}
			
			ArrayList<String> spinArray = new ArrayList<>(Arrays.asList(min,max,"R"));
			cheapTradeForm.setMMDSpread(spinArray);

		}
		
		if( (btnCheapTradeTradeTypeD.getSelection()  |  btnCheapTradeTradeTypeS.getSelection() | btnCheapTradeTradeTypeP.getSelection())) {
			boolean b1 = btnCheapTradeTradeTypeD.getSelection(); 
			boolean b2 = btnCheapTradeTradeTypeS.getSelection(); 
			boolean b3 = btnCheapTradeTradeTypeP.getSelection(); 

			ArrayList<Boolean> buttonArray = new ArrayList<>(Arrays.asList(b1,b2,b3));
			cheapTradeForm.bondInfoTrading.setTradeType(buttonArray);

			
		}
		
		
		
		
		return(cheapTradeForm);
	}
	
	public void bondInfoClear() {
		minMatDate.setValue(null);
		maxMatDate.setValue(null);
		maxDatedDate.setValue(null);
		minDatedDate.setValue(null);
		minCoupon.setSelection(0); 
		maxCoupon.setSelection(0); 
		minYield.setSelection(0); 
		maxYield.setSelection(0); 
		minParAmount.setSelection(0);
		maxParAmount.setSelection(0);
		minPrice.setSelection(0);
		maxPrice.setSelection(0);


		btnTaxExempt.setSelection(false);
		btnNotTaxExempt.setSelection(false);
		btnAMT.setSelection(false);
		btnNoBankQual.setSelection(false);
		btnYesBankQual.setSelection(false);
		
		couponCodeList.setSelection(-1);
		debtTypeList.setSelection(-1);
		stateList.setSelection(-1);
		useProceedsList.setSelection(-1);
		insuranceList.setSelection(-1);
		addlCreditList.setSelection(-1);
		capitalPurposeList.setSelection(-1);
		bondInfoSPRatingList.setSelection(-1);
		bondInfoMoodyRatingList.setSelection(-1);

		minCurrIntRate.setSelection(0);
		maxCurrIntRate.setSelection(0);
		
		minNextCallDate.setValue(null);;
		maxNextCallDate.setValue(null);;
		minNextCallPrice.setSelection(0);
		maxNextCallPrice.setSelection(0);
		
		minUpdatedDate.setValue(null);;
		maxUpdatedDate.setValue(null);;

		
		minCallDate.setValue(null);;
		maxCallDate.setValue(null);;
		minCallPrice.setSelection(0);
		maxCallPrice.setSelection(0);
		
		minPartialCallDate.setValue(null);
		maxPartialCallDate.setValue(null);
		minSinkDate.setValue(null);
		maxSinkDate.setValue(null);
		
		maxPartialCallRate.setSelection(0);
		minPartialCallRate.setSelection(0);
		minSinkPrice.setSelection(0);
		maxSinkPrice.setSelection(0);
		minRedemptionDate.setValue(null);
		maxRedemptionDate.setValue(null);
		minRedemptionPrice.setSelection(0);
		maxRedemptionPrice.setSelection(0);

		
		btnActiveMaturities.setSelection(true);
		
		minParAmountTraded.setSelection(0);
		maxParAmountTraded.setSelection(0);
		minTradeDate.setValue(null);
		maxTradeDate.setValue(null);
		minTradedYield.setSelection(0);
		maxTradedYield.setSelection(0);

		minParDollarPrice.setSelection(0);
		maxParDollarPrice.setSelection(0);

		btnCalculateTradingYield.setSelection(false);;
		btnTradingChecks.setSelection(false);;
		btnUploadMmd.setData(cusipDef);
		btnTradeTypeD.setSelection(false);
		btnTradeTypeS.setSelection(false);
		btnTradeTypeP.setSelection(false);
		btnFilterCusips.setSelection(false);

		
	}

	public void quickBondClear() {
		
		btnQuickBondActiveMaturities.setSelection(true);
		txtQuickBondCusipSearch.setText("");
		
		minQuickBondParAmountTraded.setSelection(0);
		maxQuickBondParAmountTraded.setSelection(0);
		minQuickBondTradeDate.setValue(null);
		maxQuickBondTradeDate.setValue(null);
		minQuickBondMaturityDate.setValue(null);
		maxQuickBondMaturityDate.setValue(null);
		minQuickBondCoupon.setSelection(0);
		maxQuickBondCoupon.setSelection(0);

		minQuickBondTradedYield.setSelection(0);
		maxQuickBondTradedYield.setSelection(0);

		minQuickBondParDollarPrice.setSelection(0);
		maxQuickBondParDollarPrice.setSelection(0);

		btnQuickBondCalculateTradingYield.setSelection(false);
		btnQuickBondTradingChecks.setSelection(false);
		btnQuickBondUploadMmd.setData(cusipDef);
		btnQuickBondCusipList.setData(cusipDef);;
		btnQuickBondTradeTypeD.setSelection(false);
		btnQuickBondTradeTypeS.setSelection(false);
		btnQuickBondTradeTypeP.setSelection(false);
		btnQuickBondFilterCusips.setSelection(false);

		System.out.println(btnQuickBondUploadMmd.getData().toString());
		
		
	}

	public void TCVClear() {
		
		
		txtTCVCusipSearch.setText("");
		
		
		
		minTCVParAmountTraded.setSelection(0);
		maxTCVParAmountTraded.setSelection(0);
		minTCVTradeDate.setValue(null);
		maxTCVTradeDate.setValue(null);
		
		minTCVMaturityDate.setValue(null);
		maxTCVMaturityDate.setValue(null);
		
		minTCVCoupon.setSelection(0);
		maxTCVCoupon.setSelection(0);

		
		minTCVTradedYield.setSelection(0);
		maxTCVTradedYield.setSelection(0);

		minTCVParDollarPrice.setSelection(0);
		maxTCVParDollarPrice.setSelection(0);

		btnTCVUploadMmd.setData(cusipDef);
		btnTCVCusipList.setData(cusipDef);
		btnTCVTradeTypeD.setSelection(false);
		btnTCVTradeTypeS.setSelection(false);
		btnTCVTradeTypeP.setSelection(false);
		
		
	}
	
	public void cheapTradeClear() {
		
		 minCheapTradeMatDate.setValue(null);
		 maxCheapTradeMatDate.setValue(null);
		 maxCheapTradeDatedDate.setValue(null);
		 minCheapTradeDatedDate.setValue(null);
		 minCheapTradeCoupon.setSelection(0); 
		 maxCheapTradeCoupon.setSelection(0); 
		 minCheapTradeYield.setSelection(0); 
		 maxCheapTradeYield.setSelection(0); 
		 minCheapTradeParAmount.setSelection(0);
		 maxCheapTradeParAmount.setSelection(0);
		 minCheapTradePrice.setSelection(0);
		 maxCheapTradePrice.setSelection(0);


		 btnCheapTradeTaxExempt.setSelection(false);
		 btnCheapTradeNotTaxExempt.setSelection(false);
		 btnCheapTradeAMT.setSelection(false);
		 btnCheapTradeNoBankQual.setSelection(false);
		 btnCheapTradeYesBankQual.setSelection(false);

		
		
		CheapTradeCouponCodeList.setSelection(-1);
		CheapTradeDebtTypeList.setSelection(-1);
		CheapTradeStateList.setSelection(-1);
		CheapTradeUseProceedsList.setSelection(-1);
		CheapTradeInsuranceList.setSelection(-1);
		spRatingList.setSelection(-1);
		moodyRatingList.setSelection(-1);;
		CheapTradeAddlCreditList.setSelection(-1);;
		CheapTradeCapitalPurposeList.setSelection(-1);
		
		minCheapTradeNextCallPrice.setSelection(0);
		maxCheapTradeNextCallPrice.setSelection(0);
		minCheapTradeNextCallDate.setValue(null);
		maxCheapTradeNextCallDate.setValue(null);
		
		
		 maxCheapTradeUpdatedDate.setValue(null);
		 minCheapTradeUpdatedDate.setValue(null);

		
		 btnCheapTradeActiveMaturities.setSelection(true);

		
		
		 minCheapTradeParAmountTraded.setSelection(0);
		 maxCheapTradeParAmountTraded.setSelection(0);
		 minCheapTradeTradeDate.setValue(null);
		 maxCheapTradeTradeDate.setValue(null);
		 minCheapTradeTradedYield.setSelection(0);
		 maxCheapTradeTradedYield.setSelection(0);

		 minCheapTradeParDollarPrice.setSelection(0);
		 maxCheapTradeParDollarPrice.setSelection(0);

		 btnCheapTradeCalculateTradingYield.setSelection(false);
		 btnCheapTradeUploadMmd.setData(cusipDef);
		 btnCheapTradeTradeTypeD.setSelection(false);
		 btnCheapTradeTradeTypeS.setSelection(false);
		 btnCheapTradeTradeTypeP.setSelection(false);
		
		 maxMMDSpread.setSelection(0);
		 minMMDSpread.setSelection(0);
		 
		 minCheapTradeTradeCoupon.setSelection(0);
		 maxCheapTradeTradeCoupon.setSelection(0);
		
	}
	public ArrayList<String> selectArrayList(int[] selectionIndices, ArrayList<ArrayList<String>> userList) {
		ArrayList<String> flatList = new ArrayList<String>();
		
		for(int i: selectionIndices ) {
			flatList.addAll(userList.get(i));
		}
		System.out.println(flatList.toString());
		return(flatList);
		
	}
	
	
	public static boolean isBusinessDay(BD2 date){
		Date d = date.toJavaDate();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		System.out.print("Day:" + cal.toString());

		// check if weekend
		if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
			return false;
		}
		
		// check if New Year's Day
		if (cal.get(Calendar.MONTH) == Calendar.JANUARY
			&& cal.get(Calendar.DAY_OF_MONTH) == 1) {
			return false;
		}
		
		// check if Christmas
		if (cal.get(Calendar.MONTH) == Calendar.DECEMBER
			&& cal.get(Calendar.DAY_OF_MONTH) == 25) {
			return false;
		}
		
		// check if 4th of July
		if (cal.get(Calendar.MONTH) == Calendar.JULY
			&& cal.get(Calendar.DAY_OF_MONTH) == 4) {
			return false;
		}
		
		// check Thanksgiving (4th Thursday of November)
		if (cal.get(Calendar.MONTH) == Calendar.NOVEMBER
			&& cal.get(Calendar.DAY_OF_WEEK_IN_MONTH) == 4
			&& cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
			return false;
		}
		
		// check Memorial Day (last Monday of May)
		if (cal.get(Calendar.MONTH) == Calendar.MAY
			&& cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY
			&& cal.get(Calendar.DAY_OF_MONTH) > (31 - 7) ) {
			return false;
		}
		
		// check Labor Day (1st Monday of September)
		if (cal.get(Calendar.MONTH) == Calendar.SEPTEMBER
			&& cal.get(Calendar.DAY_OF_WEEK_IN_MONTH) == 1
			&& cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
			return false;
		}
		
		// check President's Day (3rd Monday of February)
		if (cal.get(Calendar.MONTH) == Calendar.FEBRUARY
		&& cal.get(Calendar.DAY_OF_WEEK_IN_MONTH) == 3
		&& cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
		return true;
		}
		
		// check Veterans Day (November 11)
		if (cal.get(Calendar.MONTH) == Calendar.NOVEMBER
		&& cal.get(Calendar.DAY_OF_MONTH) == 11) {
		return true;
		}
		
		// check MLK Day (3rd Monday of January)
		if (cal.get(Calendar.MONTH) == Calendar.JANUARY
		&& cal.get(Calendar.DAY_OF_WEEK_IN_MONTH) == 3
		&& cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
		return true;
		}
		
		// IF NOTHING ELSE, IT'S A BUSINESS DAY
		return true;
	}

	
	
}


