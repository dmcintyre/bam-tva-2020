package tvagui;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.bamcore.util.BobDataSource;


import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.columns.Column;
import tech.tablesaw.io.csv.CsvReadOptions;
import tech.tablesaw.selection.Selection;
import tech.tablesaw.api.*;
import tech.tablesaw.io.jdbc.*;





public class sqlConnections {
	BobDataSource mergentInstance =  null;

    Connection conn;
    Statement stmt;
    
    public void openConnection() throws SQLException {

			mergentInstance = com.bamcore.util.BobDataSource.getMergentInstance();
			conn = mergentInstance.getConnection();
			stmt = conn.createStatement();
			System.out.println("Connection Successful");
		
    }
    
    
    public Connection getConnection() throws SQLException {
    	return(this.conn);
    }
    
    public Statement getStatement() throws SQLException {
    	return(this.stmt);
    }
    
    public void closeConnection(Connection conn) {
    	try {
    		stmt.close();
    		conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void closeConnection() {
    	try {
    		conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public Table executeStringForTable(String sql) throws SQLException {
  		ResultSet results = stmt.executeQuery(sql);
  		System.out.println("Query Made");
  		System.out.println(sql);
  		Table table =	Table.read().db(results);
    	results.close();
    	return(table);
    	
    }

    
    
    
    public ArrayList<Table> executeStringForTableMultiple(String sql, int howMany) throws SQLException {
  		System.out.println("Query Made");
  		System.out.println(sql);
  		ArrayList<Table> frames = new ArrayList<Table>();
  		for(int counter = 0; counter < howMany; counter++) {
  			
  	  		ResultSet results = stmt.executeQuery(sql);
  	  		Table table =	Table.read().db(results);
        	results.close();
  			frames.add(table);
  		}
    	return(frames);
    	
    }
    
    public void executeStatement(String sql) throws SQLException {
		
  		System.out.println("Execute Update Statement: " + sql); 
		stmt.executeUpdate(sql + ";");
	    System.out.println("Created table in given database...");

	
    }
    
    
    public Table executeTempTableStringMergent(String queryString, ArrayList<String> tempTableQueries) throws SQLException {
        	String tempTableName = tempTableQueries.get(0);

        	int posWhere = queryString.indexOf("WHERE");
        	if(posWhere == -1) {
                queryString += " inner join " + tempTableName + " on cusip9 = cusip_c;";
        	}
        	else {
                String afterWhere = queryString.substring(posWhere, queryString.length() - 1);
                String beforeWhere = queryString.substring(0, posWhere - 1);
                queryString = beforeWhere + " inner join " + tempTableName + " on cusip9 = cusip_c " + afterWhere + ";";
        	}
        	
        	System.out.println(queryString);
        	stmt.execute(tempTableQueries.get(1));
        	stmt.execute(tempTableQueries.get(2));
        	stmt.execute(tempTableQueries.get(3));
        	ResultSet results = stmt.executeQuery(queryString);
        	Table table = Table.read().db(results);
        	results.close();
          	return(table);
        	
    }
    
    
    
    public Table executeIssueTempTableStringMergent(String queryString, ArrayList<String> tempTableQueries) throws SQLException {
    	String tempTableName = tempTableQueries.get(0);

    	int posWhere = queryString.indexOf("WHERE");
    	if(posWhere == -1) {
            queryString += " inner join " + tempTableName + " on issue_id  = issue_id_i;";
    	}
    	else {
            String afterWhere = queryString.substring(posWhere, queryString.length() - 1);
            String beforeWhere = queryString.substring(0, posWhere - 1);
            queryString = beforeWhere + " inner join " + tempTableName + " on issue_id  = issue_id_i " + afterWhere + ";";
    	}
    	
    	System.out.println(queryString);
    	stmt.execute(tempTableQueries.get(1));
    	stmt.execute(tempTableQueries.get(2));
    	stmt.execute(tempTableQueries.get(3));
    	ResultSet results = stmt.executeQuery(queryString);
    	Table table = Table.read().db(results);
      	return(table);
    	
}

    
}
