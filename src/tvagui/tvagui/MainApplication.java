package tvagui;

/*
 * When exporting. Make sure the right swt.jar file is in the buildpath!
 * Otherwise, the code will not run!
 * Make sure, properties.txt is in our directory as well!
 */

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.SQLException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import com.bamcore.util.*;


public class MainApplication {
	//Hello
	protected Shell shell;
    public static void main(String[] args) throws NoSuchMethodException, SecurityException, MalformedURLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    	File f = new File(System.getProperty("java.class.path"));
    	File dir = f.getAbsoluteFile().getParentFile();
    	String path = dir.toString();
    	//First we read the properties.txt in our directory
    	//When you export, use the "path +..." line.
    	//When you test, use the one after.
    	BobUtil.loadDBPropertiesFromFile(path + "/properties.txt");
    	//BobUtil.loadDBPropertiesFromFile("properties.txt");

    	System.setProperty("dir", path);
        System.out.println("Working Directory = " + System.getProperty("dir"));
    	try {
    		MainApplication window = new MainApplication();
            window.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void open() throws SQLException {
    	

        Display display = Display.getDefault();
        createContents();
        shell.open();
        shell.layout();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
    }

    protected void createContents() throws SQLException {
        shell = new Shell();
        //shell.setSize(1500, 720);
        shell.setBounds(0, 0, 1500, 800);
 	   
        shell.setText("BAM TVA");
        shell.setLayout(new GridLayout(100, false));
        guiTVA tc = new guiTVA(shell, SWT.None);
        shell.open();
    }
    
    
    
}

