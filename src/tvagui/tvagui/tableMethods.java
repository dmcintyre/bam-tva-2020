package tvagui;


import static tech.tablesaw.aggregate.AggregateFunctions.sum;

//Table methods takes forms from gui.TVA to construct and execute sql queries


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.swing.JFileChooser;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.bamcore.util.BAMBOBException;
import com.bamcore.fixedincome.BondPrice;
import com.bamcore.util.*;
import com.bamcore.util.BD2;

import tech.tablesaw.api.DateColumn;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.columns.Column;
import tech.tablesaw.io.csv.CsvReadOptions;
import tech.tablesaw.io.csv.CsvWriteOptions;
import tech.tablesaw.selection.Selection;
import  tech.tablesaw.aggregate.AggregateFunctions.*;
import  tech.tablesaw.aggregate.*;


public class tableMethods {
	//Set up a connections instance
	sqlConnections s = new sqlConnections();
	
	File f = new File(System.getProperty("java.class.path"));
	File dir = f.getAbsoluteFile().getParentFile();
	String path = dir.toString();
	//String path = "~/Users/speter";
	//String CompiledSummaryFilePath = "CompiledSummary.csv";

	public tableMethods() throws SQLException {
		//Connect to the industry
		s.openConnection();
	}
	
	//These methods get the codes for States, Bond Insurance, Coupon, DebtType, AddlCredit, etc
	//The names is an arrayList. Codes are ArrayList of ArrayList of Strings
	public void getStateCodes(ArrayList<ArrayList<String>> stateCodes, ArrayList<String> stateNames) throws SQLException {

		String[] Southwest = new String[] { "AZ","AR","CO","KS","NM","OK","TX","UT"};
		String[] Southeast = new String[] { "AL", "FL", "GA","KY","LA","MS","NC","SC","TN","VA","WV"};
		String[] Northeast = new String[] { "CT", "DE", "DC","ME","MD","MA","NJ","NY","PA","RI","VT"};
		String[] Midwest = new String[] { "IL", "IN", "IA","MI","MN","MO","NE","ND","OH","SD","WI"};
		String[] Farwest = new String[] { "CA", "AK", "HI","ID","MT","NV","OR","WA","WY"};


		stateCodes.add(new ArrayList<String>(Arrays.asList(Northeast)));
		stateNames.add("Northeast");
		
		stateCodes.add(new ArrayList<String>(Arrays.asList(Southwest)));
		stateNames.add("Southwest");

		stateCodes.add(new ArrayList<String>(Arrays.asList(Southeast)));
		stateNames.add("Southeast");

		
		stateCodes.add(new ArrayList<String>(Arrays.asList(Midwest)));
		stateNames.add("Midwest");
		
		stateCodes.add(new ArrayList<String>(Arrays.asList(Farwest)));
		stateNames.add("Farwest");
		
		
		mergentQueries m = new mergentQueries();
		
		Table t = s.executeStringForTable(m.getStates());
		List<String> stateCode = t.stringColumn(0).asList();
		List<String> stateName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> stateCodeListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(stateCode)) {
			stateCodeListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		stateCodes.addAll(stateCodeListed);
		stateNames.addAll(new ArrayList<String>(stateName));		
		
		m = null;
		
	}
		
	public void getBondInsuranceCodes(ArrayList<ArrayList<String>> bondInsuranceCodes, ArrayList<String> bondInsuranceNames) throws SQLException {
		
		
		String[] BAM = new String[] { "BAM", "SBAM"};
		String[] AGM = new String[] { "AGM", "MAC", "ASGC", "SAGM","SAGMC","SAGMB", "SAMAG", "SAGC", "SMBAG", "SMAC", "SFGAC"};
		String[] SAG = new String[] {"SAGM","SAGMC","SAGMB", "SAMAG", "SAGC", "SMBAG", "SMAC", "SFGAC" };
		
		String[] none = new String[] {""};
		
		bondInsuranceCodes.add(new ArrayList<String>(Arrays.asList(BAM)));
		bondInsuranceNames.add("BAM ALL");
		
		bondInsuranceCodes.add(new ArrayList<String>(Arrays.asList(AGM)));
		bondInsuranceNames.add("AGM ALL");
		
		bondInsuranceCodes.add(new ArrayList<String>(Arrays.asList(new String[] {"SBAM"})));
		bondInsuranceNames.add("BAM Secondary");
		
		bondInsuranceCodes.add(new ArrayList<String>(Arrays.asList(SAG)));
		bondInsuranceNames.add("AGM Secondary ALL");

		bondInsuranceCodes.add(new ArrayList<String>(Arrays.asList(none)));
		bondInsuranceNames.add("No Insurance");

		
		mergentQueries m = new mergentQueries();
		Table t = s.executeStringForTable(m.getBondInsurance());
		List<String> bondInsuranceCode = t.stringColumn(0).asList();
		List<String> bondInsuranceName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> bondInsuranceCodeListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(bondInsuranceCode)) {
			bondInsuranceCodeListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		bondInsuranceCodes.addAll(bondInsuranceCodeListed);
		bondInsuranceNames.addAll(new ArrayList<String>(bondInsuranceName));		
		m = null;
		
	}
	
	public void getCouponCodes(ArrayList<ArrayList<String>> couponCodes, ArrayList<String> couponNames) throws SQLException {
		
		String[] STDUSE = new String[] {"ZER","OID","ODF","OIP","FXD"};
		couponCodes.add(new ArrayList<String>(Arrays.asList(STDUSE)));
		couponNames.add("Standard Use Codes");
		
		mergentQueries m = new mergentQueries();
		Table t = s.executeStringForTable(m.getCouponCode());
		List<String> couponCode = t.stringColumn(0).asList();
		List<String> couponName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> couponCodesListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(couponCode)) {
			couponCodesListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		couponCodes.addAll(couponCodesListed);
		couponNames.addAll(new ArrayList<String>(couponName));		
		m = null;
		
	}

	public void getDebtTypeCodes(ArrayList<ArrayList<String>> debtTypeCodes, ArrayList<String> debtTypeNames) throws SQLException {
		
		mergentQueries m = new mergentQueries();
		Table t = s.executeStringForTable(m.getDebtType());
		List<String> debtTypeCode = t.stringColumn(0).asList();
		List<String> debtTypeName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> debtTypeCodeListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(debtTypeCode)) {
			debtTypeCodeListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		debtTypeCodes.addAll(debtTypeCodeListed);
		debtTypeNames.addAll(new ArrayList<String>(debtTypeName));		
		m = null;
		
	}
	
	
	public void getAdditionalCreditCodes(ArrayList<ArrayList<String>> addCreditCodes, ArrayList<String> addCreditNames) throws SQLException {
		
		mergentQueries m = new mergentQueries();
		Table t = s.executeStringForTable(m.getAddlCredit());
		List<String> addCreditCode = t.stringColumn(0).asList();
		List<String> addCreditName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> addCreditCodeListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(addCreditCode)) {
			addCreditCodeListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		addCreditCodes.addAll(addCreditCodeListed);
		addCreditNames.addAll(new ArrayList<String>(addCreditName));		
		m = null;
		
	}
	
	
	public void getCapitalPurposeCodes(ArrayList<ArrayList<String>> capitalPurposeCodes, ArrayList<String> capitalPurposeNames) throws SQLException {
		
		mergentQueries m = new mergentQueries();
		Table t = s.executeStringForTable(m.getCapitalPurpose());
		List<String> capitalPurposeCode = t.stringColumn(0).asList();
		List<String> capitalPurposeName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> capitalPurposeCodeListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(capitalPurposeCode)) {
			capitalPurposeCodeListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		capitalPurposeCodes.addAll(capitalPurposeCodeListed);
		capitalPurposeNames.addAll(new ArrayList<String>(capitalPurposeName));		
		m = null;
		
	}

	
	public void getUseProceedsCodes(ArrayList<ArrayList<String>> useProceedsCodes, ArrayList<String> useProceedsNames) throws SQLException {
		
		mergentQueries m = new mergentQueries();

		Table t = s.executeStringForTable(m.getUseProceeds());
		List<String> useProceedsCode = t.stringColumn(0).asList();
		List<String> useProceedsName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> useProceedsCodeListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(useProceedsCode)) {
			useProceedsCodeListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		useProceedsCodes.addAll(useProceedsCodeListed);
		useProceedsNames.addAll(new ArrayList<String>(useProceedsName));		
		m = null;
		
	}
	
	
	public void getSPRatingCodes(ArrayList<ArrayList<String>> spRatingCodes, ArrayList<String> spRatingNames) throws SQLException {
		
		mergentQueries m = new mergentQueries();
		Table t = s.executeStringForTable(m.getSPLongRating());
		List<String> spRatingCode = t.stringColumn(0).asList();
		List<String> spRatingName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> spRatingCodeListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(spRatingCode)) {
			spRatingCodeListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		spRatingCodes.addAll(spRatingCodeListed);
		spRatingNames.addAll(new ArrayList<String>(spRatingName));		
		m = null;
		
	}

	
	public void getMoodyRatingCodes(ArrayList<ArrayList<String>> moodyRatingCodes, ArrayList<String> moodyRatingNames) throws SQLException {
		
		mergentQueries m = new mergentQueries();

		Table t = s.executeStringForTable(m.getMoodyLongRating());
		List<String> moodyRatingCode = t.stringColumn(0).asList();
		List<String> moodyRatingName = t.stringColumn(1).asList();
		
		ArrayList<ArrayList<String>> moodyRatingCodeListed = new ArrayList<ArrayList<String>>();
		for (String list : new ArrayList<String>(moodyRatingCode)) {
			moodyRatingCodeListed.add(new ArrayList<String>(Arrays.asList(list)));
		}

		
		moodyRatingCodes.addAll(moodyRatingCodeListed);
		moodyRatingNames.addAll(new ArrayList<String>(moodyRatingName));		
		m = null;
		
	}

	
	//This takes a regular bondInfoForm
	//Method is first: Get a list of issue_id, maturity_id, and cusips in a temp table. 
	//With this index, query all tables in the database to get info. 

	public LinkedHashMap<String, Table> bondInfoQuery(bondInfo bondInfoForm) throws SQLException, BAMBOBException, ParseException, InvalidFormatException, IOException {
		mergentQueries m = new mergentQueries();
		msrbQueries msrb = new msrbQueries();
		String tempTableName = m.genMergentTempTableName();
		
				
		System.out.println("TempTable Name: " + tempTableName);
		
		LinkedHashMap<String, Table>  tables = new LinkedHashMap<String, Table>();
		//If the query returns Issue_id_is and Maturity_id_i. Then continue to query each table to get the corresponding info for these ids.
		if(GrabIssueMaturityInfo(tables, bondInfoForm,m,msrb,s,tempTableName)) {
			
			tables = grabTables(tables, bondInfoForm,m,msrb,s,tempTableName);
			
			tables = getTrades(tables, bondInfoForm,m,msrb,s,tempTableName,false);

			System.out.println("Rating Check");
			System.out.println(bondInfoForm.SPRating.toString());
			System.out.println(bondInfoForm.MoodyRating.toString());

			
			if(bondInfoForm.SPRating.isEmpty() == false  || bondInfoForm.MoodyRating.isEmpty() == false) {
				boolean checkSP = (bondInfoForm.SPRating.isEmpty() == false);
		        boolean checkMoody = (bondInfoForm.MoodyRating.isEmpty() == false);
		        Table ratingFilter = tables.get("ratings");
		        //This is how you filter dataFrames in Java. Do a rowise check.
		        if(checkSP && checkMoody) {
		        	Selection sp = ratingFilter.stringColumn("SPUnderlying").isIn(bondInfoForm.SPRating);
		        	Selection moody = ratingFilter.stringColumn("MoodysUnderlying").isIn(bondInfoForm.MoodyRating);
		        	ratingFilter = ratingFilter.where(sp.or(moody));
		        	
		        }
		        else if(checkSP) {
		        	Selection sp = ratingFilter.stringColumn("SPUnderlying").isIn(bondInfoForm.SPRating);
		        	ratingFilter = ratingFilter.where(sp);
		        }
		        else {
		        	Selection moody = ratingFilter.stringColumn("MoodysUnderlying").isIn(bondInfoForm.MoodyRating);
		        	ratingFilter = ratingFilter.where(moody);
		        }
		        
		       
		        
		        if(!ratingFilter.isEmpty()) {
		        System.out.println("Ratings Return Some Cusips, Filtering Now");
		        tables.replace("ratings", ratingFilter);
		        String str = "ratings";
		        String bondinfo = "bondinfo";
		        String issueinfo = "issueinfo";
		        //Filter the tables by Cusips.
		        HashSet<String> ratingCusips = new HashSet<String>(ratingFilter.stringColumn(0).asList());
		        ArrayList<String> cusip6 = new ArrayList<String>();
		        for(String s: ratingCusips) {
		        	cusip6.add(s.substring(0,6));
		        }
		        
		        HashSet<String> ratingCusips6 = new HashSet<String>(cusip6);
		        cusip6 = null;
		        
		        for(String i:tables.keySet()) {
		        	if(i.equals(str) || tables.get(i) == null || tables.get(i).isEmpty()) {
		        		continue;
		        	}
		         	else if (i.equals(bondinfo)) {
		         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(2).isIn(ratingCusips))); 
		        	}
		         	else if (i.equals(issueinfo)) {
		         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(0).isIn(ratingCusips6))); 
		        	}

		        	else {
		         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(0).isIn(ratingCusips))); 
					

		        	}     	
		        }
	
			}
		        
		    System.out.println(ratingFilter);
			if(ratingFilter.isEmpty()) {
				tables = new LinkedHashMap<String, Table>();
				return(tables);
				
			}
			}
			
			System.out.println("MMD Passing:" + bondInfoForm.bondInfoTrading.getMMDTable());
			Table updateTrades = getYieldandMMD(tables,tables.get("trade_info"), bondInfoForm.bondInfoTrading.getMMDTable(), bondInfoForm.bondInfoTrading.isCalculateTradingYield());

			tables.replace("trade_info", updateTrades);

		}
		return(tables);
		
}
	
	
	//CheapTrade follows the same method as bondInfoQuery. There are further checks by rating and spread to MMD
	public LinkedHashMap<String, Table> cheapTradeQuery(cheapTrade cheapTradeForm) throws SQLException, BAMBOBException, ParseException, InvalidFormatException, IOException {
		mergentQueries m = new mergentQueries();
		msrbQueries msrb = new msrbQueries();
		String tempTableName = m.genMergentTempTableName();
		
				
		System.out.println("TempTable Name: " + tempTableName);
		
		LinkedHashMap<String, Table>  tables = new LinkedHashMap<String, Table>();
		if(GrabIssueMaturityInfo(tables, cheapTradeForm,m,msrb,s,tempTableName)) {
			tables = grabTables(tables, cheapTradeForm,m,msrb,s,tempTableName);
			tables = getTrades(tables, cheapTradeForm,m,msrb,s,tempTableName,false);

			System.out.println("Rating Check");
			System.out.println(cheapTradeForm.SPRating.toString());
			System.out.println(cheapTradeForm.MoodyRating.toString());

			
			if(cheapTradeForm.SPRating.isEmpty() == false  || cheapTradeForm.MoodyRating.isEmpty() == false) {
				boolean checkSP = (cheapTradeForm.SPRating.isEmpty() == false);
		        boolean checkMoody = (cheapTradeForm.MoodyRating.isEmpty() == false);
		        Table ratingFilter = tables.get("ratings");
		        //This is how you filter dataFrames in Java. Do a rowise check.
		        if(checkSP && checkMoody) {
		        	Selection sp = ratingFilter.stringColumn("SPUnderlying").isIn(cheapTradeForm.SPRating);
		        	Selection moody = ratingFilter.stringColumn("MoodysUnderlying").isIn(cheapTradeForm.MoodyRating);
		        	ratingFilter = ratingFilter.where(sp.or(moody));
		        	
		        }
		        else if(checkSP) {
		        	Selection sp = ratingFilter.stringColumn("SPUnderlying").isIn(cheapTradeForm.SPRating);
		        	ratingFilter = ratingFilter.where(sp);
		        }
		        else {
		        	Selection moody = ratingFilter.stringColumn("MoodysUnderlying").isIn(cheapTradeForm.MoodyRating);
		        	ratingFilter = ratingFilter.where(moody);
		        }
		        
		       
		        
		        if(!ratingFilter.isEmpty()) {
		        System.out.println("Ratings Return Some Cusips, Filtering Now");
		        tables.replace("ratings", ratingFilter);
		        String str = "ratings";
		        String bondinfo = "bondinfo";
		        String issueinfo = "issueinfo";
		        //Filter the tables by Cusips.
		        HashSet<String> ratingCusips = new HashSet<String>(ratingFilter.stringColumn(0).asList());
		        ArrayList<String> cusip6 = new ArrayList<String>();
		        for(String s: ratingCusips) {
		        	cusip6.add(s.substring(0,6));
		        }
		        
		        HashSet<String> ratingCusips6 = new HashSet<String>(cusip6);
		        cusip6 = null;
		        
		        for(String i:tables.keySet()) {
		        	if(i.equals(str) || tables.get(i) == null || tables.get(i).isEmpty()) {
		        		continue;
		        	}
		         	else if (i.equals(bondinfo)) {
		         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(2).isIn(ratingCusips))); 
		        	}
		         	else if (i.equals(issueinfo)) {
		         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(0).isIn(ratingCusips6))); 
		        	}

		        	else {
		         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(0).isIn(ratingCusips))); 
					

		        	}     	
		        }
	
			}
		        
		    System.out.println(ratingFilter);
			if(ratingFilter.isEmpty()) {
				tables = new LinkedHashMap<String, Table>();
				return(tables);
				
			}
			}
			
		
			
			
			
			
			//Similiar to the Ratings Check.
			System.out.println("MMD Passing:" + cheapTradeForm.bondInfoTrading.getMMDTable());
			//Get Yield and MMD here.
			Table updateTrades = getYieldandMMD(tables,tables.get("trade_info"), cheapTradeForm.bondInfoTrading.getMMDTable(), cheapTradeForm.bondInfoTrading.isCalculateTradingYield());
			
			
			boolean containsMMD = updateTrades.columnNames().contains("Trade Date MMD");
			System.out.println("Contains MMD: " + containsMMD);
			System.out.println(updateTrades);
			
			if( containsMMD && (cheapTradeForm.getMMDSpread() != null && cheapTradeForm.getMMDSpread().isEmpty() == false) ) {
				System.out.println("Filtering by MMD Spread");
				String min = cheapTradeForm.getMMDSpread().get(0);
				String max = cheapTradeForm.getMMDSpread().get(1);
				System.out.println(min);
				System.out.println(max);
				
				System.out.println(updateTrades.columnNames());
				String str = "Trade Yield Spread to MMD";
				if(updateTrades.columnNames().contains("Trade YTW Spread to MMD")) {
					str = "Trade YTW Spread to MMD";
					System.out.println("Filtering by YTW");
				}
				
				String empty = "";
				System.out.println(updateTrades.columnNames().indexOf(str));

				//13 is the column with Trade Yield Spread to MMD
				int spread = updateTrades.columnNames().indexOf(str);
				System.out.println("Spread Index: " + spread);

				if(min.equals(empty)) {
					double maxDouble = Double.parseDouble(max);
					Selection LessThan = updateTrades.doubleColumn(13).isLessThanOrEqualTo(maxDouble);
					updateTrades = updateTrades.where(LessThan);
					System.out.println("New Table");
					System.out.println(updateTrades);
					
				}
				else if(max.equals(empty)) {
					double minDouble = Double.parseDouble(min);
					Selection greaterThan = updateTrades.doubleColumn(13).isGreaterThanOrEqualTo(minDouble);
					updateTrades = updateTrades.where(greaterThan);
					System.out.println("New Table");
					System.out.println(updateTrades);


					
				}				
				else {
					
					double minDouble = Double.parseDouble(min);
					double maxDouble = Double.parseDouble(max);
					Selection LessThan = updateTrades.doubleColumn(13).isLessThanOrEqualTo(maxDouble);
					Selection greaterThan = updateTrades.doubleColumn(13).isGreaterThanOrEqualTo(minDouble);
					updateTrades = updateTrades.where(LessThan.and(greaterThan));
					System.out.println("New Table");
					System.out.println(updateTrades);

				}
				
				 
			        if(!updateTrades.isEmpty()) {
						HashSet<String> spreadCusips = new HashSet<String>(updateTrades.stringColumn("cusip").asList());

						
				        ArrayList<String> cusip6Spread = new ArrayList<String>();
				        for(String s: spreadCusips) {
				        	cusip6Spread.add(s.substring(0,6));
				        }
				        
				        HashSet<String> spreadCusips6 = new HashSet<String>(cusip6Spread);
				        cusip6Spread = null;
				        
				        
				        System.out.println("Filtering Cusips From Spread Comparison");
				        tables.replace("trade_info", updateTrades);
				        System.out.println(updateTrades);
				        String trade_info = "trade_info";
				        String bondinfo = "bondinfo";
				        String issueinfo = "issueinfo";

				        for(String i:tables.keySet()) {
				        	System.out.println(i);
		         			System.out.println(tables.get(i));
				        	if(i.equals(trade_info) || tables.get(i) == null || tables.get(i).isEmpty()) {
				        		continue;
				        	}
				        	
				        	
				         	else if (i.equals(bondinfo)) {
				         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(2).isIn(spreadCusips))); 

				         			System.out.println(tables.get(i));
				        	}
				         	else if (i.equals(issueinfo)) {
				         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(0).isIn(spreadCusips6))); 
			         			System.out.println(tables.get(i));

				        	}

				        	else {
				         		tables.replace(i, tables.get(i).where(tables.get(i).stringColumn(0).isIn(spreadCusips))); 
			         				System.out.println(tables.get(i));

				        	}     	
				        }
			
					}
				 
				 
				 
			    	if(updateTrades.isEmpty()) {
						tables = new LinkedHashMap<String, Table>();
						return(tables);
						
					}
			}
				  
			tables.replace("trade_info", updateTrades);
		}
		return(tables);
		
}

	
	public LinkedHashMap<String, Table> QuickbondInfoQuery(quickBond quickBondForm) throws SQLException, BAMBOBException, ParseException, InvalidFormatException, IOException {
		mergentQueries m = new mergentQueries();
		msrbQueries msrb = new msrbQueries();
		String tempTableName = m.genMergentTempTableName();
		LinkedHashMap<String, Table>  tables = new LinkedHashMap<String, Table>();

		if(QuickGrabIssueMaturityInfo(tables, quickBondForm,m,msrb,s,tempTableName)) {
			tables = grabTables(tables,quickBondForm,m,msrb,s,tempTableName);
			tables = getTrades(tables,quickBondForm,m,msrb,s,tempTableName,false);

			Table updateTrades = getYieldandMMD(tables,tables.get("trade_info"), quickBondForm.bondInfoTrading.getMMDTable(), quickBondForm.bondInfoTrading.isCalculateTradingYield());
			tables.replace("trade_info", updateTrades);

		}
		
		return(tables);
	}
	
	
	public LinkedHashMap<String, Table> TCVInfoQuery(tradingCusipVisualiser quickBondForm) throws SQLException, BAMBOBException, ParseException, InvalidFormatException, IOException {
		mergentQueries m = new mergentQueries();
		msrbQueries msrb = new msrbQueries();
		String tempTableName = m.genMergentTempTableName();
		LinkedHashMap<String, Table>  tables = new LinkedHashMap<String, Table>();

		if(QuickGrabIssueMaturityInfo(tables, quickBondForm,m,msrb,s,tempTableName)) {
			tables = grabTables(tables,quickBondForm,m,msrb,s,tempTableName);
			tables = getTrades(tables,quickBondForm,m,msrb,s,tempTableName,true);

			Table updateTrades = getYieldandMMD(tables,tables.get("trade_info"), quickBondForm.bondInfoTrading.getMMDTable(), true);

			if(updateTrades == null) {
				return(tables);
			}
			tables.replace("trade_info", updateTrades);
			
			boolean mmd = updateTrades.columnNames().contains("Trade Date MMD");
			System.out.println("Transferred MMD to Summary Tables:" + mmd);
			Table t = tables.get("TradingHistorySummarybyDate");
			t = t.addColumns(updateTrades.doubleColumn("trade_ytw"));
			if(mmd) {
				t.addColumns(updateTrades.doubleColumn("Trade Date MMD"));
			}
			tables.replace("TradingHistorySummarybyDate", t);
			
			t = tables.get("TradingHistorySummary");
			t = t.addColumns(updateTrades.doubleColumn("trade_ytw"));
			if(mmd) {
				t.addColumns(updateTrades.doubleColumn("Trade Date MMD"));
			}
			tables.replace("TradingHistorySummary", t);
		
			
			
			tables = getCusipWeightedStats(tables);
			
		}
		return(tables);

	}

	public boolean QuickGrabIssueMaturityInfo(LinkedHashMap<String, Table>  tables , quickBond bondInfoForm, mergentQueries m, msrbQueries msrb, sqlConnections s, String tempTablename ) throws SQLException {
		boolean firstFilter = true;
		boolean isCreated = false;
		//No info, returns false. Stops the query
		if(bondInfoForm.getBondInfoDictionary().isEmpty() && bondInfoForm.bondInfoTrading.getTradeInfoDictionary().isEmpty()) {
			return(false);
		}
		//Having Active Maturities on only is not sufficient info. 
		if( bondInfoForm.getBondInfoDictionary().keySet().contains("active_maturity_flag_i") && bondInfoForm.getBondInfoDictionary().keySet().size() == 1 && bondInfoForm.bondInfoTrading.getTradeInfoDictionary().isEmpty()) {
			return(false);
		}
		//If theres trading filters. Its faster to get trading info first and then filter.
		System.out.println("Quick Bond Grab Issue Maturity IDs");
		if(bondInfoForm.getBondInfoDictionary().keySet().contains("active_maturity_flag_i") && bondInfoForm.getBondInfoDictionary().keySet().size() == 1) {
			
			if( (bondInfoForm.bondInfoTrading.getTradeInfoDictionary().isEmpty() == false) & bondInfoForm.bondInfoTrading.isCusipFilter()) {
				String queryString  = msrb.getMSRBData(bondInfoForm.bondInfoTrading.getTradeInfoDictionary(), false);
				System.out.println("Trading String");
				if(firstFilter) {
					firstFilter = false;
					String tempTableCreate = msrb.getIssueMaturityFromCusip(queryString,tempTablename);
					System.out.println(tempTableCreate);
					s.executeStatement(tempTableCreate);
					tables.put("trade_info", s.executeStringForTable(queryString));
					isCreated = true;

				}
				
			}
			
		}
		else {
		if(bondInfoForm.getBondInfoDictionary().keySet().isEmpty() == false) {
			String queryString = m.getQuickBondInfoQuery(bondInfoForm.getBondInfoDictionary(),  false);	
			
			ArrayList<String> tempTableCreate = m.genIssueMaturityTempTableFromSql(tempTablename, queryString, "bondinfo");
			System.out.println("Bond Info String");
			System.out.println(tempTableCreate.toString());
			if(firstFilter) {
			firstFilter =  false;
			for(String i: tempTableCreate) {
				s.executeStatement(i);
			}
			System.out.println("Temp Table Created");
			isCreated = true;
			}
			
			else {
				ArrayList<String> filterTempTable = m.filterTempTable(queryString, tempTablename, "bondinfo" );
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);
				}
				isCreated = true;
			}
		}
		System.out.println("Trading String");
		System.out.println(bondInfoForm.bondInfoTrading.getTradeInfoDictionary());
		
		if( (bondInfoForm.bondInfoTrading.getTradeInfoDictionary().isEmpty() == false) & bondInfoForm.bondInfoTrading.isCusipFilter()) {
			String queryString  = msrb.getMSRBData(bondInfoForm.bondInfoTrading.getTradeInfoDictionary(), false);
			System.out.println("Trading String");

			
			if(firstFilter) {
				firstFilter =  false;
				String tempTableCreate = msrb.getIssueMaturityFromCusip(queryString,tempTablename);
				System.out.println(tempTableCreate);
				s.executeStatement(tempTableCreate);
				tables.put("trade_info", s.executeStringForTable(queryString));
				isCreated = true;

			
			}
			else {
				ArrayList<String>  filterTempTable = msrb.genCusipFilter(queryString,tempTablename);
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);
				}
				isCreated = true;
			}
			
		}
		}

		return(isCreated);

		
	}	
	
	public boolean GrabIssueMaturityInfo(LinkedHashMap<String, Table> tables, bondInfo bondInfoForm, mergentQueries m, msrbQueries msrb, sqlConnections s, String tempTablename ) throws SQLException {
		//isCreated refers to if the issue maturity id is created. This is what we return
		//First filter asks if the there were previous filters. Previous filters require updated the temp table
		boolean firstFilter = true;
		boolean isCreated = false;
		//Return Empty if the form is Empty
		if(bondInfoForm.getBondInfoDictionary().isEmpty() && bondInfoForm.getCallInfoDictionary().isEmpty() && bondInfoForm.getIssueInfoDictionary().isEmpty() && bondInfoForm.getCallInfoDictionary().isEmpty() && bondInfoForm.getSinkFundDictionary().isEmpty() && bondInfoForm.getAddlCreditDictionary().isEmpty() && bondInfoForm.getRedemptnDictionary().isEmpty() && bondInfoForm.getPartRdemptnDictionary().isEmpty() && bondInfoForm.getVarRateDictionary().isEmpty() && bondInfoForm.bondInfoTrading.getTradeInfoDictionary().isEmpty()) {
			return(false);
		}
		
		//Return empty if active maturity flag is the only query.
		if(bondInfoForm.getBondInfoDictionary().keySet().contains("active_maturity_flag_i") && bondInfoForm.getBondInfoDictionary().keySet().size() == 1 && bondInfoForm.getCallInfoDictionary().isEmpty() && bondInfoForm.getAddlCreditDictionary().isEmpty() && bondInfoForm.getIssueInfoDictionary().isEmpty() && bondInfoForm.getSinkFundDictionary().isEmpty() & bondInfoForm.getSinkFundDictionary().isEmpty() & bondInfoForm.getRedemptnDictionary().isEmpty() && bondInfoForm.getPartRdemptnDictionary().isEmpty() && bondInfoForm.getVarRateDictionary().isEmpty() && bondInfoForm.bondInfoTrading.getTradeInfoDictionary().isEmpty()) {
			return(false);
		}
		//If there is a trade range specified, querying msrb is faster than going through mergent.
		if( (bondInfoForm.bondInfoTrading.getTradeInfoDictionary().isEmpty() == false) & bondInfoForm.bondInfoTrading.isCusipFilter() && bondInfoForm.bondInfoTrading.getTradeDate().isEmpty() == false) {
			String queryString  = msrb.getMSRBData(bondInfoForm.bondInfoTrading.getTradeInfoDictionary(), false);
			System.out.println("Trading String");
			if(firstFilter) {
				firstFilter = false;
				String tempTableCreate = msrb.getIssueMaturityFromCusip(queryString,tempTablename);
				System.out.println(tempTableCreate);
				s.executeStatement(tempTableCreate);
				tables.put("trade_info", s.executeStringForTable(queryString));
				isCreated = true;
			}
			
			
			
			
			
		}
		
		if(bondInfoForm.getBondInfoDictionary().isEmpty() == false | bondInfoForm.getIssueInfoDictionary().isEmpty() == false) {
			String queryString = m.getExtendedBondInfoQuery(bondInfoForm.getBondInfoDictionary(),bondInfoForm.getIssueInfoDictionary(),  true);						
			ArrayList<String> tempTableCreate = m.genIssueMaturityTempTableFromSql(tempTablename, queryString, "bondinfo");
			firstFilter = false;
			System.out.println("Bond Info String");
			if(firstFilter) {
				firstFilter = false;
			for(String i: tempTableCreate) {
				System.out.println(i);
				s.executeStatement(i);
			}
			isCreated = true;
			}
			else {
				ArrayList<String> filterTempTable = m.filterTempTable(queryString, tempTablename, "bondinfo" );
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);
				}
				isCreated = true;
			}
			
		}
		
		if(bondInfoForm.getAddlCreditDictionary().isEmpty() == false ) {
			String queryString = m.getAddlCreditQuery(bondInfoForm.getAddlCreditDictionary(),  false);				
			System.out.println("Addl Credit Info Info String");

			if(firstFilter) {
				queryString = m.addLimit(queryString);				
				ArrayList<String> tempTableCreate = m.genIssueMaturityTempTableFromSql(tempTablename, queryString, "addlcred");
				
				firstFilter = false;
				System.out.println(tempTableCreate);

				for(String i: tempTableCreate) {
					System.out.println(i);
					s.executeStatement(i);
				}		
				isCreated = true;

			}
			else {
				ArrayList<String> filterTempTable = m.filterTempTable(queryString, tempTablename, "addlcred" );
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);
				}
				isCreated = true;

				
			}
			

		}
		
		if(bondInfoForm.getCallInfoDictionary().isEmpty() == false ) {
			String queryString = m.getCallScheduleQuery(bondInfoForm.getCallInfoDictionary(),  false);				
			System.out.println("Call Info Info String");

			if(firstFilter) {
				queryString = m.addLimit(queryString);				
				ArrayList<String> tempTableCreate = m.genIssueMaturityTempTableFromSql(tempTablename, queryString, "callschd");
				
				firstFilter = false;
				System.out.println(tempTableCreate);

				for(String i: tempTableCreate) {
					System.out.println(i);
					s.executeStatement(i);
				}		
				isCreated = true;

			}
			else {
				ArrayList<String> filterTempTable = m.filterTempTable(queryString, tempTablename, "callschd" );
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);
				}
				isCreated = true;

				
			}
			

		}
		if(bondInfoForm.getSinkFundDictionary().isEmpty() == false ) {
			String queryString = m.getSinkFundData(bondInfoForm.getSinkFundDictionary(),  false);				
			System.out.println("Sink Fund String");

			if(firstFilter) {
				queryString = m.addLimit(queryString);				
				queryString = m.addLimit(queryString);				
				ArrayList<String> tempTableCreate = m.genIssueMaturityTempTableFromSql(tempTablename, queryString, "sinkfund");
				firstFilter = false;
				for(String i: tempTableCreate) {
					System.out.println(i);
					s.executeStatement(i);
				}				

				isCreated = true;

			}
			else {
				ArrayList<String> filterTempTable = m.filterTempTable(queryString, tempTablename, "sinkfund");
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);
				}
				isCreated = true;

			}
		}
		if(bondInfoForm.getRedemptnDictionary().isEmpty() == false ) {
			
			String queryString = m.getRedemptnData(bondInfoForm.getRedemptnDictionary(),  false);				
			System.out.println("Redemption Fund String");

			if(firstFilter) {
				queryString = m.addLimit(queryString);				
				ArrayList<String> tempTableCreate = m.genIssueMaturityTempTableFromSql(tempTablename, queryString, "redemptn");
				firstFilter = false;
				System.out.println(tempTableCreate);
				for(String i: tempTableCreate) {
					System.out.println(i);
					s.executeStatement(i);
				}	
				isCreated = true;

			}
			else {
				ArrayList<String> filterTempTable = m.filterTempTable(queryString, tempTablename, "redemptn");
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);

				}
				isCreated = true;

			}
			
			
		}
		
		if(bondInfoForm.getPartRdemptnDictionary().isEmpty() == false ) {
			String queryString = m.getPartRedemptnData(bondInfoForm.getPartRdemptnDictionary(),  false);				
			System.out.println("Partial Redemption Fund String");

			if(firstFilter) {
				queryString = m.addLimit(queryString);				
				ArrayList<String> tempTableCreate = m.genIssueMaturityTempTableFromSql(tempTablename, queryString, "partredm");
				firstFilter = false;
				System.out.println(tempTableCreate);
				for(String i: tempTableCreate) {
					System.out.println(i);
					s.executeStatement(i);
				}	
				isCreated = true;

			}
			else {
				ArrayList<String> filterTempTable = m.filterTempTable(queryString, tempTablename,  "partredm");
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);
				}
				isCreated = true;

			}
			
			
		}
		if(bondInfoForm.getVarRateDictionary().isEmpty( )== false ) {
			
			String queryString = m.getVarRateData(bondInfoForm.getVarRateDictionary(),  false);				
			System.out.println("Var Rate Fund String");

			if(firstFilter) {
				queryString = m.addLimit(queryString);				
				ArrayList<String> tempTableCreate = m.genIssueMaturityTempTableFromSql(tempTablename, queryString, "varrate");
				firstFilter = false;
				System.out.println(tempTableCreate);
				for(String i: tempTableCreate) {
					System.out.println(i);
					s.executeStatement(i);
				}	
				isCreated = true;

			}
			else {
				ArrayList<String> filterTempTable = m.filterTempTable(queryString, tempTablename, "varrate");
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);

				}
				isCreated = true;

			}
			
			
		}

		if((bondInfoForm.bondInfoTrading.getTradeInfoDictionary().isEmpty() == false) & bondInfoForm.bondInfoTrading.isCusipFilter() ) {
			System.out.println("Trading String");
			System.out.println(bondInfoForm.bondInfoTrading.getTradeInfoDictionary().keySet().toString());
			System.out.println(bondInfoForm.bondInfoTrading.getTradeInfoDictionary().values().toString());
			String queryString  = msrb.getMSRBData(bondInfoForm.bondInfoTrading.getTradeInfoDictionary(), true);
			
			System.out.println(queryString);
			System.out.println(bondInfoForm.bondInfoTrading.getTradeInfoDictionary().keySet().toString());
			System.out.println(bondInfoForm.bondInfoTrading.getTradeInfoDictionary().values().toString());
			
			
			if(firstFilter) {
				String tempTableCreate = msrb.getIssueMaturityFromCusip(queryString,tempTablename);
				System.out.println(tempTableCreate);
				s.executeStatement(tempTableCreate);
				tables.put("trade_info", s.executeStringForTable(queryString));
				isCreated = true;

			
			}
			else {
				ArrayList<String>  filterTempTable = msrb.genCusipFilter(queryString,tempTablename);
				for(String i: filterTempTable) {
					System.out.println(i);
					s.executeStatement(i);
					isCreated = true;
				}
			
			}
			
			
			
		}

		return(isCreated);

		
	}

	public LinkedHashMap<String, Table>  grabTables(LinkedHashMap<String, Table>  tables, bondInfo bondInfoForm,mergentQueries m, msrbQueries msrb, sqlConnections s, String tempTableName) throws SQLException, BAMBOBException, ParseException, InvalidFormatException, IOException {
		String queryString = m.getBondInfoQuery(bondInfoForm.getBondInfoDictionary(), false);						
		System.out.println(m.genIssueMaturityFilterTable(queryString,tempTableName,"bondinfo"));
		tables.put("bondinfo",s.executeStringForTable(m.genIssueMaturityFilterTable(queryString,tempTableName,"bondinfo")));
		System.out.println(tables.get("bondinfo"));

		
		
		queryString = m.getIssueInfoQuery(bondInfoForm.getIssueInfoDictionary(), false);						
		System.out.println(m.genIssueFilterTable(queryString,tempTableName,"issueinfo"));	
		tables.put("issueinfo",s.executeStringForTable(m.genIssueFilterTable(queryString,tempTableName,"issueinfo")));
		System.out.println(tables.get("issueinfo"));

		queryString = m.getCallScheduleQuery(bondInfoForm.getCallInfoDictionary(), false);						
		System.out.println(m.genIssueMaturityFilterTable(queryString,tempTableName,"callschd"));
		tables.put("callschd",s.executeStringForTable(m.genIssueMaturityFilterTable(queryString,tempTableName,"callschd")));
		System.out.println(tables.get("callschd"));

		
		queryString = m.getSinkFundData(bondInfoForm.getSinkFundDictionary(), false);						
		System.out.println(m.genIssueMaturityFilterTable(queryString,tempTableName,"sinkfund"));
		tables.put("sinkfund",s.executeStringForTable(m.genIssueMaturityFilterTable(queryString,tempTableName,"sinkfund")));
		System.out.println(tables.get("sinkfund"));

		
		queryString = m.getRedemptnData(bondInfoForm.getRedemptnDictionary(), false);						
		System.out.println(m.genIssueMaturityFilterTable(queryString,tempTableName,"redemptn"));
		tables.put("redemptn",s.executeStringForTable(m.genIssueMaturityFilterTable(queryString,tempTableName,"redemptn")));
		System.out.println(tables.get("redemptn"));

		
		queryString = m.getPartRedemptnData(bondInfoForm.getPartRdemptnDictionary(), false);						
		System.out.print(m.genIssueMaturityFilterTable(queryString,tempTableName,"partredm"));
		tables.put("partredemptn",s.executeStringForTable(m.genIssueMaturityFilterTable(queryString,tempTableName,"partredm")));
		System.out.println(tables.get("partredemptn"));

		
		queryString = m.getVarRateData(bondInfoForm.getVarRateDictionary(),  false);				
		System.out.println(m.genIssueMaturityFilterTable(queryString,tempTableName,"varrate"));
		tables.put("varrate",s.executeStringForTable(m.genIssueMaturityFilterTable(queryString,tempTableName,"varrate")));
		System.out.println(tables.get("varrate"));

		
		queryString = m.getAddlCreditQuery(bondInfoForm.getAddlCreditDictionary(),  false);				
		System.out.println(m.genIssueMaturityFilterTable(queryString,tempTableName,"addlcred"));
		tables.put("addlcred",s.executeStringForTable(m.genIssueMaturityFilterTable(queryString,tempTableName,"addlcred")));
		System.out.println(tables.get("addlcred"));
		
		
		System.out.println("Getting Trades");
		System.out.println(bondInfoForm.bondInfoTrading.isTradingChecks());
	

		System.out.println("RATINGS DATA");

		String cusip = "cusip_c";
		List cusips = tables.get("bondinfo").stringColumn(cusip).asList();
		ArrayList<String> cusipArrayList = new ArrayList<String>(cusips);
		JSONArray jsArray = new JSONArray();
		jsArray.addAll(cusipArrayList);

		ResultSet ratingTable = null;
		ratingTable = com.bamcore.secmaster.SecMaster.bulkLookupByCusipRaw(s.getConnection(), jsArray);	
        //com.opencsv.CSVWriter writer = new com.opencsv.CSVWriter(new FileWriter(CompiledSummaryFilePath));
       // writer.writeAll(ratingTable, true);
       //writer.close();
        
        
        ArrayList<String> ratingCusipArrayList = new ArrayList<String>();
        ArrayList<String> priorcusip = new ArrayList<String>();
        ArrayList<Double> coupon = new ArrayList<Double>();
        ArrayList<String> couponcode = new ArrayList<String>();
        ArrayList<String> matdate = new ArrayList<String>();
        ArrayList<String> settlementdate = new ArrayList<String>();
        ArrayList<String> firstcoupon = new ArrayList<String>();
        ArrayList<String> bondinsurance = new ArrayList<String>();
        ArrayList<String> issuerlongname = new ArrayList<String>();
        ArrayList<String> state = new ArrayList<String>();
        ArrayList<String> securitycode = new ArrayList<String>();
        ArrayList<String> spur = new ArrayList<String>();
        ArrayList<String> spurDate = new ArrayList<String>();
        ArrayList<String> spurOut = new ArrayList<String>();
        ArrayList<String> spurOutDate = new ArrayList<String>();
        ArrayList<String> STDLong = new ArrayList<String>();
        ArrayList<String> STDLongDate = new ArrayList<String>();
        ArrayList<String> STDLongOutlook = new ArrayList<String>();
        ArrayList<String> STDLongOutDate = new ArrayList<String>();
        ArrayList<String> School = new ArrayList<String>();
        ArrayList<String> SchoolOutlook = new ArrayList<String>();
        ArrayList<String> SchoolDate = new ArrayList<String>();
        ArrayList<String> SchoolOutlookDate = new ArrayList<String>();
        ArrayList<String> SPUnderlying = new ArrayList<String>();
        ArrayList<String> ULR = new ArrayList<String>();
        ArrayList<String> ULRDate = new ArrayList<String>();
        ArrayList<String> Enh = new ArrayList<String>();
        ArrayList<String> EnhDate = new ArrayList<String>();
        ArrayList<String> Ins = new ArrayList<String>();
        ArrayList<String> InsDate = new ArrayList<String>();

    	System.out.println("Filling in Columns");

        while(ratingTable.next()) {
        	
        	ratingCusipArrayList.add(ratingTable.getString("cusip"));        	
        	priorcusip.add(ratingTable.getString("prior_cusip_c"));
        	coupon.add(ratingTable.getDouble("coupon_f"));
        	couponcode.add(ratingTable.getString("coupon_code_c"));
        	matdate.add(ratingTable.getString("maturity_date_d"));
        	settlementdate.add(ratingTable.getString("settlement_date_d"));
        	firstcoupon.add(ratingTable.getString("first_coupon_date_d"));
        	bondinsurance.add(ratingTable.getString("bond_insurance_code_c"));
        	issuerlongname.add(ratingTable.getString("issuer_long_name_c"));
        	state.add(ratingTable.getString("state_c"));
        	securitycode.add(ratingTable.getString("SecurityCode"));
        	spur.add(ratingTable.getString("SPUR"));
        	spurOut.add(ratingTable.getString("SPURoutlook"));
        	spurDate.add(ratingTable.getString("SPURdate"));
        	spurOutDate.add(ratingTable.getString("SPURoutlookDate"));
        	STDLong.add(ratingTable.getString("STDLONG"));
        	STDLongOutlook.add(ratingTable.getString("STDLONGoutlook"));
        	STDLongDate.add(ratingTable.getString("STDLONGdate"));
        	STDLongOutDate.add(ratingTable.getString("STDLONGoutlookDate"));
        	School.add(ratingTable.getString("SCHOOL"));
        	SchoolOutlook.add(ratingTable.getString("SCHOOLoutlook"));
        	SchoolDate.add(ratingTable.getString("SCHOOLoutlook"));
        	SchoolOutlookDate.add(ratingTable.getString("SCHOOLoutlookDate"));
        	ULR.add(ratingTable.getString("ULR"));
        	Enh.add(ratingTable.getString("Enh"));
        	Ins.add(ratingTable.getString("Ins"));
        	ULRDate.add(ratingTable.getString("ULRDate"));
        	EnhDate.add(ratingTable.getString("EnhDate"));
        	InsDate.add(ratingTable.getString("InsDate"));
        	
        	String schoolRating = ratingTable.getString("SCHOOL"); 
        	String spurRating = ratingTable.getString("SPUR"); 
        	
        	if(schoolRating != null && !schoolRating.equals("") && !schoolRating.equals("NR")) {
        		SPUnderlying.add(schoolRating);
        	}
        	else {
        		SPUnderlying.add(spurRating);        		
        	}
        	
        }
        ratingTable.close();
        System.out.println("RatingSize");
        
        Table ratingsTable =  Table.create("ratingsFilter").addColumns(StringColumn.create("cusip_c", ratingCusipArrayList), StringColumn.create("prior_cusip_c",priorcusip));
        ratingsTable = ratingsTable.addColumns(DoubleColumn.create("coupon_f",coupon),StringColumn.create("coupon_code_c",couponcode), StringColumn.create("maturtity_date_d",matdate));
        ratingsTable = ratingsTable.addColumns(StringColumn.create("settlement_date_d",settlementdate),StringColumn.create("first_coupon_date_d",firstcoupon), StringColumn.create("bond_insurance_code_c",bondinsurance));
        ratingsTable = ratingsTable.addColumns(StringColumn.create("issuer_long_name_c",issuerlongname),StringColumn.create("state_c",state), StringColumn.create("SecurityCode",securitycode));
        ratingsTable = ratingsTable.addColumns(StringColumn.create("SPUnderlying",SPUnderlying),StringColumn.create("SPUR",spur), StringColumn.create("SPURoutlook",spurOut));
        ratingsTable = ratingsTable.addColumns(StringColumn.create("SPURDate",spurDate),StringColumn.create("SPURoutlookDate",spurOutDate), StringColumn.create("STDLong",STDLong));
        ratingsTable = ratingsTable.addColumns(StringColumn.create("STDLongOutlook",STDLongOutlook),StringColumn.create("STDLongDate",STDLongDate), StringColumn.create("STDLONGoutlookDate",STDLongOutDate));
        ratingsTable = ratingsTable.addColumns(StringColumn.create("SCHOOL",School),StringColumn.create("SCHOOLOutlook",SchoolOutlook), StringColumn.create("SchoolDate",SchoolDate));
        ratingsTable = ratingsTable.addColumns(StringColumn.create("SCHOOLoutlookDate",SchoolOutlookDate),StringColumn.create("MoodysUnderlying",ULR), StringColumn.create("Enh",Enh));
        ratingsTable = ratingsTable.addColumns(StringColumn.create("Ins",Ins),StringColumn.create("ULRDate",ULRDate), StringColumn.create("EnhDate",EnhDate),StringColumn.create("InsDate",InsDate));
		System.out.println(ratingsTable);
        tables.put("ratings",ratingsTable);
		return(tables);
	}
	
	
	public LinkedHashMap<String, Table> getTrades(LinkedHashMap<String, Table> tables, bondInfo bondInfoForm, mergentQueries m, msrbQueries msrb, sqlConnections s, String tempTableName, boolean TCVForm ) throws SQLException       {
	System.out.println(bondInfoForm.bondInfoTrading.isTradingChecks());
		
	if(bondInfoForm.bondInfoTrading.isTradingChecks()) {
		
		if(tables.containsKey("trade_info")) {
			System.out.println(tables.get("trade_info"));
			String queryString  = msrb.getMSRBData(bondInfoForm.bondInfoTrading.getTradeInfoDictionary(), false);
			if(TCVForm) {
				
				ArrayList<Table> tradeArray = s.executeStringForTableMultiple(queryString,2);
				tables.put("TradingHistorySummarybyDate", tradeArray.remove(0));
				tables.put("TradingHistorySummary", tradeArray.remove(0));
				
			}
			return(tables);
		}
		
		
		if(tables.containsKey("trade_info") == false) {
			String queryString  = msrb.getMSRBData(bondInfoForm.bondInfoTrading.getTradeInfoDictionary(), false);
			System.out.println(queryString);
			ArrayList<String> tradingInfoQueries = msrb.genCusipTableFromBondInfo(queryString, tempTableName);
			System.out.println(tradingInfoQueries.get(0));
			System.out.println(tradingInfoQueries.get(1));
			s.executeStatement(tradingInfoQueries.get(0));
			
			
			if(TCVForm) {
				
				ArrayList<Table> tradeArray = s.executeStringForTableMultiple(tradingInfoQueries.get(1),3);
				tables.put("trade_info", tradeArray.remove(0));
				tables.put("TradingHistorySummarybyDate", tradeArray.remove(0));
				tables.put("TradingHistorySummary", tradeArray.remove(0));
			}
			else {
				Table trade = s.executeStringForTable(tradingInfoQueries.get(1));
				
				if( (trade != null) && !trade.isEmpty() ) {
				  trade = trade.sortOn("trade_date");
				  tables.put("trade_info", trade);
				}
			}
			s.executeStatement(tradingInfoQueries.get(2));
			System.out.println(tables.get("trade_info"));
			}
			
		}
		return(tables);

	}
	
	

	
    public Table getYieldandMMD(LinkedHashMap<String, Table> tables, Table dataFrame, String MMDFileName, boolean calculateYield) throws InvalidFormatException, IOException {
		
    	String blank = "";
		System.out.println("Seeing Eligibility");
		if(dataFrame == null || dataFrame.isEmpty()) {
			System.out.println("No Trades Found");
			return(dataFrame);
		}
		System.out.println("Calculate Yield: " + calculateYield);
		System.out.println("Calculate MMD: " + !(MMDFileName == null || MMDFileName.equals(blank)));

		if( (calculateYield == false) && (MMDFileName == null || MMDFileName.equals(blank)) ) {
    		return(dataFrame);
    	}
		System.out.println("Eligible");
   		List<LocalDate> trade_date =  dataFrame.dateColumn("trade_date").asList();
   		List<LocalDate> maturity_date = dataFrame.dateColumn("maturity_date").asList();
		List<LocalDate> dated_date = dataFrame.dateColumn("dated_date").asList();

   		List<LocalDate> settle_date = dataFrame.dateColumn("settle_date").asList();
		List<Double> dollar_price = dataFrame.floatColumn("dollar_price").asDoubleColumn().asList();
		List<Double> coupon = dataFrame.floatColumn("coupon").asDoubleColumn().asList();
		List<String> cusipName = dataFrame.stringColumn("cusip").asList();


		
		System.out.println("Num Rows" + trade_date.size());
		//System.out.println(maturity_date.size());
		//System.out.println(settle_date.size());
		//System.out.println(dollar_price.size());
		//System.out.println(coupon.size());

		TreeSet<LocalDate> mmdDateIndex = null;
		TreeSet<Double> mmdColIndex = null;
		DoubleColumn trade_mmd = null;
		Table mmdTable = null;
		boolean mmdVerified = false;
		
		
		if((MMDFileName != null) && !MMDFileName.equals(blank)) {
				mmdTable = getMMDFrame(MMDFileName);
				mmdDateIndex = new TreeSet<LocalDate>(mmdTable.dateColumn("Date").asList());
		        mmdColIndex = new TreeSet<Double>();
		        List<String> mmdCols = mmdTable.columnNames();
		      
		        for (int counter = 1; counter < mmdCols.size() - 1; counter++) {
					 String str = mmdCols.get(counter);
					 mmdColIndex.add(Double.parseDouble(str));
				 }
				
		        trade_mmd = DoubleColumn.create("Trade Date MMD");
				System.out.println("MMD Upload Success");

				mmdVerified = true;
			
			

		}
		
		System.out.println("Checking to Calculate Yield");
		if(!calculateYield && !mmdVerified) {
			return(dataFrame);
		}

		double ytw = 0.0;
		List l = Collections.nCopies(trade_date.size(), ytw);
		ArrayList<Double> trade_ytwList = new ArrayList<Double>(l);
		l = null;
		
		
		ArrayList<Double> yield = new ArrayList<Double>(dataFrame.floatColumn("yield").asDoubleColumn().asList());
		DoubleColumn yieldToMMD = DoubleColumn.create("Trade Yield Spread to MMD");
		DoubleColumn tradeYieldToMMD = DoubleColumn.create("Trade YTW Spread to MMD");
		
		int size = trade_date.size();
		ZoneId defaultZoneId = ZoneId.systemDefault();
 
		for (int counter = 0; counter < size; counter++) {
		     //System.out.println(counter);
		     
			 Date tradeD = Date.from(trade_date.get(counter).atStartOfDay(defaultZoneId).toInstant());
			 LocalDateTime tradeDate = LocalDateTime.ofInstant(tradeD.toInstant(), ZoneId.systemDefault());
		     LocalDateTime matDate  = null;

			if(mmdVerified) {
				 if(maturity_date.get(counter) == null) {
					  trade_mmd.append(-1.0);
					  System.out.println("NULL");

				 }
				 
				 else {
				     Date matdateY = Date.from(maturity_date.get(counter).atStartOfDay(defaultZoneId).toInstant());
				     matDate = LocalDateTime.ofInstant(matdateY.toInstant(), ZoneId.systemDefault());
				     matdateY = null;

				     
					 double monthsBetween = ChronoUnit.MONTHS.between(YearMonth.from(tradeDate), YearMonth.from(matDate));
					 if(monthsBetween < 0) {
						 monthsBetween = 0.0;
					 }
					 
					 LocalDate tdate = tradeD.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

					 
					 LocalDate nearestDate = getDateNearest(mmdDateIndex,tdate);
					 double nearestCol = getNumNearest(mmdColIndex,monthsBetween);
					 int x  = mmdTable.columnNames().indexOf(Double.toString(nearestCol));
					 int y = mmdTable.dateColumn("Date").asList().indexOf(nearestDate);					 
					 Double mmd = mmdTable.doubleColumn(x).get(y);
					 trade_mmd.append(mmd);
				 }
				 	yieldToMMD.append(yield.get(counter) - trade_mmd.getDouble(counter));
				 
			 }
			
			
			
			 if(calculateYield) {
				 double trade_ytw = 0.0;

				 if( (maturity_date.get(counter) != null) && (settle_date.get(counter) != null) && (maturity_date.get(counter).compareTo(settle_date.get(counter)) > 0) && (dollar_price.get(counter) != null)  &&  (coupon.get(counter) != null)) {
			
				     Date matdate1 = Date.from(maturity_date.get(counter).atStartOfDay(defaultZoneId).toInstant());
				     Date settDate1 = Date.from(settle_date.get(counter).atStartOfDay(defaultZoneId).toInstant());
				     Date datedDate = Date.from(dated_date.get(counter).atStartOfDay(defaultZoneId).toInstant());

				     
				     		
					 BD2 matDateYield = new BD2(matdate1);
					 BD2 tradeDateYield = new BD2(tradeD);
					 BD2 settleDateYield = new BD2(settDate1);
					 BD2 datedDateYield = new BD2(datedDate);

					 BD2 nextCallDate = new BD2();
					 boolean hasCallDate = false;
					 
					 BondPrice dollarPriceYield = new BondPrice(dollar_price.get(counter));
					 double couponYield = coupon.get(counter)/100.0;

					 if(dollar_price.get(counter).doubleValue() > 100.0 ){ 
					 //Check for Call Date 
					 if(tables.containsKey("bondinfo")) {
						 String cusip = cusipName.get(counter);
						 DateColumn g = tables.get("bondinfo").dateColumn("next_call_date_d");
						 StringColumn cusipCol = tables.get("bondinfo").stringColumn("cusip_c");
						 Selection cusipMatch = cusipCol.isEqualTo(cusip);
						 
			
						 try {
							 LocalDate callDate = g.where(cusipMatch).get(0);
							 
							 if(callDate != null) {
								  nextCallDate  = new BD2(Date.from(callDate.atStartOfDay(defaultZoneId).toInstant()));
								  System.out.println("CallDate Detected: " + nextCallDate.toString());
									hasCallDate = true;
							 }
							 
						 }
						 catch (Exception e){
							 System.out.println("No Call Date Found, using maturity");
						 }
					 }
					 }
					 
					 
					/*  
					 System.out.println("DollarPrice:" +dollar_price.get(counter));
					 System.out.println("Coupon:" + couponYield);
					 System.out.println("mat:" + matDateYield.toString());
					 System.out.println("sett:" + settleDateYield.toString());
					 //System.out.println("trade:" + tradeDateYield.toString());
					  */ 
					 
					 try {
						 if(hasCallDate) {
							 trade_ytw = BondMath.YIELD(couponYield, datedDateYield, settleDateYield, nextCallDate, dollarPriceYield)*100.0;
						 }
						 else {
							 trade_ytw = BondMath.YIELD(couponYield, datedDateYield, settleDateYield, matDateYield, dollarPriceYield)*100.0;

						 }
						 if(counter % 100 == 0) {
							 System.out.println(counter + " of " + size + ": ");
						 }
						 trade_ytwList.set(counter,trade_ytw);
					 }
					 catch (Exception e) {
						 System.out.println("Errored Trying to Use Trade Date Instead of SettleDate");
						 try {
							 
							 trade_ytw = BondMath.YIELD(couponYield, datedDateYield, tradeDateYield, matDateYield, dollarPriceYield)*100.0;
							 if(counter % 100 == 0) {
								 System.out.println(counter + " of " + size + ": ");
							 }
							 trade_ytwList.set(counter,trade_ytw);
						 }
						 catch(Exception e1) {
							 trade_ytwList.set(counter,trade_ytw);
						 }
						 
					 }
					 
				 }
				 
				 
			 }
			 if(mmdVerified & calculateYield) {
				 tradeYieldToMMD.append(trade_ytwList.get(counter)- trade_mmd.get(counter));
			 }
			 

			 
			 
		 }
		 if(mmdVerified && trade_mmd.size() == trade_date.size()) {
			 dataFrame = dataFrame.addColumns(trade_mmd);
			 System.out.println("MMD Added");
			 dataFrame = dataFrame.addColumns(yieldToMMD);
		 }
		 
		 if(calculateYield) {
			 DoubleColumn g  = DoubleColumn.create("trade_ytw",trade_ytwList);
			 dataFrame = dataFrame.addColumns(g);
			 g = null;
			 System.out.println("YTW Added");

		 }
		 
		 if(mmdVerified && calculateYield) {
			 dataFrame = dataFrame.addColumns(tradeYieldToMMD);
		 }
		 
		 
		 
		 return(dataFrame);
    	
    }
    
    
    public LinkedHashMap<String, Table> getCusipWeightedStats( LinkedHashMap<String, Table> tradeMap) {
    	
    	for(String key : tradeMap.keySet()) {
			if(!key.contains("TradingHistory")) {
				continue;
			}
		boolean byDate = key.endsWith("byDate");
		Table dataFrame = tradeMap.get(key);

		Table p = Table.create("par_sum");
    	if(byDate) {
    		System.out.println("This is by Date");
    	  	p = p.addColumns(dataFrame.stringColumn("cusip"),dataFrame.dateColumn("trade_date"),dataFrame.longColumn("par_traded"));
	    	p = p.summarize("par_traded",sum).by("cusip","trade_date");
	    	System.out.println(p.columnNames());
	    	System.out.println(dataFrame);

	    	DoubleColumn g = DoubleColumn.create("total_par_traded");
            g.append(p.doubleColumn("Sum [par_traded]"));
            System.out.println(g);
            p = p.replaceColumn("Sum [par_traded]",g);
            g = null;
			
            System.out.println(p);
	    	System.out.println(dataFrame);

        }
        else {
    		System.out.println("This is by All Dates");
    	  	p = p.addColumns(dataFrame.stringColumn("cusip"),dataFrame.longColumn("par_traded"));
	    	p = p.summarize("par_traded",sum).by("cusip");
	    	System.out.println(p.columnNames());

	    	DoubleColumn g = DoubleColumn.create("total_par_traded");
            g.append(p.doubleColumn("Sum [par_traded]"));
            p = p.replaceColumn("Sum [par_traded]",g);      
            g = null;
            System.out.println(p);
        }

        DoubleColumn total_par = DoubleColumn.create("Total Par Traded");
        DoubleColumn cusipWeight = DoubleColumn.create("Cusip Weight");
        
        DoubleColumn cusipWeightTradeYield = DoubleColumn.create("Cusip Weighted Trade Yield");
        DoubleColumn cusipWeightTradeYTW =  DoubleColumn.create("Cusip Weighted Trade YTW");
        DoubleColumn cusipWeightAvgPrice =  DoubleColumn.create("Cusip Weighted Average Price");

        boolean hasMMD = dataFrame.columnNames().contains("Trade Date MMD");
  
        DoubleColumn cusipWeightTradeYieldSpread = DoubleColumn.create("Cusip Weighted Spread Yield");
        DoubleColumn cusipWeightTradeYTWSpread = DoubleColumn.create("Cusip Weighted Spread YTW");;
        DoubleColumn cusipWeightTradeMMD = DoubleColumn.create("Cusip Weighted Trade Date MMD");
        System.out.println("Working DataFrame");
        System.out.println(dataFrame);

		 for (int counter = 0; counter < dataFrame.rowCount(); counter++) {
			 
			 
			 tech.tablesaw.api.Row row  = dataFrame.row(counter);
			 if(byDate) {
				 DoubleColumn g = p.doubleColumn("total_par_traded");
				 Selection totalparCusip = p.stringColumn("cusip").isEqualTo(row.getString("cusip"));
				 Selection totalparDate = p.dateColumn("trade_date").isEqualTo(row.getDate("trade_date"));
				 total_par.append((Double)g.where(totalparCusip.and(totalparDate)).get(0));
			 }
			 else {
				 DoubleColumn g = p.doubleColumn("total_par_traded");
				 Selection totalparCusip = p.stringColumn("cusip").isEqualTo(row.getString("cusip"));
				 total_par.append((Double) g.where(totalparCusip).get(0));
			 }			
			 
			 Double l =  (double) row.getLong("par_traded");
			 
			 
			 double cusip_weight = 0.00;
			try {
				cusip_weight = l.doubleValue()/total_par.get(counter);
			}
			catch(Exception e) {
				System.out.println("Division Error");
			}
			cusipWeight.append(cusip_weight);
			double price =  (double) dataFrame.floatColumn("dollar_price").getFloat(counter);
			double yield = (double) dataFrame.floatColumn("yield").getFloat(counter);
			double ytw = 0;
			try {
				 ytw = row.getDouble("trade_ytw");
			}
			catch(Exception e) {
				System.out.println("Errored");
				System.out.println(row);
			}
			double mmd = 0;
			double yield_mmd = 0;
			double ytw_mmd = 0; 
				 if(hasMMD) {
					  mmd = row.getDouble("Trade Date MMD");
					  yield_mmd = yield - mmd;
					  ytw_mmd = ytw - mmd;
				 }
				 
				 cusipWeightTradeYield.append(cusip_weight*yield);
				 cusipWeightTradeYTW.append(cusip_weight*ytw);
				 cusipWeightAvgPrice.append(cusip_weight*price);

				 if(hasMMD) {
					 cusipWeightTradeYieldSpread.append(cusip_weight*yield_mmd);
					 cusipWeightTradeYTWSpread.append(cusip_weight*ytw_mmd);
					 cusipWeightTradeMMD.append(cusip_weight*mmd);

				 }
		 }
		 
		 System.out.println(total_par);
		 System.out.println(total_par.size());

		 System.out.println(cusipWeight);
		 System.out.println(cusipWeight.size());

		 System.out.println(cusipWeightTradeYield);
		 System.out.println(cusipWeightTradeYield.size());

		 System.out.println(cusipWeightTradeYTW);
		 System.out.println(cusipWeightTradeYTW.size());


		 dataFrame =  dataFrame.addColumns(total_par,cusipWeight, cusipWeightTradeYield,cusipWeightTradeYTW);
		 if(hasMMD) {
			 dataFrame = dataFrame.addColumns(cusipWeightTradeYieldSpread,cusipWeightTradeYTWSpread, cusipWeightTradeMMD);
		 }
		 
		 dataFrame = dataFrame.addColumns(cusipWeightAvgPrice);
		 
		 tradeMap.replace(key, dataFrame);
		}

			Table TradingTableByDate = tradeMap.get("TradingHistorySummarybyDate");
			Table TradingTableAllDates = tradeMap.get("TradingHistorySummary");
			
			System.out.println("Grouping");
			System.out.println(TradingTableByDate);
			System.out.println(TradingTableAllDates);

			
			tradeMap.put("TradingHistorySummaryRaw", TradingTableAllDates);
			if(TradingTableByDate.columnNames().contains("Trade Date MMD"))
			{
				TradingTableByDate = TradingTableByDate.retainColumns("cusip","trade_date","Cusip Weighted Trade Yield","Cusip Weighted Trade YTW", "Cusip Weighted Trade Date MMD","Cusip Weighted Spread Yield","Cusip Weighted Spread YTW","Cusip Weighted Average Price");
				Table TradingTableByDate1 = Table.create("byDate1");
				Table TradingTableByDate2 = Table.create("byDate2");

				 TradingTableByDate1 = TradingTableByDate.summarize("Cusip Weighted Trade Yield","Cusip Weighted Trade YTW", "Cusip Weighted Trade Date MMD","Cusip Weighted Spread Yield", tech.tablesaw.aggregate.AggregateFunctions.sum).by("cusip","trade_date");	
				 TradingTableByDate2 = TradingTableByDate.summarize("Cusip Weighted Spread YTW","Cusip Weighted Average Price", tech.tablesaw.aggregate.AggregateFunctions.sum).by("cusip","trade_date");	
				


				Table TradingTableByDateFinal = Table.create("byDateFinal");
				TradingTableByDateFinal = TradingTableByDate1.addColumns(TradingTableByDate2.doubleColumn("Sum [Cusip Weighted Spread YTW]"),TradingTableByDate2.doubleColumn("Sum [Cusip Weighted Average Price]"));
				System.out.println(TradingTableByDateFinal);
				
				TradingTableByDate1 = null;
				TradingTableByDate2 = null;
								
				System.out.println("ALL DATES");
				System.out.println(TradingTableAllDates);
				TradingTableAllDates = TradingTableAllDates.retainColumns("cusip","Cusip Weighted Trade Yield","Cusip Weighted Trade YTW", "Cusip Weighted Trade Date MMD","Cusip Weighted Spread Yield","Cusip Weighted Spread YTW","Cusip Weighted Average Price");
				
				Table TradingTableAllDates1 = Table.create("byAllDates1");
				Table TradingTableAllDates2 = Table.create("byAllDates2");

				
				TradingTableAllDates1 = TradingTableAllDates.summarize("Cusip Weighted Trade Yield","Cusip Weighted Trade YTW", "Cusip Weighted Trade Date MMD","Cusip Weighted Spread Yield", tech.tablesaw.aggregate.AggregateFunctions.sum).by("cusip");	
				TradingTableAllDates2 = TradingTableAllDates.summarize("Cusip Weighted Spread YTW","Cusip Weighted Average Price", tech.tablesaw.aggregate.AggregateFunctions.sum).by("cusip");	
				
				Table TradingTableAllDatesFinal = Table.create("byAllDatesFinal");
				TradingTableAllDatesFinal = TradingTableAllDates1.addColumns(TradingTableAllDates2.doubleColumn("Sum [Cusip Weighted Spread YTW]"),TradingTableAllDates2.doubleColumn("Sum [Cusip Weighted Average Price]"));
				
				TradingTableAllDates1 = null;
				TradingTableAllDates2 = null;
				
				TradingTableByDateFinal = TradingTableByDateFinal.sortOn("cusip","trade_date");
				TradingTableAllDatesFinal = TradingTableAllDatesFinal.sortOn("cusip");
				
				tradeMap.replace("TradingHistorySummarybyDate", TradingTableByDateFinal);
				tradeMap.replace("TradingHistorySummary", TradingTableAllDatesFinal);

			}
			else {
				TradingTableByDate = TradingTableByDate.retainColumns("cusip","trade_date","Cusip Weighted Trade Yield", "Cusip Weighted Trade YTW", "Cusip Weighted Average Price");
				TradingTableByDate = TradingTableByDate.summarize("Cusip Weighted Trade Yield", "Cusip Weighted Trade YTW", "Cusip Weighted Average Price", tech.tablesaw.aggregate.AggregateFunctions.sum).by("cusip", "trade_date");
								

				
				TradingTableAllDates = TradingTableAllDates.retainColumns("cusip","trade_date","Cusip Weighted Trade Yield", "Cusip Weighted Trade YTW", "Cusip Weighted Average Price");
				TradingTableAllDates = TradingTableAllDates.summarize("Cusip Weighted Trade Yield", "Cusip Weighted Trade YTW", "Cusip Weighted Average Price", tech.tablesaw.aggregate.AggregateFunctions.sum).by("cusip");

				TradingTableByDate = TradingTableByDate.sortOn("cusip","trade_date");
				TradingTableAllDates = TradingTableAllDates.sortOn("cusip");
				
				tradeMap.replace("TradingHistorySummarybyDate", TradingTableByDate);
				tradeMap.replace("TradingHistorySummary", TradingTableAllDates);

			}
			
			

			
		 return(tradeMap);
    }
    
    
    
	private LocalDate getDateNearest(TreeSet<LocalDate> dates, LocalDate targetDate){
		if(dates.contains(targetDate)) {
 		   return targetDate;
 		}
		return dates.lower(targetDate);
	}
	
	private double getNumNearest(TreeSet<Double> nums, double targetDouble){
		if(nums.contains(targetDouble)) {
 		   return targetDouble;
 		}
		return nums.lower(targetDouble);
	}
	
    public static Table getMMDFrame(String mmdName) throws IOException, InvalidFormatException {
        XSSFWorkbook workbook = new XSSFWorkbook(mmdName);
        XSSFSheet sheet = workbook.getSheet("Historical MMD");
        StringBuilder data = new StringBuilder();
        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
		File f = new File(System.getProperty("java.class.path"));
		File userdir = f.getAbsoluteFile().getParentFile();
		String pathName = userdir.toString();
		
        
        try {
            Iterator<Row> rowIterator = sheet.iterator();
            //SkipTwo
            
            rowIterator.next();
            rowIterator.next();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                	
                    Cell cell = cellIterator.next();
                    CellValue cellValue = evaluator.evaluate(cell);
                    
                    CellType type = cellValue.getCellType();
                    if (type == CellType.BOOLEAN) {
                        data.append(cellValue.getBooleanValue());
                    } else if (type == CellType.NUMERIC) {
                    	if(cell.getColumnIndex() == 0) {
                    		data.append(cell.getLocalDateTimeCellValue().toLocalDate());
                    	}
                    	else {
                        data.append(cellValue.getNumberValue());
                    	}
                    } else if (type == CellType.STRING) {
                        data.append(cellValue.getStringValue());
              
                    } else if (type == CellType.BLANK) {
                    } else {
                        data.append(cell + "");
                    }
                    data.append(",");
                }
                data.append('\n');
            }

            Files.write(Paths.get(pathName + "/MMDDate.csv"), data.toString().getBytes("UTF-8"));

            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            
        }
        
        System.out.println("Success");
        
    	CsvReadOptions.Builder builder = CsvReadOptions.builder(pathName + "/MMDDate.csv").separator(',').header(true);									// table is tab-delimited
    	CsvReadOptions options = builder.build();  
    	System.out.println("MMD: " + pathName + "/MMDDate.csv");
    	Table mmdTable = Table.read().csv(pathName + "/MMDDate.csv"); 
        System.out.println(mmdTable);        
        return(mmdTable);
        
        
        
    }

 
    
    public static boolean containsSubString (Collection<String> col, String str) {
        for (String s: col) {
            if (s.contains(str)) {
                return true;
            }
        }
        return false;
    }

    public String Cusip9trimString(String s) {
    	
    	
    	return(s.substring(0, 6));
    }
    
    public double sumDataFrame(List list) {
    	List<Double> doubleList  = list;
    	double sum = 0;
    	for(Double i: doubleList) {
    		sum += i;
    	}
    	return(sum);
    }
    

}
	


